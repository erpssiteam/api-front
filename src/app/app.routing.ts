import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent }   from './app.component';
import { ErrorsComponent } from './templates/errors/errors.component';
import { FooterComponent } from './templates/footer/footer.component';
import { HeaderComponent } from './templates/header/header.component';
import { NavbarasideComponent } from './templates/navbaraside/navbaraside.component';

import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { EmailComponent } from './components/email/email.component';
import { EmailcreateComponent } from './components/email/emailcreate/emailcreate.component';
import { EmailviewComponent } from './components/email/emailview/emailview.component';
import { EmployesComponent } from "./components/employes/list/employes.component";
import { EmployeeCreateComponent } from "./components/employes/create/employee-create.component";
import { EmployeeUpdateComponent } from "./components/employes/update/employee-update.component";
import { EmployeViewComponent } from "./components/employes/view/employe-view.component";
import { HolidaysListComponent } from "./components/holidays/list/holidays-list.component";
import { HolidaysUpdateComponent } from "./components/holidays/update/holidays-update.component";
import { HolidaysCreateComponent } from "./components/holidays/create/holidays-create.component";
import { HolidaysFormComponent } from './components/holidays/form/holidays-form.component';

import { ExpenseAccountUpdateComponent } from './components/expense-account/update/expense-account-update.component';
import { ExpenseAccountCreateComponent } from './components/expense-account/create/expense-account-create.component';
import { ExpenseAccountRootComponent } from './components/expense-account/root/expense-account-root.component';

import { NewsCreateComponent } from './components/news/create/news-create.component';
import { NewsUpdateComponent } from './components/news/update/news-update.component';
import { NewsViewComponent } from './components/news/view/news-view.component';
import { NewsListComponent } from './components/news/list/news-list.component';

import { ApplicationUpdateComponent } from './components/application/update/application-update.component';
import { ApplicationViewComponent } from './components/application/view/application-view.component';
import { ApplicationListComponent } from './components/application/list/application-list.component';
import { ApplicationCreateComponent } from './components/application/create/application-create.component';

import { SkillUpdateComponent } from './components/skill/update/skill-update.component';
import { SkillRootComponent } from './components/skill/root/skill-root.component';
import { SkillCreateComponent } from './components/skill/create/skill-create.component';

import { CustomersCreateComponent } from './components/customers/create/customers-create.component';
import { CustomersListComponent } from './components/customers/list/customers-list.component';
import { CustomersUpdateComponent } from './components/customers/update/customers-update.component';

import { GroupwareListComponent } from './components/groupware/list/groupware-list.component';

import { ProfileFormComponent } from './components/profile/form/profile-form.component';

import { JobProfileListComponent } from './components/jobprofile/list/jobprofile-list.component';
import { JobProfileUpdateComponent } from './components/jobprofile/update/jobprofile-update.component';
import { JobProfileCreateComponent } from './components/jobprofile/create/jobprofile-create.component';
import { JobProfileFormComponent } from './components/jobprofile/form/jobprofile-form.component';

import { OfferListComponent } from './components/offer/list/offer-list.component';
import { OfferUpdateComponent } from './components/offer/update/offer-update.component';
import { OfferCreateComponent } from './components/offer/create/offer-create.component';
import { OfferFormComponent } from './components/offer/form/offer-form.component';

import { CrasUpdateComponent } from './components/cras/update/cras-update.component';
import { CrasCreateComponent } from './components/cras/create/cras-create.component';
import { CrasRootComponent } from './components/cras/root/cras.root.component';
import { CrasViewComponent } from './components/cras/view/cras-view.component';

import { CvListComponent } from './components/cv/list/cv-list.component';

import { AuthGuard } from './services/authGuard/auth-guard.service';


const appRoutes: Routes = [

  { path: '', canActivate: [AuthGuard], children :[
    { path: '' , component: HeaderComponent, outlet: 'header'},
    { path: '' , component: FooterComponent, outlet: 'footer'},
    { path: '' , component: NavbarasideComponent, outlet: 'navbaraside'},
    { path: '', component: DashboardComponent},

    { path: 'employees' , children :[
      { path: '' , component: EmployesComponent },
      { path: 'view/:id', component: EmployeViewComponent },
      { path: 'create', component: EmployeeCreateComponent },
      { path: 'update/:id', component: EmployeeUpdateComponent },

    ]},
    { path: 'cv' , children :[
      { path: '' , component: CvListComponent },

    ]},
    { path: 'cras' , children :[
      { path: '' , component: CrasRootComponent },
      { path: 'view/:id', component: CrasViewComponent },
      { path: 'create', component: CrasCreateComponent },
      { path: 'update/:id', component: CrasUpdateComponent },

    ]},
    { path: 'customers', children :[
      { path: '', component: CustomersListComponent },
      { path: 'create', component: CustomersCreateComponent },
      { path: 'update/:id', component: CustomersUpdateComponent },

    ]},
    { path: 'expense-accounts', children : [
      { path: '', component: ExpenseAccountRootComponent },
      { path: 'create', component: ExpenseAccountCreateComponent },
      { path: 'update/:id', component: ExpenseAccountUpdateComponent },
    ]},
    { path: 'news', children : [
      { path: '', component: NewsListComponent },
      { path: 'create', component: NewsCreateComponent },
      { path: 'update/:id', component: NewsUpdateComponent },
      { path: 'view/:id', component: NewsViewComponent },
    ]},
    { path: 'skills', children : [
      { path: '', component: SkillRootComponent },
      { path: 'create', component: SkillCreateComponent },
      { path: 'update/:id', component: SkillUpdateComponent },
    ]},
    { path: 'applications', children : [
      { path: '', component: ApplicationListComponent },
      { path: 'create', component: ApplicationCreateComponent },
      { path: 'update/:id', component: ApplicationUpdateComponent },
      { path: 'view/:id', component: ApplicationViewComponent },
    ]},
    { path: 'holidays', children : [
      { path: '', component: HolidaysListComponent },
      { path: 'create', component: HolidaysCreateComponent },
      { path: 'update/:id', component: HolidaysUpdateComponent },
    ]},
    { path: 'jobs', children : [
      { path: '', component: JobProfileListComponent },
      { path: 'create', component: JobProfileCreateComponent },
      { path: 'update/:id', component: JobProfileUpdateComponent },
      { path: 'offers', children : [
        { path: '', component: OfferListComponent },
        { path: 'create', component: OfferCreateComponent },
        { path: 'update/:id', component: OfferUpdateComponent },
      ]},
    ]},
    { path: 'profile', component: ProfileFormComponent},
    { path: 'email', children: [
      { path: '', component: EmailComponent },
      { path: 'creer', component: EmailcreateComponent },
      { path: ':id', component: EmailviewComponent }
    ]},
    { path: 'groupware', children: [
      { path: '', component: GroupwareListComponent },
    ]}
  ]},
  { path: 'login', children :[
    { path: '', component: LoginComponent}
  ]},

  { path: '**', component: ErrorsComponent },
  { path: 'error', component: ErrorsComponent },
];

export const appRoutingProviders: any[] = [

];


export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
