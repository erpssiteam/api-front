import { Pipe } from '@angular/core'

@Pipe({
    name: 'truncate'
})
export class TruncatePipe {
    transform(value: string, limit: number) : string {
        let trail = '...';

        if(value != undefined){
            value = value.replace(/<\/?[^>]+>/ig, " ").trim()
            return value.length > limit ? value.substring(0, limit) + trail : value;
        } else {
            return '';
        }
    }
}
