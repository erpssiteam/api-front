import { Component, OnInit } from '@angular/core';
import { TranslateService } from 'ng2-translate/ng2-translate';
import * as _ from 'lodash';
import { CookiesTokenService } from './services/cookies/cookies-token.service';
import { Router } from '@angular/router'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [CookiesTokenService]
})
export class AppComponent implements OnInit {
  title = 'app works!';
  private _showCookieBar:boolean = true;

  constructor(private translate: TranslateService, private _cookiesTokenService:CookiesTokenService, private _router: Router){
      this.translateConfig();
  }

  ngOnInit(){

      if(this._cookiesTokenService.getCookie('cookie') != null){
          this._showCookieBar = false;
      } else {
          this._showCookieBar = true;
      }
  }

  createCookie(){
      this._cookiesTokenService.setCookie('cookie', 'true');
      this._showCookieBar = false;
  }

  translateConfig() {

    let userLang = navigator.language.split('-')[0];
    userLang = /(fr|en|de)/gi.test(userLang) ? userLang : 'en';

    // this language will be used as a fallback when a translation isn't found in the current language
    this.translate.setDefaultLang(userLang);

    // the lang to use, if the lang isn't available, it will use the current loader to get them
    this.translate.use(userLang);
  }
}
