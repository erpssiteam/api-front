import { AbstractControl, ValidatorFn, NG_VALIDATORS, Validator, Validators } from '@angular/forms';
import { OnChanges, Input, Directive, SimpleChange, SimpleChanges } from "@angular/core";

export function forbiddenZipCodeValidator(): ValidatorFn  {
    let ZIPCODE_REGEXP = new RegExp("/^(([0-8][0-9])|(9[0-5])|(2[ab]))[0-9]{3}$/");

    return (c: AbstractControl) => {
        let isValid = ZIPCODE_REGEXP.test(c.value);
        if (isValid) {
            return null;
        } else {
            return {
                validateZipCode: {
                    valid: false
                }
            };
        }
    }
}
@Directive({
    selector: '[forbiddenZipcode][ngModel]',
    providers: [
        { provide: NG_VALIDATORS , useExisting: ZipCodeValidator, multi: true }
    ]
})
export class ZipCodeValidator implements Validator, OnChanges {

    @Input() forbiddenZipcode: string;
    private validator: ValidatorFn = Validators.nullValidator;

    constructor() {
        this.validator = forbiddenZipCodeValidator();
    }
    ngOnChanges(changes: SimpleChanges): void {
        const change = changes['forbiddenZipcode'];
        if (change) {
            this.validator = forbiddenZipCodeValidator();
        } else {
            this.validator = Validators.nullValidator;
        }
    }
    validate(control: AbstractControl): {[key: string]: any} {
        return this.validator(control);
    }

}