import { AbstractControl, ValidatorFn, NG_VALIDATORS, Validator, Validators } from '@angular/forms';
import { OnChanges, Input, Directive, SimpleChange, SimpleChanges } from "@angular/core";

export function forbiddenTelephoneValidator(): ValidatorFn  {
    let TELEPHONE_REGEXP = new RegExp("/(0|(\\+33)|(0033))[1-9][0-9]{8}$");

    return (c: AbstractControl) => {
        let isValid = TELEPHONE_REGEXP.test(c.value);
        if (isValid) {
            return null;
        } else {
            return {
                validateTelephone: {
                    valid: false
                }
            };
        }
    }
}
@Directive({
    selector: '[forbiddenTelephone][ngModel]',
    providers: [
        { provide: NG_VALIDATORS , useExisting: TelephoneValidator, multi: true }
    ]
})
export class TelephoneValidator implements Validator, OnChanges {

    @Input() forbiddenTelephone: string;
    private validator: ValidatorFn = Validators.nullValidator;

    constructor() {
        this.validator = forbiddenTelephoneValidator();
    }
    ngOnChanges(changes: SimpleChanges): void {
        const change = changes['forbiddenTelephone'];
        if (change) {
            this.validator = forbiddenTelephoneValidator();
        } else {
            this.validator = Validators.nullValidator;
        }
    }
    validate(control: AbstractControl): {[key: string]: any} {
        return this.validator(control);
    }

}