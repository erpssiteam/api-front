import { AbstractControl, ValidatorFn, NG_VALIDATORS, Validator, Validators } from '@angular/forms';
import { OnChanges, Input, Directive, SimpleChange, SimpleChanges } from "@angular/core";

export function forbiddenCinValidator(): ValidatorFn  {
    let CIN_REGEXP = new RegExp("^0[1-2]([0-9]{2}){5}$");

    return (c: AbstractControl) => {
        let isValid = CIN_REGEXP.test(c.value);
        if (isValid) {
            return null;
        } else {
            return {
                validateCin: {
                    valid: false
                }
            };
        }
    }
}
@Directive({
    selector: '[forbiddenCin][ngModel]',
    providers: [
        { provide: NG_VALIDATORS , useExisting: CinValidator, multi: true }
    ]
})

export class CinValidator implements Validator, OnChanges {

    @Input() forbiddenCin: string;
    private validator: ValidatorFn = Validators.nullValidator;

    constructor() {
        this.validator = forbiddenCinValidator();
    }
    ngOnChanges(changes: SimpleChanges): void {
        const change = changes['forbiddenCin'];
        if (change) {
            this.validator = forbiddenCinValidator();
        } else {
            this.validator = Validators.nullValidator;
        }
    }
    validate(control: AbstractControl): {[key: string]: any} {
        return this.validator(control);
    }

}