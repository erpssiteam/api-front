import { AbstractControl, ValidatorFn, NG_VALIDATORS, Validator, Validators } from '@angular/forms';
import { OnChanges, Input, Directive, SimpleChange, SimpleChanges } from "@angular/core";

export function forbiddenPasseportValidator(): ValidatorFn  {
    let PASSEPORT_REGEXP = new RegExp("/^[A-PR-WY][1-9]\d\s?\d{4}[1-9]$","ig");

    return (c: AbstractControl) => {
        let isValid = PASSEPORT_REGEXP.test(c.value);
        if (isValid) {
            return null;
        } else {
            return {
                validatePasseport: {
                    valid: false
                }
            };
        }
    }

}
@Directive({
    selector: '[forbiddenPasseport][ngModel]',
    providers: [
        { provide: NG_VALIDATORS , useExisting: PasseportValidator, multi: true }
    ]
})
export class PasseportValidator implements Validator, OnChanges {

    @Input() forbiddenPasseport: string;
    private validator: ValidatorFn = Validators.nullValidator;

    constructor() {
        this.validator = forbiddenPasseportValidator();
    }
    ngOnChanges(changes: SimpleChanges): void {
        const change = changes['forbiddenPasseport'];
        if (change) {
            this.validator = forbiddenPasseportValidator();
        } else {
            this.validator = Validators.nullValidator;
        }
    }
    validate(control: AbstractControl): {[key: string]: any} {
        return this.validator(control);
    }

}