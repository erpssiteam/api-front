import {Injectable} from '@angular/core';
import {ModelInterface} from './common/model.interface';
import { DatePickerOptions, DateModel } from 'ng2-datepicker';
import {CommonModel} from './common/common.model';

@Injectable()
export class Holiday extends CommonModel implements ModelInterface {

    private id: Number;
    private employee: any;
    private reason: Number;
    private ended_at: Date;
    private state: Number;
    private isValid: boolean;
    private started_at: Date;
    private validated_at: Date;
    private asked_at: Date;
    private reasonsArray: Array<Object> = [
        {
            id: 1,
            label: 'Congé payé'
        },
        {
            id: 2,
            label: 'Congé sans solde'
        },
        {
            id: 3,
            label: 'Congé parental'
        },
        {
            id: 4,
            label: 'RTT'
        }
    ];
    private stateArray: Array<Object> = [
        {
            id: '1',
            label: 'En cours',
        },
        {
            id: '2',
            label: 'Acceptée',
        },
        {
            id: '3',
            label: 'Refusée',
        }
    ];

    constructor() {
      super();
      this.state = 1;
      this.reason = 1;
    }

    /**
    *
    * @param model
    * @param json
    * @returns {null}
    */
    hydrateModelFromJSON(model, json): {} {
        model._id = json.id;
        model._employee = json.employee;
        model._reason = json.reason;
        model._ended_at = json.ended_at;
        model._state = json.state;
        model._started_at = json.started_at;
        model._validated_at = json.validated_at;
        model._asked_at = json.asked_at;
        return model;
    }

    get _asked_at(): any {
        return this.asked_at;
    }

    set _asked_at(value: any) {
        this.asked_at = value;
    }

    get _id(): Number {
        return this.id;
    }

    set _id(value: Number) {
        this.id = value;
    }

    get _state(): Number {
        return this.state;
    }

    set _state(value: Number) {
        this.state = value;
    }

    get _reason(): Number {
        return this.reason;
    }

    set _reason(value: Number) {
        this.reason = value;
    }

    get _ended_at(): any {
        return this.ended_at;
    }

    set _ended_at(value: any) {
        this.ended_at = value;
    }

    get _started_at(): any {
        return this.started_at;
    }

    set _started_at(value: any) {
        this.started_at = value;
    }

    set _validated_at(value: any) {
        this.validated_at = value;
    }

    get _validated_at(): any {
        return this.validated_at;
    }

    set _employee(value: any) {
        this.employee = value;
    }

    get _employee(): any {
        return this.employee;
    }
}
