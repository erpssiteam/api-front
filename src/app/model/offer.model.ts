import {Injectable} from '@angular/core';
import {ModelInterface} from './common/model.interface';
import {CommonModel} from './common/common.model';

@Injectable()
export class Offer extends CommonModel implements ModelInterface {

    private id: Number;
    private consulted: Number;
    private end_at: Date;
    private is_valid: Boolean;
    private job_profile: Number;
    private name: String;
    private reference: String;
    private start_at: Date;
    private created_at: Date;
    private modified_at: Date;

    constructor() {
      super();
      this._is_valid = false
      this._consulted = 0
    }

    /**
    *
    * @param model
    * @param json
    * @returns {null}
    */
    hydrateModelFromJSON(model, json): {} {
        model._id = json.id;
        model._consulted = json.consulted;
        model._end_at = json.end_at;
        model._is_valid = json.is_valid;
        model._job_profile = json.job_profile.id;
        model._name = json.name;
        model._reference = json.reference;
        model._start_at = json.start_at;
        model._created_at = json.created_at;
        model._modified_at = json.modified_at;
        return model;
    }

    get _consulted(): Number {
        return this.consulted;
    }

    set _consulted(value: Number) {
        this.consulted = value;
    }

    get _end_at(): any {
        return this.end_at;
    }

    set _end_at(value: any) {
        this.end_at = value;
    }

    get _id(): Number {
        return this.id;
    }

    set _id(value: Number) {
        this.id = value;
    }

    get _is_valid(): Boolean {
        return this.is_valid;
    }

    set _is_valid(value: Boolean) {
        this.is_valid = value;
    }

    get _job_profile(): Number {
        return this.job_profile;
    }

    set _job_profile(value: Number) {
        this.job_profile = value;
    }

    get _name(): String {
        return this.name;
    }

    set _name(value: String) {
        this.name = value;
    }

    get _reference(): String {
        return this.reference;
    }

    set _reference(value: String) {
        this.reference = value;
    }

    get _start_at(): any {
        return this.start_at;
    }

    set _start_at(value: any) {
        this.start_at = value;
    }

    get _created_at(): any {
        return this.created_at;
    }

    set _created_at(value: any) {
        this.created_at = value;
    }

    get _modified_at(): any {
        return this.modified_at;
    }

    set _modified_at(value: any) {
        this.modified_at = value;
    }
}
