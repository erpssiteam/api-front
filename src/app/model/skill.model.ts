import {ModelInterface} from "./common/model.interface";
import {CommonModel} from "./common/common.model";

export class Skill extends CommonModel implements ModelInterface{

  private id: Number;
  private label: String;
  private description: String;
  private modified_at: Date;
  private created_at: Date;


  constructor() {
    super();
  }

  /**
   *
   * @returns {Number}
   */
  getId(): Number {
    return this.id;
  }

  /**
   *
   * @param id
   */
  setId(id: Number) {
    this.id = id;
  }

  getLabel(): String {
    return this.label;
  }

  setLabel(value: String) {
    this.label = value;
  }

  setDescription(value: String) {
    this.description = value;
  }

  getDescription(): String {
    return this.description;
  }

  /**
   *
   * @param createdAt
   */
  setCreatedAt(createdAt: Date) {
    this.created_at = createdAt;
  }

  /**
   *
   * @returns {Date}
   */
  getModifiedAt(): Date {
    return this.modified_at;
  }

  /**
   *
   * @param modifiedAt
   */
  setModifiedAt(modifiedAt: Date) {
    this.modified_at = modifiedAt;
  }

  /**
   *
   * @returns {Date}
   */
  getCreatedAt(): Date {
    return this.created_at;
  }


  /**
   *
   * @param model
   * @param json
   * @returns {any}
   */
  hydrateModelFromJSON(model, json): {} {

    model.setId(json.id);
    model.setLabel(json.label);
    model.setDescription(json.description);
    model.setCreatedAt(json.created_at);
    model.setModifiedAt(json.modified_at);
    return model;

  }
}
