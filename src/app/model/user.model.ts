import {Injectable} from '@angular/core'
import {ModelInterface} from "./common/model.interface";
import {Role} from "./role.model";
@Injectable()

export class User implements ModelInterface{

  private id: Number;
  private createdAt: Date;
  private validateAt: Date;

  constructor(private username: String, private password: String, private role: [Role]) {
  }

  getCreatedAt(): Date {
    return this.createdAt;
  }

  setCreatedAt(value: Date) {
    this.createdAt = value;
  }

  getValidateAt(): Date {
    return this.validateAt;
  }

  setValidateAt(value: Date) {
    this.validateAt = value;
  }

  getUsername(): String {
    return this.username;
  }

  setUsername(value: String) {
    this.username = value;
  }

  getPassword(): String {
    return this.password;
  }

  setPassword(value: String) {
    this.password = value;
  }

  getRole(): [Role] {
    return this.role;
  }

  setRole(value: [Role]) {
    this.role = value;
  }

  getId(): Number {
    return this.id;
  }

  setId(value: Number) {
    this.id = value;
  }

  /**
   *
   * @param model User
   * @param json
   * @returns {null}
   */
  hydrateModelFromJSON(model, json): {} {
    model.setId(json.id);
    model.setUsername(json.username);
    return model;
  }
}
