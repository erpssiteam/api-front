import {Injectable} from '@angular/core'
@Injectable()

export class CommonModel {

  /**
   *
   * @returns {string}
   * @param value
   */
  getFormatedDateToString(value: any): string {
    return value.date.substring(0, 10);
  }

  /**
   * @param value
   * @returns {Date}
   */
  getFormatedDateToDatePicker(value: any): Date {
    return new Date(value);
  }

}
