import { Injectable } from '@angular/core';
import { ModelInterface } from './common/model.interface';
import { DatePickerOptions, DateModel } from 'ng2-datepicker';
import { Employee } from './employee.model';
import { EmployesProvider } from '../services/employes/employes-provider.service';
import { Role } from './role.model';
import { CommonModel } from './common/common.model';

@Injectable()
export class Groupware extends CommonModel implements ModelInterface {

    id: Number;
    roles:Array<Role> = [];
    private employees:Array<Employee> = [];
    private employeesRoles:Array<String> = [];
    private _employesProvider: EmployesProvider;

    constructor() {
      super();
    }

    /**
    *
    * @param model
    * @param json
    * @returns {null}
    */
    hydrateModelFromJSON(model, json): {} {

        for(let i in json){
            if(json.hasOwnProperty(i)){
                // roles
                let role = new Role(json[i].label);
                this._roles.push(role)

                // employees
                // this.employees.push(json[i].id)
            }
        }

        for(let r in this.roles){

            for(let e in this.employees){
                let value : String = ""

                // for(let j of json){
                    if(this.employees[e].getUser().getRole()[0][0] === this.roles[r]._label){
                        value = "checked"
                    }
                // }

                // employeesRoles
                this.employeesRoles[this.roles[r]._label+"-"+this.employees[e].getId()] = value
            }
        }

        return model;
    }

    get _id(): Number {
        return this.id;
    }

    set _id(value: Number) {
        this.id = value;
    }

    get _roles(): Array<Role> {
        return this.roles;
    }

    set _roles(value: Array<Role>) {
        this.roles = value;
    }

    get _employees(): Array<Employee> {
        return this.employees;
    }

    set _employees(value: Array<Employee>) {
        this.employees = value;
    }

    get _employeesRoles(): Array<String> {
        return this.employeesRoles;
    }

    set _employeesRoles(value: Array<String>) {
        this.employeesRoles = value;
    }
}
