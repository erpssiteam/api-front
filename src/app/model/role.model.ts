import {Injectable} from '@angular/core'
import {ModelInterface} from "./common/model.interface";

@Injectable()
export class Role implements ModelInterface{
    private id: Number;
    private createdAt: Date;
    private modifiedAt: Date;
    rolesMap = { admin: 'ROLE_ADMIN', rh: 'ROLE_RH', manager: 'ROLE_MANAGER', collaborator: 'ROLE_COLLABORATOR', news: 'ROLE_NEWS' };
    
    constructor(private label: String) {
        this._label = label
    }

    /**
    *
    * @param model
    * @param json
    * @returns Role
    */
    hydrateModelFromJSON(model, json): Role {
        model._id = json.id
        model._label = json.label
        return model;
    }

  getLabel(): String {
    return this.label;
  }

    set _id(value: Number) {
        this.id = value;
    }

    get _id(): Number {
        return this.id;
    }

    set _label(value: String) {
        this.label = value;
    }

    get _label(): String {
        return this.label;
    }
}
