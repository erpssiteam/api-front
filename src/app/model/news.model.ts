import {ModelInterface} from "./common/model.interface";
import {CommonModel} from "./common/common.model";
import Any = jasmine.Any;

export class News extends CommonModel implements ModelInterface{

  private id: Number;
  private published: Boolean;
  private modifiedAt: Date;
  private createdAt: Date;

  /**
   *
   * @param title
   * @param content
   * @param date_news
   */
  constructor(private title: String, private content: String, private date_news: Date) {
    super();
    this.setTitle(title);
    this.setContent(content);
    this.setDateNews(date_news);
    this.setPublished(false);
  }

  /**
   *
   * @returns {Number}
   */
  getId(): Number {
    return this.id;
  }

  /**
   *
   * @param id
   */
  setId(id: Number) {
    this.id = id;
  }

  /**
   *
   * @returns {String}
   */
  getTitle(): String {
    return this.title;
  }

  /**
   *
   * @param title
   */
  setTitle(title: String) {
    this.title = title;
  }

  getContent(): String {
    return this.content;
  }

  setContent(content: String) {
    this.content = content;
  }

  getDateNews(): Date {
    return this.date_news;
  }

  /**
   *
   * @param date_news
   */
  setDateNews(date_news: any) {
    this.date_news = date_news;
  }

  isPublished(): Boolean {
    return this.published;
  }

  setPublished(published: Boolean) {
    this.published = published;
  }

  /**
   *
   * @param createdAt
   */
  setCreatedAt(createdAt: Date) {
    this.createdAt = createdAt;
  }

  /**
   *
   * @returns {Date}
   */
  getModifiedAt(): Date {
    return this.modifiedAt;
  }

  /**
   *
   * @param modifiedAt
   */
  setModifiedAt(modifiedAt: Date) {
    this.modifiedAt = modifiedAt;
  }

  /**
   *
   * @returns {Date}
   */
  getCreatedAt(): Date {
    return this.createdAt;
  }



  /**
   *
   * @param model
   * @param json
   * @returns {any}
   */
  hydrateModelFromJSON(model, json): {} {

    model.setId(json.id);
    model.setTitle(json.title);
    model.setContent(json.content);
    model.setDateNews(json.date_news);
    model.setPublished(json.published);
    model.setCreatedAt(json.created_at);
    model.setModifiedAt(json.modified_at);
    return model;

  }
}
