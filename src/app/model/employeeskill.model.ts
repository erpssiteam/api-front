import {ModelInterface} from "./common/model.interface";
import {CommonModel} from "./common/common.model";
import {Skill} from "./skill.model";
import {Employee} from "./employee.model";

export class EmployeeSkill extends CommonModel implements ModelInterface{

    private id: Number;
    private level: String;
    private date_skill: Date;
    private description: String;
    private employee: Number;
    private validate: Boolean;

    constructor() {
        super();
        this._validate = false;
        this._level = "0"
        this._description = ""
    }

    /**
    *
    * @param model
    * @param json
    * @returns {any}
    */
    hydrateModelFromJSON(model, json): {} {
        model._level = json.level;
        model._date_skill = json.date_skill;
        model._description = json.description;
        model._validate = json.validate;
        model._id = json.skill.id
        // model._employee = json.employee.id;
        return model;

    }

    get _validate():Boolean {
        return this.validate;
    }

    set _validate(_validate: Boolean) {
        this.validate = _validate;
    }

    get _id():Number {
        return this.id;
    }

    set _id(_id: Number) {
        this.id = _id;
    }

    get _date_skill():any {
        return this.date_skill;
    }

    set _date_skill(_date_skill: any) {
        this.date_skill = _date_skill;
    }

    get _description():String {
        return this.description;
    }

    set _description(_description: String) {
        this.description = _description;
    }

    get _level():String {
        return this.level;
    }

    set _level(_level: String) {
        this.level = _level;
    }

    get _employee():Number {
        return this.employee;
    }

    set _employee(_employee: Number) {
        this.employee = _employee;
    }
}
