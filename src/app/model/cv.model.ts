import { Injectable } from '@angular/core';
import { ModelInterface } from './common/model.interface';
import { CommonModel } from './common/common.model';

@Injectable()
export class Cv extends CommonModel implements ModelInterface {

  private id: Number;
  private src: String;

  constructor() {
    super();
  }

    getId(): Number {
        return this.id;
    }

    setId(value: Number) {
        this.id = value;
    }

    getSrc(): String {
        return this.src;
    }

    setSrc(value: String) {
        this.src = value;
    }


  /**
   *
   * @param model
   * @param json
   * @returns {null}
   */
  hydrateModelFromJSON(model, json ) {
      model.setId(json.id);
      model.setSrc("http://"+json.src);
    return model;
  }


}
