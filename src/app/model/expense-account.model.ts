import { Employee } from './employee.model';
import {ModelInterface} from "./common/model.interface";
import {CommonModel} from "./common/common.model";

const STATE_PENDING = 1;
const STATE_VALIDATED = 2;
const STATE_DENIED = 3;

export class ExpenseAccount extends CommonModel implements ModelInterface{

  private id: Number;
  private description: String;
  private image: String;
  private date_expense_account: Date;
  private state: Number;
  private validatedAt: Date;
  private createdAt: Date;
  private file: File;

  /**
   *
   * @param employee
   * @param label
   * @param total
   * @param tva
   */
  constructor(private employee: Employee, private label: String,  private total: Number,  private tva: Number) {
    super();
    this.setLabel(label);
    this.setTotal(total);
    this.setTva(tva);
    this.setState(STATE_PENDING);
  }

  /**
   *
   * @returns {Number}
   */
  getId(): Number {
    return this.id;
  }

  /**
   *
   * @param id
   */
  setId(id: Number) {
    this.id = id;
  }

  /**
   *
   * @returns {Employee}
   */
  getEmployee(): Employee {
    return this.employee;
  }

  /**
   *
   * @param employee
   */
  setEmployee(employee: Employee) {
    this.employee = employee;
  }

  /**
   *
   * @returns {String}
   */
  getLabel(): String {
    return this.label;
  }

  /**
   *
   * @param label
   */
  setLabel(label: String) {
    this.label = label;
  }

  /**
   *
   * @returns {Number}
   */
  getTotal(): Number {
    return this.total;
  }

  /**
   *
   * @param total
   */
  setTotal(total: Number) {
    this.total = total;
  }

  /**
   *
   * @returns {Number}
   */
  getTva(): Number {
    return this.tva;
  }

  /**
   *
   * @param tva
   */
  setTva(tva: Number) {
    this.tva = tva;
  }

  /**
   *
   * @param description
   */
  setDescription(description: String) {
    this.description = description;
  }

  /**
   *
   * @returns {String}
   */
  getDescription(): String {
    return this.description;
  }

  /**
   *
   * @param image
   */
  setImage(image: String) {
    this.image = image;
  }

  /**
   *
   * @returns {String}
   */
  getImage(): String {
    return this.image;
  }

  getFile(): File {
    return this.file;
  }

  setFile(value: File) {
    this.file = value;
  }

  /**
   *
   * @param dateExpenseAccount
   */
  setDateExpenseAccount(dateExpenseAccount: any) {
    this.date_expense_account = dateExpenseAccount;
  }

  /**
   *
   * @returns {Date}
   */
  getDateExpenseAccount(): Date {
    return this.date_expense_account;
  }

  /**
   *
   * @param state
   */
  setState(state: Number) {
    this.state = state;
  }

  /**
   *
   * @returns {Number}
   */
  getState(): Number {
    return this.state;
  }

  /**
   *
   * @param validatedAt
   */
  setValidatedAt(validatedAt: Date) {
    this.validatedAt = validatedAt
  }

  /**
   *
   * @returns {Date}
   */
  getValidatedAt(): Date {
    return this.validatedAt;
  }

  /**
   *
   * @param createdAt
   */
  setCreatedAt(createdAt: Date) {
    this.createdAt = createdAt;
  }

  /**
   *
   * @returns {Date}
   */
  getCreatedAt(): Date {
    return this.createdAt;
  }

  /**
   *
   * @param model
   * @param json
   * @returns {any}
   */
  hydrateModelFromJSON(model, json): {} {
    model.setId(json.id);
    model.getEmployee().hydrateModelFromJSON(model.getEmployee(), json.employee);
    model.setLabel(json.label);
    model.setDescription(json.description);
    model.setTotal(json.total);
    model.setTva(json.tva);
    model.setImage(json.image);
    model.setDateExpenseAccount(json.date_expense_account);
    model.setValidatedAt(json.validated_at);
    model.setCreatedAt(json.created_at);
    model.setState(json.state);
    return model;

  }
}
