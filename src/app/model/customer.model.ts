import {Injectable} from '@angular/core';
import { ModelInterface } from './common/model.interface';
import { DatePickerOptions, DateModel } from 'ng2-datepicker';

import { Person } from "./person.model";
import { User } from "./user.model";

import {CommonModel} from './common/common.model';

@Injectable()
export class Customer extends Person {

    private siret: string;
    private siren: string;
    private website: string;
    private contactName: string;
    private contactPhone: string;
    private contactEmail: string;
    private generalEmail: string;


    constructor(first_name: String, last_name: String, birthday_date: Date, email: String, address: String, zip_code: Number, city: String, user: User) {
        super(first_name, last_name, birthday_date, email, address, zip_code, city, user);
    }


    getSiret(): string {
        return this.siret;
    }

    setSiret(value: string) {
        this.siret = value;
    }

    getSiren(): string {
        return this.siren;
    }

    setSiren(value: string) {
        this.siren = value;
    }

    getWebsite(): string {
        return this.website;
    }

    setWebsite(value: string) {
        this.website = value;
    }

    getContactName(): string {
        return this.contactName;
    }

    setContactName(value: string) {
        this.contactName = value;
    }

    getContactPhone(): string {
        return this.contactPhone;
    }

    setContactPhone(value: string) {
        this.contactPhone = value;
    }

    getContactEmail(): string {
        return this.contactEmail;
    }

    setContactEmail(value: string) {
        this.contactEmail = value;
    }

    getGeneralEmail(): string {
        return this.generalEmail;
    }

    setGeneralEmail(value: string) {
        this.generalEmail = value;
    }
    /**
     *
     * @param model Customer
     * @param json
     * @returns {null}
     */
    hydrateModelFromJSON(model, json){
        model.setId(json.id);
        model.setSiret(json.siret);
        model.setSiren(json.siren);
        model.setWebsite(json.webiste);
        model.setContactName(json.contact_name);
        model.setContactPhone(json.contact_phone);
        model.setContactEmail(json.contact_email);
        model.setGeneralEmail(json.email);
        //
        return model;
    }



}
