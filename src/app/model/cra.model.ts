import { Injectable } from '@angular/core';
import { ModelInterface } from './common/model.interface';
import { DatePickerOptions, DateModel } from 'ng2-datepicker';
import { Employee } from './employee.model';
import { CommonModel } from './common/common.model';
import { Customer } from "./customer.model";


const CRA_TYPE_JOB_CDD = 1;
const CRA_TYPE_JOB_INTERIM = 2;
const CRA_TYPE_JOB_FREELANCE = 3;

const STATE_PENDING = 1;
const STATE_VALIDATED = 2;
const STATE_DENIED = 3;

@Injectable()
export class Cra extends CommonModel implements ModelInterface {

  private id: Number;
  private title: String;
  private description: String;
  private time_journey: Number;
  private distance: Number;
  private type_job: Number;
  private recorded_at: Date;
  private started_at: Date;
  private ended_at: Date;
  private validated_at: Date;
  private created_at: Date;
  private modified_at: Date;
  private state: Number;
  private idEmployee: Number;
  private idCustomer: Number;
    private employee: Employee;
    private customer: Customer;
 // private customer: Customer;
    private typeJobArray: Array<Object> = [
        {
            id: CRA_TYPE_JOB_CDD,
            label: 'CDD'
        },
        {
            id: CRA_TYPE_JOB_INTERIM,
            label: 'Intérim'
        },
        {
            id: CRA_TYPE_JOB_FREELANCE,
            label: 'Freelance'
        }
    ];



  constructor() {
    super();
    this.setState(STATE_PENDING);
  }

    getId(): Number {
        return this.id;
    }

    setId(value: Number) {
        this.id = value;
    }

    getTitle(): String {
        return this.title;
    }

    setTitle(value: String) {
        this.title = value;
    }

    getDescription(): String {
        return this.description;
    }

    setDescription(value: String) {
        this.description = value;
    }

    getTime_journey(): Number {
        return this.time_journey;
    }

    setTime_journey(value: Number) {
        this.time_journey = value;
    }

    getDistance(): Number {
        return this.distance;
    }

    setDistance(value: Number) {
        this.distance = value;
    }

    getType_job(): Number {
        return this.type_job;
    }

    setType_job(value: Number) {
        this.type_job = value;
    }
    getIdEmployee(): Number {
        return this.idEmployee;
    }

    setIdEmployee(value: Number) {
        this.idEmployee = value;
    }
    getIdCustomer(): Number {
        return this.idCustomer;
    }

    setIdCustomer(value: Number) {
        this.idCustomer = value;
    }
    getRecorded_at(): Date  {
        return this.recorded_at;
    }

    /**
     *
     * @param dateRecorderAt
     */

    setRecorded_at(dateRecorderAt: any) {
        this.recorded_at = dateRecorderAt;
    }

    getStarted_at(): Date {
        return this.started_at;
    }
    /**
     *
     * @param dateSartedAt
     */
    setStarted_at(dateSartedAt: any) {
        this.started_at = dateSartedAt;
    }

    getEnded_at(): Date {
        return this.ended_at;
    }

    setEnded_at(value: any) {
        this.ended_at = value;
    }

    getValidated_at(): Date {
        return this.validated_at;
    }

    setValidated_at(value: any) {
        this.validated_at = value;
    }

    getCreated_at(): Date {
        return this.created_at;
    }

    setCreated_at(value: any) {
        this.created_at = value;
    }

    getModified_at(): Date {
        return this.modified_at;
    }

    setModified_at(value: any) {
        this.modified_at = value;
    }

    getCustomer(): Customer {
        return this.customer;
    }

    setCustomer(value: Customer) {
        this.customer = value;
    }

    getEmployee(): Employee {
        return this.employee;
    }

    setEmployee(value: Employee) {
        this.employee = value;
    }
    getState(): Number {
      return this.state;
    }

    setState(value: Number) {
      this.state = value;
    }

  /**
   *
   * @param model
   * @param json
   * @returns {null}
   */
  hydrateModelFromJSON(model, json ) {
      model.setId(json.id);
      model.setEmployee(json.employee);
      model.setIdEmployee(json.employee.id);
      model.setCustomer(json.customer);
      model.setIdCustomer(json.customer.id);
      model.setTitle(json.title);
      model.setTime_journey(json.time_journey);
      model.setDistance(json.distance);
      model.setType_job(json.type_job);
      model.setRecorded_at(json.recorded_at);
      model.setStarted_at(json.started_at);
      model.setEnded_at(json.ended_at);
      model.setValidated_at(json.validated_at);
      model.setState(json.state);
      model.setDescription(json.description);
    return model;
  }


}
