import {Injectable} from '@angular/core';
import {ModelInterface} from './common/model.interface';
import {CommonModel} from './common/common.model';

@Injectable()
export class JobProfile extends CommonModel implements ModelInterface {

    private id: Number;
    private label: String;
    private description: String;
    private salary: Number;
    private expertise: Number;
    private information: String;
    private published: Boolean;
    private customer: Number;
    private localisation: String;
    private contract_type: String;

    constructor() {
      super();
      this._published = false;
      this._localisation = "";
      this._contract_type = "CDD";
    }

    /**
    *
    * @param model
    * @param json
    * @returns {null}
    */
    hydrateModelFromJSON(model, json): {} {
        model._id = json.id;
        model._customer = json.customer.id;
        model._label = json.label;
        model._description = json.description;
        model._salary = json.salary;
        model._expertise = json.expertise;
        model._information = json.information;
        model._published = json.is_published;
        return model;
    }

    get _localisation(): String {
        return this.localisation;
    }

    set _localisation(value: String) {
        this.localisation = value;
    }

    get _contract_type(): String {
        return this.contract_type;
    }

    set _contract_type(value: String) {
        this.contract_type = value;
    }

    get _id(): Number {
        return this.id;
    }

    set _id(value: Number) {
        this.id = value;
    }

    get _customer(): Number {
        return this.customer;
    }

    set _customer(value: Number) {
        this.customer = value;
    }

    get _label(): String {
        return this.label;
    }

    set _label(value: String) {
        this.label = value;
    }

    get _description(): String {
        return this.description;
    }

    set _description(value: String) {
        this.description = value;
    }

    get _salary(): Number {
        return this.salary;
    }

    set _salary(value: Number) {
        this.salary = value;
    }

    get _expertise(): Number {
        return this.expertise;
    }

    set _expertise(value: Number) {
        this.expertise = value;
    }

    set _information(value: String) {
        this.information = value;
    }

    get _information(): String {
        return this.information;
    }

    set _published(value: Boolean) {
        this.published = value;
    }

    get _published(): Boolean {
        return this.published;
    }
}
