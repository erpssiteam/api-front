import {ModelInterface} from './common/model.interface';
import {CommonModel} from './common/common.model';
import {Offer} from "./offer.model";


const STATE_PENDING = 1;
const STATE_VALIDATED = 2;
const STATE_DENIED = 3;

export class Application extends CommonModel implements ModelInterface {

  private id: Number;
  private cv: string;
  private name: string;
  private address: string;
  private zip_code: string;
  private phone: string;
  private city: string;
  private state: number;
  private offer: Offer;
  private firstname: string;
  private lastname: string;
  private email: string;
  private birthday_date: Date;
  private modifiedAt: Date;
  private createdAt: Date;

  constructor() {
    super();
    this.setState(STATE_PENDING);
  }

  /**
   *
   * @returns {Number}
   */
  getId(): Number {
    return this.id;
  }

  /**
   *
   * @param id
   */
  setId(id: Number) {
    this.id = id;
  }

  getFirstname(): string {
    return this.firstname;
  }

  setFirstname(value: string) {
    this.firstname = value;
  }
title
  getLastname(): string {
    return this.lastname;
  }

  setLastname(value: string) {
    this.lastname = value;
  }

  getEmail(): string {
    return this.email;
  }

  setEmail(value: string) {
    this.email = value;
  }

  getBirthdayDate(): Date {
    return this.birthday_date;
  }

  setBirthdayDate(value: any) {
    this.birthday_date = value;
  }

  getOffer(): Offer {
    return this.offer;
  }

  setOffer(value: Offer) {
    this.offer = value;
  }

  getCv(): string {
    return this.cv;
  }

  setCv(value: string) {
    this.cv = value;
  }

  setName(value: string) {
    this.name = value;
  }

  getName(): string {
    return this.name;
  }

  setAddress(value: string) {
    this.address = value;
  }

  getAddress(): string {
    return this.address;
  }

  setZipcode(value: string) {
    this.zip_code = value;
  }

  getZipcode(): string {
    return this.zip_code;
  }

  setPhone(value: string) {
    this.phone = value;
  }

  getPhone(): string {
    return this.phone;
  }

  setCity(value: string) {
    this.city = value;
  }

  getCity(): string {
    return this.city;
  }

  setState(value: number) {
    this.state = value;
  }

  getState(): number {
    return this.state;
  }

  /**
   *
   * @param createdAt
   */
  setCreatedAt(createdAt: Date) {
    this.createdAt = createdAt;
  }

  /**
   *
   * @returns {Date}
   */
  getModifiedAt(): Date {
    return this.modifiedAt;
  }

  /**
   *
   * @param modifiedAt
   */
  setModifiedAt(modifiedAt: Date) {
    this.modifiedAt = modifiedAt;
  }

  /**
   *
   * @returns {Date}
   */
  getCreatedAt(): Date {
    return this.createdAt;
  }



  /**
   *
   * @param model
   * @param json
   * @returns {any}
   */
  hydrateModelFromJSON(model, json): {} {
    model.setId(json.id);
    model.setCv(json.cv);
    model.setName(json.name);
    model.setFirstname(json.firstname);
    model.setLastname(json.lastname);
    model.setBirthdayDate(json.birthday_date);
    model.setEmail(json.email);
    model.setAddress(json.address);
    model.setZipcode(json.zip_code);
    model.setPhone(json.phone);
    model.setCity(json.city);
    model.setState(json.state);
    const offerModel = new Offer();
    model.setOffer(offerModel.hydrateModelFromJSON(offerModel, json.offer));
    model.setCreatedAt(json.created_at);
    model.setModifiedAt(json.modified_at);
    return model;

  }
}
