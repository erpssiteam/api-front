import {Injectable} from '@angular/core';
import {ModelInterface} from './common/model.interface';
import { DatePickerOptions, DateModel } from 'ng2-datepicker';
import {CommonModel} from './common/common.model';

@Injectable()
export class Categorie extends CommonModel implements ModelInterface {

    private id: Number;
    private label: String;

    constructor() {
      super();
    }

    /**
    *
    * @param model
    * @param json
    * @returns {null}
    */
    hydrateModelFromJSON(model, json): {} {
        model._id = json.id;
        model.label = json.label;
        return model;
    }

    get _label(): String {
        return this.label;
    }

    set _label(value: String) {
        this.label = value;
    }

    get _id(): Number {
        return this.id;
    }

    set _id(value: Number) {
        this.id = value;
    }
}
