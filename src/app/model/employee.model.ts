import {ModelInterface} from "./common/model.interface";
import {Person} from "./person.model";
import {User} from "./user.model";

export class Employee extends Person {

  private cin: string;
  private dt_embauche: string;
  private dt_collaborateur: string;
  private dt_manager: string;
  private situation_familiale: string;
  private customers: any ;
  private gmail: string;

  constructor(first_name: String, last_name: String, birthday_date: Date, email: String, address: String, zip_code: Number, city: String, user: User) {
    super(first_name, last_name, birthday_date, email, address, zip_code, city, user);
  }

  getCin(): string {
    return this.cin;
  }

  setCin(value: string) {
    this.cin = value;
  }

  getDt_embauche(): string {
    return this.dt_embauche;
  }

  setDt_embauche(value: string) {
    this.dt_embauche = value;
  }

  getDt_collaborateur(): string {
    return this.dt_collaborateur;
  }

  setDt_collaborateur(value: string) {
    this.dt_collaborateur = value;
  }

  getDt_manager(): string {
    return this.dt_manager;
  }

  setDt_manager(value: string) {
    this.dt_manager = value;
  }

  getsituation_familiale(): string {
    return this.situation_familiale;
  }

  setsituation_familiale(value: string) {
    this.situation_familiale = value;
  }

  getCustomers(): any {
    return this.customers;
  }
  setCustomers(value: any) {
    this.customers = value;
  }

  getGmail(): any {
    return this.gmail;
  }
  setGmail(value: any) {
    this.gmail = value;
  }

  /**
   *
   * @param model Employee
   * @param json
   * @returns {null}
   */
  hydrateModelFromJSON(model, json){
    model.setId(json.id);
    model.setsituation_familiale(json.situation_familiale);
    model.setDt_embauche(json.date_embauche);
    model.setDt_collaborateur(json.date_collaborater);
    model.setDt_manager(json.date_manager);
    model.setCin(json.cin);
    model.setCustomers(json.customers);
    model.setGmail(json.gmail);
    // employe.last_connect = item.last_connect;
    // employe.image = item.image;
    //
    return model;
  }



}
