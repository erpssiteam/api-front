
import {ModelInterface} from "./common/model.interface";
import {User} from "./user.model";
import {CommonModel} from "./common/common.model";

export class Person extends CommonModel implements ModelInterface {

  protected id: Number;
  protected createdAt: Date;
  protected modifiedAt: Date;
  protected phone: String;

  constructor(protected first_name: String, protected last_name: String, protected birthday_date: Date, protected email: String, protected address: String, protected zip_code: Number, protected city: String, protected user: User) {
    super();
  }

  getFirstname(): String {
    return this.first_name;
  }

  setFirstname(value: String) {
    this.first_name = value;
  }

  getLastname(): String {
    return this.last_name;
  }

  setLastname(value: String) {
    this.last_name = value;
  }

  getBirthdayDate(): Date {
    return this.birthday_date;
  }

  setBirthdayDate(value: Date) {
    this.birthday_date = value;
  }

  getEmail(): String {
    return this.email;
  }

  setEmail(value: String) {
    this.email = value;
  }

  getAddress(): String {
    return this.address;
  }

  setAddress(value: String) {
    this.address = value;
  }

  getZipcode(): Number {
    return this.zip_code;
  }

  setZipcode(value: Number) {
    this.zip_code = value;
  }

  getCity(): String {
    return this.city;
  }

  setCity(value: String) {
    this.city = value;
  }

  getUser(): User {
    return this.user;
  }

  setUser(value: User) {
    this.user = value;
  }

  getId(): Number {
    return this.id;
  }

  setId(value: Number) {
    this.id = value;
  }

  getCreatedAt(): Date {
    return this.createdAt;
  }

  setCreatedAt(value: Date) {
    this.createdAt = value;
  }

  getModifiedAt(): Date {
    return this.modifiedAt;
  }

  setModifiedAt(value: Date) {
    this.modifiedAt = value;
  }

  getPhone(): String {
    return this.phone;
  }

  setPhone(value: String) {
    this.phone = value;
  }

  /**
   *
   * @param model Person
   * @param json
   * @returns {null}
   */
  hydrateModelFromJSON(model, json): {} {
    model.setId(json.id);
    model.setFirstname(json.first_name);
    model.setLastname(json.last_name);
    model.setEmail(json.email);
    model.setBirthdayDate(json.birthday_date);
    model.setAddress(json.address);
    model.setZipcode(json.zip_code);
    model.setCity(json.city);
    // TODO uncomment this comment below when the back will be ready
    // model.setPhone(json.phone);
    model.getUser().hydrateModelFromJSON(model.getUser(), json.user);
    model.setCreatedAt(json.created_at);
    model.setModifiedAt(json.modified_at);
    return model;
  }
}
