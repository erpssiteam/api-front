import {ModelInterface} from "../common/model.interface"
import {CommonModel} from "../common/common.model";

export class CustumerSerialize extends CommonModel implements ModelInterface {

    protected id: Number;
    protected username: String;
    protected password: String;
    protected role: String;
    protected first_name: String;
    protected last_name: String;
    protected birthday_date: String;
    protected address: String;
    protected zip_code: String;
    protected city: String;
    protected siret: String;
    protected siren: String;
    protected website: String;
    protected email: String;
    protected contact_name: String;
    protected contact_phone: String;
    protected contact_email: String;
    protected general_email: String;
    protected createdAt: String;
    protected modifiedAt: String;


    getId(): Number {
        return this.id;
    }

    setId(value: Number) {
        this.id = value;
    }

    getUsername(): String {
        return this.username;
    }

    setUsername(value: String) {
        this.username = value;
    }

    getPassword(): String {
        return this.password;
    }

    setPassword(value: String) {
        this.password = value;
    }
    getRole(): String {
        return this.role;
    }

    setRole(value: String) {
        this.role = value;
    }
    getFirstname(): String {
        return this.first_name;
    }

    setFirstname(value: String) {
        this.first_name = value;
    }

    getLastname(): String {
        return this.last_name;
    }

    setLastname(value: String) {
        this.last_name = value;
    }

    getBirthdayDate(): String {
        return this.birthday_date;
    }

    setBirthdayDate(value: String) {
        this.birthday_date = value;
    }

    getEmail(): String {
        return this.email;
    }

    setEmail(value: String) {
        this.email = value;
    }

    getAddress(): String {
        return this.address;
    }

    setAddress(value: String) {
        this.address = value;
    }

    getZipcode(): String {
        return this.zip_code;
    }

    setZipcode(value: String) {
        this.zip_code = value;
    }

    getCity(): String {
        return this.city;
    }

    setCity(value: String) {
        this.city = value;
    }

    getCreatedAt(): String {
        return this.createdAt;
    }

    setCreatedAt(value: String) {
        this.createdAt = value;
    }

    getModifiedAt(): String {
        return this.modifiedAt;
    }

    setModifiedAt(value: String) {
        this.modifiedAt = value;
    }

    getSiret(): String {
        return this.siret;
    }

    setSiret(value: String) {
        this.siret = value;
    }

    getSiren(): String {
        return this.siren;
    }

    setSiren(value: String) {
        this.siren = value;
    }

    getWebsite(): String {
        return this.website;
    }

    setWebsite(value: String) {
        this.website = value;
    }

    getContact_name(): String {
        return this.contact_name;
    }

    setContact_name(value: String) {
        this.contact_name = value;
    }
    getContact_phone(): String {
        return this.contact_phone;
    }

    setContact_phone(value: String) {
        this.contact_phone = value;
    }
    getContact_email(): String {
        return this.contact_email;
    }

    setContact_email(value: String) {
        this.contact_email = value;
    }

    getGeneral_email(): String {
        return this.general_email;
    }

    setGeneral_email(value: String) {
        this.general_email = value;
    }


    /**
     *
     * @param model Employe
     * @returns {null}
     */
    hydrateModelFromJSON(model): {} {
        let userName = model.user.username;

        // let dateBirthday = ( birthday.getFullYear() + '-' + birthday.getMonth() + 1) + '-' + birthday.getDate()

        let custumerSerialize = new CustumerSerialize();
        custumerSerialize.setId(model.id);
        custumerSerialize.setUsername(userName);
        custumerSerialize.setPassword('secret');
        custumerSerialize.setRole('collaborateur');
        custumerSerialize.setFirstname(model.first_name);
        custumerSerialize.setLastname(model.last_name);
        custumerSerialize.setBirthdayDate('2020-12-21');
        custumerSerialize.setEmail(model.email);
        custumerSerialize.setAddress(model.address);
        custumerSerialize.setZipcode(model.zip_code);
        custumerSerialize.setCity(model.city);
        custumerSerialize.setSiret(model.siret);
        custumerSerialize.setSiren(model.siren);
        custumerSerialize.setWebsite(model.website);
        custumerSerialize.setContact_name(model.contactName);
        custumerSerialize.setContact_phone(model.contactPhone);
        custumerSerialize.setContact_email(model.contactEmail);
        custumerSerialize.setGeneral_email(model.email);3
        // custumerSerialize.setCreatedAt(model.created_at);
        // custumerSerialize.setModifiedAt(model.modified_at);
        return custumerSerialize;
    }
    escapeHtml(text) {
        var map = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#039;'
        };
        return text.replace(/[&<>"']/g, function(m) { return map[m]; });
    }

    // reception api
// {
//     "data" :
//         {
//             "username": "test",
//             "password": "test",
//             "role": "role_admin",
//             "first_name": "test",
//             "last_name": "test",
//             "birthday_date": "1993-10-14",
//             "email": "test@test.fr",
//             "address": "test",
//             "zip_code": "test",
//             "city": "test",
//             "siret": "fe1g1er5641ge6r51g",
//             "siren": "fe1g1er5641ge6r51g",
//             "website": "www.google.ocm",
//             "contact_name": "test",
//             "contact_phone": "test",
//             "contact_email": "test",
//             "general_email": "test"
//         }
// }
}
