import {ModelInterface} from "../common/model.interface"
import {CommonModel} from "../common/common.model";

export class EmployeSerialize extends CommonModel implements ModelInterface {

    protected id: Number;
    protected username: String;
    protected password: String;
    protected role: String;
    protected first_name: String;
    protected last_name: String;
    protected birthday_date: String;
    protected address: String;
    protected zip_code: String;
    protected city: String;
    protected email: String;
    protected gmail: String;
    protected gmail_password: String;
    protected date_embauche: String;
    protected situation_familiale: String;
    protected date_collaborater: String;
    protected date_manager: String;
    protected createdAt: String;
    protected modifiedAt: String;
    protected phone: String;

    getId(): Number {
        return this.id;
    }

    setId(value: Number) {
        this.id = value;
    }

    getUsername(): String {
        return this.username;
    }

    setUsername(value: String) {
        this.username = value;
    }

    getPassword(): String {
        return this.password;
    }

    setPassword(value: String) {
        this.password = value;
    }
    getRole(): String {
        return this.role;
    }

    setRole(value: String) {
        this.role = value;
    }
    getFirstname(): String {
        return this.first_name;
    }

    setFirstname(value: String) {
        this.first_name = value;
    }

    getLastname(): String {
        return this.last_name;
    }

    setLastname(value: String) {
        this.last_name = value;
    }

    getBirthdayDate(): String {
        return this.birthday_date;
    }

    setBirthdayDate(value: String) {
        this.birthday_date = value;
    }

    getEmail(): String {
        return this.email;
    }

    setEmail(value: String) {
        this.email = value;
    }

    getGmail(): String {
        return this.gmail;
    }

    setGmail(value: String) {
        this.gmail = value;
    }
    getEmailPassword(): String {
        return this.gmail_password;
    }

    setEmailPassword(value: String) {
        this.gmail_password = value;
    }

    getAddress(): String {
        return this.address;
    }

    setAddress(value: String) {
        this.address = value;
    }

    getZipcode(): String {
        return this.zip_code;
    }

    setZipcode(value: String) {
        this.zip_code = value;
    }

    getCity(): String {
        return this.city;
    }

    setCity(value: String) {
        this.city = value;
    }

    getCreatedAt(): String {
        return this.createdAt;
    }

    setCreatedAt(value: String) {
        this.createdAt = value;
    }

    getModifiedAt(): String {
        return this.modifiedAt;
    }

    setModifiedAt(value: String) {
        this.modifiedAt = value;
    }

    getPhone(): String {
        return this.phone;
    }

    setPhone(value: String) {
        this.phone = value;
    }

    getDate_embauche(): String {
        return this.date_embauche;
    }

    setDate_embauche(value: String) {
        this.date_embauche = value;
    }

    getDate_collaborater(): String {
        return this.date_collaborater;
    }

    setDate_collaborater(value: String) {
        this.date_collaborater = value;
    }

    getDate_manager(): String {
        return this.date_manager;
    }

    setDate_manager(value: String) {
        this.date_manager = value;
    }

    getSituation_familiale(): String {
        return this.situation_familiale;
    }

    setSituation_familiale(value: String) {
        this.situation_familiale = value;
    }


    /**
     *
     * @param model Employe
     * @returns {null}
     */
    hydrateModelFromJSON(model): {} {
        let birthday = model.birthday_date;
        let dateCollaborateur = model.dt_collaborateur;
        let dateManager = model.dt_manager;
        let dateEmbauche = model.dt_embauche;
        let userName = model.user.username;

        // let dateBirthday = ( birthday.getFullYear() + '-' + birthday.getMonth() + 1) + '-' + birthday.getDate()
        if(null != dateCollaborateur){
             dateCollaborateur = dateEmbauche.toString('yyyy-MM-dd')
        }else{
            dateCollaborateur = null
        }
        if(null != dateManager){
            dateManager = dateEmbauche.toString('yyyy-MM-dd')
        }else{
            dateManager = null
        }
        if(null != dateEmbauche){
            dateEmbauche = dateEmbauche.toString('yyyy-MM-dd')
        }else{
            dateEmbauche = null
        }
        if("" != userName){
            userName = userName;
        }else{
            userName = model.first_name;
            dateEmbauche = new Date() ;
            dateEmbauche = dateEmbauche.toString('yyyy-MM-dd');
        }
        let employeSerialize = new EmployeSerialize();
        employeSerialize.setId(model.id);
        employeSerialize.setUsername(userName);
        employeSerialize.setPassword(model.password);
        employeSerialize.setRole(model.role);
        employeSerialize.setFirstname(model.first_name);
        employeSerialize.setLastname(model.last_name);
        employeSerialize.setBirthdayDate('2020-12-21');
        employeSerialize.setEmail(model.email);
        employeSerialize.setGmail(model.gmail);
        employeSerialize.setEmailPassword(model.password);
        employeSerialize.setAddress(model.address);
        employeSerialize.setZipcode(model.zip_code);
        employeSerialize.setCity(model.city);
        employeSerialize.setPhone(model.phone);
        employeSerialize.setSituation_familiale(model.situation_familiale);
        employeSerialize.setDate_collaborater(dateCollaborateur);
        employeSerialize.setDate_manager(dateManager);
        employeSerialize.setDate_embauche(dateEmbauche);
        // employeSerialize.setCreatedAt(model.created_at);
        // employeSerialize.setModifiedAt(model.modified_at);
        return employeSerialize;
    }

    escapeHtml(text) {
        let map = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#039;'
        };

        return text.replace(/[&<>"']/g, function(m) { return map[m]; });
    }
}
