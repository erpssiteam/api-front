import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/switchMap';

@Component({
    selector: 'app-errors',
    templateUrl: './errors.component.html',
    styleUrls: ['./errors.component.css']
})
export class ErrorsComponent implements OnInit {
    code:Number = 0
    message:String = ""
    private _messages = [
        {'code':401,'message':'Unauthorized'},
        {'code':403,'message':'Forbidden'},
        {'code':404,'message':'Not Found'},
        {'code':500,'message':'Internal Server Error'},
        {'code':503,'message':'Service Unavailable'},
        {'code':504,'message':'Gateway Time-out'},
    ];

    constructor(private _route: ActivatedRoute) {

    }

    ngOnInit() {

        this._route.queryParams.subscribe(params => {
            this.transformMessage(+params['code'] || 404, params['message'] || "");
        });
    }

    transformMessage(code:Number, message:String){

        for (let entry of this._messages) {

            // If the code exists
            if(entry.code == code){
                this.code = entry.code
                this.message = entry.message || message
            }
        }

        // if the code not exists, we show the parameter code and parameter message
        if (this.code == 0) {
            this.code = code
            this.message = message
        }
    }
}
