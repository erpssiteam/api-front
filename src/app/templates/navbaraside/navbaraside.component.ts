import { Component, OnInit } from '@angular/core';
import {CommonProvider} from "../../services/common/common-provider.service";
import {Employee} from "../../model/employee.model";
import { CookiesTokenService } from '../../services/cookies/cookies-token.service';

const FEATURE_GROUPWARE = 'feature_groupware';

declare var jQuery:any;
@Component({
    selector: 'app-navbaraside',
    templateUrl: './navbaraside.component.html',
    styleUrls: ['./navbaraside.component.css'],
    providers: [CookiesTokenService, CommonProvider]

})
export class NavbarasideComponent implements OnInit {

    private employee: Employee;
    private role: string;
    private voterGroupware:Boolean;

    constructor( private _cookiesTokenService:CookiesTokenService, private commonProvider: CommonProvider) {
        this.employee = this.commonProvider.getCurrentEmployee();
        this.voterGroupware = this.commonProvider.voteFactory(FEATURE_GROUPWARE);
    }

    ngOnInit() {
        let employee = this._cookiesTokenService.getSessionJSON('employee');
        this.role = employee.user.role[0];
    }
}
