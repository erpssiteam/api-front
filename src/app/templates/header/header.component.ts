import { Component, OnInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { CookiesTokenService } from '../../services/cookies/cookies-token.service';
import {CommonProvider} from "../../services/common/common-provider.service";
import {Employee} from "../../model/employee.model";
import {User} from "../../model/user.model";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: [CookiesTokenService, CommonProvider]
})
export class HeaderComponent implements OnInit {

  private  title = null;
  private  famille = null;
  private  container = null;
  private employee: Employee;

  constructor( myElement: ElementRef, private _router:Router, private _cookiesTokenService:CookiesTokenService, private commonProvider: CommonProvider){
    this.title = "site";
    this.famille = "site";
    this.container = myElement;
    this.employee = this.commonProvider.getCurrentEmployee();
    this.inialisation();
  }
  inialisation = function(){

    // $('main').addClass(this.famille);
    // $('main').removeClass("connexion");

    let employee = this._cookiesTokenService.getSessionJSON('employee');
    this.username = employee.first_name+" "+employee.last_name
  }

  ngOnInit() {
  }

  logOut(){
    this._cookiesTokenService.deleteSession('token');
    this._cookiesTokenService.deleteSession('employee');
    this._router.navigate(['/login']);
  }

}
