import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Http, HttpModule } from '@angular/http';
import { TranslateModule, TranslateLoader, TranslateStaticLoader } from 'ng2-translate/ng2-translate';
import { FileSelectDirective  } from 'ng2-file-upload';
import { CKEditorModule } from 'ng2-ckeditor';
import { DatePickerModule } from "ng2-datepicker";
import { RouteReuseStrategy } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { routing, appRoutingProviders }  from './app.routing';

import { HttpApiErpssiService } from './services/httpApiErpssi/http-api-erpssi.service' ;
import { CookiesTokenService } from './services/cookies/cookies-token.service';
import { AuthGuard } from './services/authGuard/auth-guard.service';
import { TruncatePipe } from './pipes/truncate.pipe';

import { LoginsService } from './services/login/logins.service';

import { MaterializeModule } from 'angular2-materialize';

import { AppComponent } from './app.component';
import { FooterComponent } from './templates/footer/footer.component';
import { ErrorsComponent } from './templates/errors/errors.component';
import { HeaderComponent } from './templates/header/header.component';

import { LoginComponent } from './components/login/login.component';
import { NavbarasideComponent } from './templates/navbaraside/navbaraside.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { EmailComponent } from './components/email/email.component';
import { EmailcreateComponent } from './components/email/emailcreate/emailcreate.component';
import { EmailviewComponent } from './components/email/emailview/emailview.component';
import { EmailsidebarComponent } from './components/email/emailsidebar/emailsidebar.component';

import { ExpenseAccountUpdateComponent } from './components/expense-account/update/expense-account-update.component';
import { ExpenseAccountManagedListComponent } from './components/expense-account/list/managed-list/expense-account-managed-list.component';
import { ExpenseAccountCreateComponent } from './components/expense-account/create/expense-account-create.component';
import { ExpenseAccountFormComponent } from './components/expense-account/form/expense-account-form.component';
import { ExpenseAccountSelfListComponent } from './components/expense-account/list/self-list/expense-account-self-list.component';
import { ExpenseAccountCommonListComponent } from './components/expense-account/list/common-list/expense-account-common-list.component';
import { ExpenseAccountRootComponent } from './components/expense-account/root/expense-account-root.component';

import { HolidaysListComponent } from './components/holidays/list/holidays-list.component';
import { HolidaysUpdateComponent } from './components/holidays/update/holidays-update.component';
import { HolidaysCreateComponent } from './components/holidays/create/holidays-create.component';
import { HolidaysFormComponent } from './components/holidays/form/holidays-form.component';
import { GroupwareListComponent } from './components/groupware/list/groupware-list.component';

import { JobProfileListComponent } from './components/jobprofile/list/jobprofile-list.component';
import { JobProfileUpdateComponent } from './components/jobprofile/update/jobprofile-update.component';
import { JobProfileCreateComponent } from './components/jobprofile/create/jobprofile-create.component';
import { JobProfileFormComponent } from './components/jobprofile/form/jobprofile-form.component';

import { OfferListComponent } from './components/offer/list/offer-list.component';
import { OfferUpdateComponent } from './components/offer/update/offer-update.component';
import { OfferCreateComponent } from './components/offer/create/offer-create.component';
import { OfferFormComponent } from './components/offer/form/offer-form.component';

import { EmployeeCreateComponent } from './components/employes/create/employee-create.component';
import { EmployeeFormComponent } from './components/employes/form/employee-form.component';
import { EmployeeUpdateComponent } from './components/employes/update/employee-update.component';
import { EmployesComponent } from './components/employes/list/employes.component';
import { EmployeViewComponent } from './components/employes/view/employe-view.component';

import { CustomersCreateComponent } from './components/customers/create/customers-create.component';
import { CustomersListComponent } from './components/customers/list/customers-list.component';
import { CustomersFormComponent } from './components/customers/form/customers-form.component';
import { CustomersUpdateComponent } from './components/customers/update/customers-update.component';
import { NewsCreateComponent } from './components/news/create/news-create.component';
import { NewsFormComponent } from './components/news/form/news-form.component';
import { NewsUpdateComponent } from './components/news/update/news-update.component';
import { NewsListComponent } from './components/news/list/news-list.component';
import { NewsViewComponent } from './components/news/view/news-view.component';

import { ProfileFormComponent } from './components/profile/form/profile-form.component';

import { CrasFormComponent } from './components/cras/form/cras-form.component';
import { CrasRootComponent } from './components/cras/root/cras.root.component';
import { CrasManagedListComponent } from './components/cras/list/managed-list/cras-managed-list.component';
import { CrasCommonListComponent } from './components/cras/list/common-list/cras-common-list.component';
import { CrasSelfListComponent } from  './components/cras/list/self-list/cras-self-list.component';
import { CrasCreateComponent } from './components/cras/create/cras-create.component';
import { CrasUpdateComponent } from './components/cras/update/cras-update.component';
import { CrasViewComponent } from './components/cras/view/cras-view.component';

import { ApplicationListComponent } from './components/application/list/application-list.component';
import { ApplicationCreateComponent } from './components/application/create/application-create.component';
import { ApplicationFormComponent } from './components/application/form/application-form.component';
import { ApplicationUpdateComponent } from './components/application/update/application-update.component';
import { ApplicationViewComponent } from './components/application/view/application-view.component';
import { SkillListComponent } from './components/skill/list/skill-list.component';
import { SkillCreateComponent } from './components/skill/create/skill-create.component';
import { SkillUpdateComponent } from './components/skill/update/skill-update.component';
import { SkillRootComponent } from './components/skill/root/skill-root.component';
import { SkillFormComponent } from './components/skill/form/skill-form.component';
import { SkillEmployeeComponent } from './components/skill/skill-employee/skill-employee.component';
import { CvListComponent } from './components/cv/list/cv-list.component';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';

export function createTranslateLoader(http: Http) {
  return new TranslateStaticLoader(http, './assets/i18n', '-lang.json');
}

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    FileSelectDirective,
    LoginComponent,
    NavbarasideComponent,
    DashboardComponent,
    EmailComponent,
    EmailcreateComponent,
    EmailviewComponent,
    EmailsidebarComponent,
    ErrorsComponent,
    ExpenseAccountManagedListComponent,
    ExpenseAccountSelfListComponent,
    ExpenseAccountRootComponent,
    ExpenseAccountCommonListComponent,
    ExpenseAccountUpdateComponent,
    ExpenseAccountCreateComponent,
    ExpenseAccountFormComponent,
    ExpenseAccountRootComponent,
    HolidaysListComponent,
    HolidaysUpdateComponent,
    HolidaysCreateComponent,
    HolidaysFormComponent,
    EmployeeCreateComponent,
    EmployeeFormComponent,
    EmployeeUpdateComponent,
    EmployesComponent,
    EmployeViewComponent,
    GroupwareListComponent,
    CustomersCreateComponent,
    CustomersListComponent,
    CustomersFormComponent,
    CustomersUpdateComponent,
    CrasFormComponent,
    CrasManagedListComponent,
    CrasCommonListComponent,
    CrasSelfListComponent,
    CrasCreateComponent,
    CrasUpdateComponent,
    CrasViewComponent,
    NewsCreateComponent,
    NewsFormComponent,
    NewsUpdateComponent,
    NewsListComponent,
    NewsViewComponent,
    ApplicationListComponent,
    ApplicationCreateComponent,
    ApplicationFormComponent,
    ApplicationUpdateComponent,
    ApplicationViewComponent,
    CrasRootComponent,
    ProfileFormComponent,
    JobProfileListComponent,
    JobProfileUpdateComponent,
    JobProfileCreateComponent,
    JobProfileFormComponent,
    OfferListComponent,
    OfferUpdateComponent,
    OfferCreateComponent,
    OfferFormComponent,
    ApplicationViewComponent,
    TruncatePipe,
    SkillListComponent,
    SkillCreateComponent,
    SkillUpdateComponent,
    SkillRootComponent,
    SkillFormComponent,
    SkillEmployeeComponent,
    CvListComponent,
    BreadcrumbComponent
  ],
  imports: [
    DatePickerModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterializeModule,
    CKEditorModule,
    ReactiveFormsModule,
    routing,
    // FlashMessagesModule,
    TranslateModule.forRoot({
      provide: TranslateLoader,
      useFactory: (createTranslateLoader),
      deps: [Http]
    })
  ],
  providers: [
    appRoutingProviders,
    HttpApiErpssiService,
    CookiesTokenService,
    LoginsService,
    AuthGuard,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
