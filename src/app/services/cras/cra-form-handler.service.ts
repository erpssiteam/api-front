import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import {CommonProvider} from '../common/common-provider.service';
import {Http} from '@angular/http';
import { Router } from '@angular/router';
import {CookiesTokenService} from '../cookies/cookies-token.service';
import {DateModel, DatePickerOptions} from "ng2-datepicker";

@Injectable()
export class CraFormHandler extends CommonProvider {

    formErrors = {
        'startedat': '',
        'endedat': ''
    };
    validationMessages = {
        'startedat': {
            'required':      'La date est obligatoire.',
        },
        'endedat': {
            'required':      'La date est obligatoire.',
        },
        'reason': {
            'required': 'La raison est obligatoire'
        }
    };
    configDatePicker = {
        format: 'DD-MM-YYYY',
        initialDate: new Date(),
        locale: 'fr'
    };

    constructor(http: Http, cookiesTokenService: CookiesTokenService, private _router:Router) {
        super(http, cookiesTokenService,_router);
    };


}
