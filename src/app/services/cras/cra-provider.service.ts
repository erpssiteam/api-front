
import { Injectable } from '@angular/core';
import { Http }          from '@angular/http';
import { Router } from '@angular/router';

import 'rxjs/add/operator/toPromise';
import {Holiday} from '../../model/holiday.model';
import { Cra } from '../../model/cra.model';
import {CommonProvider} from "../common/common-provider.service";
import {CookiesTokenService} from "../cookies/cookies-token.service";

import {Employee} from '../../model/employee.model';
import {Customer} from '../../model/customer.model';
import {User} from '../../model/user.model';
import {Role} from '../../model/role.model';

const DATA_URI = 'http://erpssi.sandbox.skilvioo.com/api/cras';
const DATA_URI_PARAM = 'http://erpssi.sandbox.skilvioo.com/api/cras/';

@Injectable()
export class CraProvider extends CommonProvider{

    private crasMap = [];

    constructor(private _router: Router, http: Http, cookiesTokenService: CookiesTokenService) {
        super(http, cookiesTokenService, _router);
    };

    getAll(): Promise<Cra[]> {
        return new Promise((resolve, reject) => {
            if(this.crasMap.length == 0) {
                this.http.get(DATA_URI, { headers: this.getHeader() }).toPromise().then(response => {
                    let json = JSON.parse(response['_body']).data;
                    let cras = [];
                    for(let k in json) {
                        if(json.hasOwnProperty(k)) {
                            cras.push(CraProvider.createCrasModel(json[k]));
                        }
                    }
                    this.crasMap = cras;
                    resolve(this.crasMap);
                }).catch(error => {
                    this.error(error)
                });
            } else {
                resolve(this.crasMap);
            }
        });
    }

    get(id: Number): Promise<Cra[]> {
        return new Promise((resolve, reject) => {
            this.http.get(DATA_URI_PARAM + id, { headers: this.getHeader() }).toPromise().then(response => {
                let json = JSON.parse(response['_body']).data;
                let craModel = CraProvider.createCrasModel(json);
                resolve(craModel);
            }).catch(error => {
                this.error(error)
            });
        });
    }

    remove(id: Number): Promise<Cra[]> {
        return new Promise((resolve, reject) => {
            this.http.delete(DATA_URI_PARAM + id, { headers: this.getHeader() }).toPromise().then(response => {
                // toast with response message
                this.crasMap.forEach((h, index) => {
                    if(h.id === id) {
                        this.crasMap.splice(index, 1);
                    }
                });
            }).catch(error => {
                this.error(error)
            });
            resolve(this.crasMap);
        });
    }
    getAllByEmployee(): Promise<Cra[]> {
        return new Promise((resolve, reject) => {
            if (this.crasMap.length === 0) {
                this.http.get(DATA_URI_PARAM + 'employee/' + this.getCurrentEmployee().getId(), { headers: this.getHeader() }).toPromise().then(response => {
                    resolve(this.parseJson(response));
                });
            } else {
                resolve(this.crasMap);
            }
        });
    };
    post(cra: Cra): Promise<Cra> {
        return new Promise((resolve, reject) => {
            this.http.post(DATA_URI_PARAM +"employee/" + this.getCurrentUserId() +"/customer/" + cra.getIdCustomer(), { data: cra }, { headers: this.getHeader() }).toPromise().then(response => {
                let json = JSON.parse(response['_body']).data;
                let craModel = CraProvider.createCrasModel(json);
                this.crasMap.push(craModel);
                resolve(this.crasMap);
            }).catch(error => {
                this.error(error)
            });
        });
    }
    put(cra: Cra): Promise<Cra> {
        return new Promise((resolve, reject) => {
                this.http.put(DATA_URI_PARAM + cra.getId()+'/employee/'+cra.getIdEmployee()+'/customer/'+ cra.getIdCustomer(), { data: cra }, { headers: this.getHeader() }).toPromise().then(response => {
                let json = JSON.parse(response['_body']).data;
                cra.hydrateModelFromJSON(cra, json);
                resolve(cra);
            }).catch(error => {
                this.error(error)
            });
        });
    }
    validate(state:number, cra: Cra): Promise<Cra[]> {
        return new Promise((resolve, reject) => {
            this.http.put(DATA_URI_PARAM + cra.getId() + '/handle/' + state, {}, { headers: this.getHeader() }).toPromise().then(response => {
                let json = JSON.parse(response['_body']).data;
                cra.hydrateModelFromJSON(cra, json);
                this.crasMap.forEach((n, index) => {
                    if (n.getId() === cra.getId()) {
                        this.crasMap[index] = cra;
                    }
                });
                resolve(this.crasMap);
            });
        });
    };
    private static createCrasModel(json: any): Cra {
        let role = new Role(json.employee.user.role);
        let user = new User(json.employee.user.username, json.employee.user.username, [role]);
        let employee = new Employee(json.employee.first_name, json.employee.last_name, json.employee.birthday_date, json.employee.email, json.employee.address, json.employee.zip_code, json.employee.city, user);
        let roleCustomer = new Role(json.customer.user.role);
        let userCustomer = new User(json.customer.user.username, json.customer.user.username, [roleCustomer]);
        let customer = new Customer(json.customer.first_name, json.customer.last_name, json.customer.birthday_date, json.customer.email, json.customer.address, json.customer.zip_code, json.customer.city, userCustomer);
        let craModel = new Cra();
        craModel.hydrateModelFromJSON(craModel, json);
        return craModel;
    }
    private parseJson(response: any): any {
        let json = JSON.parse(response['_body']).data;
        let cras = [];
        for (let k in json) {
            if (json.hasOwnProperty(k)) {
                cras.push(CraProvider.createCrasModel(json[k]));
            }
        }
        this.crasMap = cras;
        return this.crasMap;
    };
}

