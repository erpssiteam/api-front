import { Injectable } from '@angular/core';
import { Http }          from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import { CommonProvider } from '../common/common-provider.service';
import { CookiesTokenService } from '../cookies/cookies-token.service';
import {News} from "../../model/news.model";

const NEWS_DATA_URI = 'http://erpssi.sandbox.skilvioo.com/api/news';
const NEWS_DATA_URI_PARAM = 'http://erpssi.sandbox.skilvioo.com/api/news/';

@Injectable()
export class NewsProvider extends CommonProvider {

  private newsMap = [];

  constructor(http: Http, cookiesTokenService: CookiesTokenService, router : Router) {
    super(http, cookiesTokenService, router);
  };

  getAll(): Promise<News[]> {
    return new Promise((resolve, reject) => {
      if (this.newsMap.length === 0) {
        this.http.get(NEWS_DATA_URI + '/list', { headers: this.getHeader() }).toPromise().then(response => {
          resolve(this.parseJson(response));
        });
      } else {
        resolve(this.newsMap);
      }
    });
  };

  get(id: Number): Promise<News> {
    return new Promise((resolve, reject) => {
      this.http.get(NEWS_DATA_URI_PARAM + id, { headers: this.getHeader() }).toPromise().then(response => {
        let json = JSON.parse(response['_body']).data;
        let newsModel = NewsProvider.createNewsModel(json);
        resolve(newsModel);
      });
    });
  };

  remove(id: Number): Promise<News[]> {
    return new Promise((resolve, reject) => {
      this.http.delete(NEWS_DATA_URI_PARAM + id, { headers: this.getHeader() }).toPromise().then(response => {
        // toast with response message
        this.newsMap.forEach((n, index) => {
          if (n.getId() === id) {
            this.newsMap.splice(index, 1);
          }
        });
      });
      resolve(this.newsMap);
    });
  };

  post(news: News): Promise<News> {
    return new Promise((resolve, reject) => {
      this.http.post(NEWS_DATA_URI, { data: news }, { headers: this.getHeader() }).toPromise().then(response => {
          let json = JSON.parse(response['_body']).data;
          let newsModel = NewsProvider.createNewsModel(json);
          this.newsMap.push(newsModel);
          resolve(this.newsMap);
      });
    });
  };

  put(news: News): Promise<News> {
    return new Promise((resolve, reject) => {
      this.http.put(NEWS_DATA_URI_PARAM + news.getId(), { data: news }, { headers: this.getHeader() }).toPromise().then(response => {
        let json = JSON.parse(response['_body']).data;
        news.hydrateModelFromJSON(news, json);
        resolve(news);
      });
    });
  };

  private static createNewsModel(json: any): News {
    let news = new News(json.title, json.description, json.date_news);
    news.hydrateModelFromJSON(news, json);
    return news;
  };

  private parseJson(response: any): any {
    let json = JSON.parse(response['_body']).data;
    let news = [];
    for (let k in json) {
      if (json.hasOwnProperty(k)) {
        news.push(NewsProvider.createNewsModel(json[k]));
      }
    }
    this.newsMap = news;
    return this.newsMap;
  };

}
