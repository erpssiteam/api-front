import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import {CommonProvider} from '../common/common-provider.service';
import {Http} from '@angular/http';
import {CookiesTokenService} from '../cookies/cookies-token.service';
import {DateModel, DatePickerOptions} from "ng2-datepicker";
import {Router} from "@angular/router";

const NEWS_DATA_URI_PARAM = 'http://erpssi.sandbox.skilvioo.com/api/news/';

@Injectable()
export class NewsFormHandler extends CommonProvider {

  formErrors = {
    'title': '',
    'content': '',
    'date_news': '',
  };
  validationMessages = {
    'title': {
      'required':      'Le titre est obligatoire.',
      'minlength':     'Vous devez saisir un titre qui possède au moins 3 caractères',
    },
  };
  filePlaceholder = 'Choisir un justificatif...';
  configDatePicker = {
    format: 'DD-MM-YYYY',
    initialDate: new Date(),
    locale: 'fr'
  };

  constructor(http: Http, cookiesTokenService: CookiesTokenService, router: Router) {
    super(http, cookiesTokenService, router);
  };

}
