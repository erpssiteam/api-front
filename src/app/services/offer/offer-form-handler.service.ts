import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import {CommonProvider} from '../common/common-provider.service';
import {Http} from '@angular/http';
import { Router } from '@angular/router';
import {CookiesTokenService} from '../cookies/cookies-token.service';
import {DateModel, DatePickerOptions} from "ng2-datepicker";

@Injectable()
export class OfferFormHandler extends CommonProvider {

    public formErrors = {
        'startat': '',
        'endat': ''
    };
    public validationMessages = {
        'startat': {
            'required':      'La date est obligatoire.',
        },
        'endat': {
            'required':      'La date est obligatoire.',
        },
    };
    public configStartAt = {
        format: 'DD-MM-YYYY',
        initialDate: new Date(),
        locale: 'fr'
    };
    public configEndAt = {
        format: 'DD-MM-YYYY',
        initialDate: new Date(),
        locale: 'fr'
    };

    constructor(private _router:Router, http: Http, cookiesTokenService: CookiesTokenService) {
        super(http, cookiesTokenService, _router);
    };
}
