import { Injectable } from '@angular/core';
import { Http }          from '@angular/http';
import { Router } from '@angular/router';

import 'rxjs/add/operator/toPromise';
import {Offer} from '../../model/offer.model';
import {CommonProvider} from "../common/common-provider.service";
import {CookiesTokenService} from "../cookies/cookies-token.service";

const OFFER_DATA_URI_LIST = 'http://erpssi.sandbox.skilvioo.com/api/offers/list';
const OFFER_DATA_URI_PARAM_LIST = 'http://erpssi.sandbox.skilvioo.com/api/offers/list/';
const OFFER_DATA_URI = 'http://erpssi.sandbox.skilvioo.com/api/offers';
const OFFER_DATA_URI_PARAM = 'http://erpssi.sandbox.skilvioo.com/api/offers/';

@Injectable()
export class OfferProvider extends CommonProvider{

    private offerMap = [];

    constructor(private _router: Router, http: Http, cookiesTokenService: CookiesTokenService) {
        super(http, cookiesTokenService, _router);
    };

    getAll(): Promise<Offer[]> {
        return new Promise((resolve, reject) => {
            if(this.offerMap.length == 0) {
                this.http.get(OFFER_DATA_URI_LIST, { headers: this.getHeader() }).toPromise().then(response => {
                    let json = JSON.parse(response['_body']).data;
                    let offer = [];
                    for(let k in json) {
                        if(json.hasOwnProperty(k)) {
                            offer.push(OfferProvider.createOfferModel(json[k]));
                        }
                    }
                    this.offerMap = offer;
                    resolve(this.offerMap);
                }).catch(error => {
                    this.error(error)
                });
            } else {
                resolve(this.offerMap);
            }
        });
    }

    get(id: Number): Promise<Offer[]> {
        return new Promise((resolve, reject) => {
            this.http.get(OFFER_DATA_URI_PARAM + id, { headers: this.getHeader() }).toPromise().then(response => {
                let json = JSON.parse(response['_body']).data;
                let offerModel = OfferProvider.createOfferModel(json);
                resolve(offerModel);
            }).catch(error => {
                this.error(error)
            });
        });
    }

    remove(id: Number): Promise<Offer[]> {
        return new Promise((resolve, reject) => {
            this.http.delete(OFFER_DATA_URI_PARAM + id, { headers: this.getHeader() }).toPromise().then(response => {
                // toast with response message
                this.offerMap.forEach((jb, index) => {
                    if(jb.id === id) {
                        this.offerMap.splice(index, 1);
                    }
                });
            }).catch(error => {
                this.error(error)
            });
            resolve(this.offerMap);
        });
    }

    post(offer: Offer): Promise<Offer> {
        return new Promise((resolve, reject) => {
            this.http.post(OFFER_DATA_URI_PARAM + offer._job_profile, { data: offer }, { headers: this.getHeader() }).toPromise().then(response => {
                let json = JSON.parse(response['_body']).data;
                let offerModel = OfferProvider.createOfferModel(json);
                this.offerMap.push(offerModel);
                resolve(this.offerMap);
            }).catch(error => {
                this.error(error)
            });
        });
    }

    put(offer: Offer): Promise<Offer> {
        return new Promise((resolve, reject) => {
            this.http.put(OFFER_DATA_URI_PARAM + offer._id, { data: offer }, { headers: this.getHeader() }).toPromise().then(response => {
                let json = JSON.parse(response['_body']).data;
                offer.hydrateModelFromJSON(offer, json);
                resolve(offer);
            }).catch(error => {
                this.error(error)
            });
        });
    }

    private static createOfferModel(json: any): Offer {
        let offerModel = new Offer();
        offerModel.hydrateModelFromJSON(offerModel, json);
        return offerModel;
    }
}
