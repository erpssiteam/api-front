import { Injectable } from '@angular/core';
import { Http }          from '@angular/http';
import { Router } from '@angular/router';

import 'rxjs/add/operator/toPromise';
import {Holiday} from '../../model/holiday.model';
import {CommonProvider} from "../common/common-provider.service";
import {CookiesTokenService} from "../cookies/cookies-token.service";

const HOLIDAYS_DATA_URI = 'http://erpssi.sandbox.skilvioo.com/api/holidays';
const HOLIDAYS_DATA_URI_PARAM = 'http://erpssi.sandbox.skilvioo.com/api/holidays/';

@Injectable()
export class HolidaysProvider extends CommonProvider{

    private holidaysMap = [];

    constructor(private _router: Router, http: Http, cookiesTokenService: CookiesTokenService) {
        super(http, cookiesTokenService, _router);
    };

    getAll(): Promise<Holiday[]> {
        return new Promise((resolve, reject) => {
            if(this.holidaysMap.length == 0) {
                this.http.get(HOLIDAYS_DATA_URI, { headers: this.getHeader() }).toPromise().then(response => {
                    let json = JSON.parse(response['_body']).data;
                    let holidays = [];
                    for(let k in json) {
                        if(json.hasOwnProperty(k)) {
                            holidays.push(HolidaysProvider.createHolidaysModel(json[k]));
                        }
                    }
                    this.holidaysMap = holidays;
                    resolve(this.holidaysMap);
                }).catch(error => {
                    this.error(error)
                });
            } else {
                resolve(this.holidaysMap);
            }
        });
    }

    get(id: Number): Promise<Holiday[]> {
        return new Promise((resolve, reject) => {
            this.http.get(HOLIDAYS_DATA_URI_PARAM + id, { headers: this.getHeader() }).toPromise().then(response => {
                let json = JSON.parse(response['_body']).data;
                let holidayModel = HolidaysProvider.createHolidaysModel(json);
                resolve(holidayModel);
            }).catch(error => {
                this.error(error)
            });
        });
    }

    remove(id: Number): Promise<Holiday[]> {
        return new Promise((resolve, reject) => {
            this.http.delete(HOLIDAYS_DATA_URI_PARAM + id, { headers: this.getHeader() }).toPromise().then(response => {
                // toast with response message
                this.holidaysMap.forEach((h, index) => {
                    if(h.id === id) {
                        this.holidaysMap.splice(index, 1);
                    }
                });
            }).catch(error => {
                this.error(error)
            });
            resolve(this.holidaysMap);
        });
    }

    post(holiday: Holiday): Promise<Holiday> {
        return new Promise((resolve, reject) => {
            this.http.post(HOLIDAYS_DATA_URI_PARAM  + this.getCurrentUserId(), { data: holiday }, { headers: this.getHeader() }).toPromise().then(response => {
                let json = JSON.parse(response['_body']).data;
                let holidayModel = HolidaysProvider.createHolidaysModel(json);
                this.holidaysMap.push(holidayModel);
                resolve(this.holidaysMap);
            }).catch(error => {
                this.error(error)
            });
        });
    }

    put(holiday: Holiday): Promise<Holiday> {
        return new Promise((resolve, reject) => {
            this.http.put(HOLIDAYS_DATA_URI_PARAM + holiday._id, { data: holiday }, { headers: this.getHeader() }).toPromise().then(response => {
                let json = JSON.parse(response['_body']).data;
                holiday.hydrateModelFromJSON(holiday, json);
                resolve(holiday);
            }).catch(error => {
                this.error(error)
            });
        });
    }

    private static createHolidaysModel(json: any): Holiday {
        let holidayModel = new Holiday();
        holidayModel.hydrateModelFromJSON(holidayModel, json);
        return holidayModel;
    }
}
