import { Injectable } from '@angular/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Injectable()
export class CookiesTokenService {

    constructor() { }

    getToken(){

        if(localStorage.getItem('token') != undefined && localStorage.getItem('token') != null && localStorage.getItem('token') != ""){
            return JSON.parse(localStorage.getItem('token'));
        } else {
            return null
        }
    }

    getSession(title:string){

        if(localStorage.getItem(title) != undefined && localStorage.getItem(title) != null && localStorage.getItem(title) != ""){
            return localStorage.getItem(title)
        } else {
            return null
        }
    }

    getSessionJSON(title:string){

        if(localStorage.getItem(title) != undefined && localStorage.getItem(title) != null && localStorage.getItem(title) != ""){
            return JSON.parse(localStorage.getItem(title))
        } else {
            return null
        }
    }

    setSession(name:string, value:any){
        localStorage.setItem(name, value);
    }

    deleteSession(name:string){

        if(localStorage.getItem(name) != undefined && localStorage.getItem(name) != null && localStorage.getItem(name) != ""){
            localStorage.removeItem(name);
        }
    }

    getCookie(title:string){

        if(Cookie.get(title) != undefined && Cookie.get(title) != null && Cookie.get(title) != ""){
            return Cookie.get(title)
        } else {
            return null
        }
    }

    setCookie(title:string, value){
        Cookie.set(title, value, 365);
    }

    deleteCookie(title:string){

        if(Cookie.get(title) != undefined && Cookie.get(title) != null && Cookie.get(title) != ""){
            Cookie.delete(title);
        }
    }
}
