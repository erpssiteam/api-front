/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CookiesTokenService } from './cookies-token.service';

describe('CookiesTokenService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CookiesTokenService]
    });
  });

  it('should ...', inject([CookiesTokenService], (service: CookiesTokenService) => {
    expect(service).toBeTruthy();
  }));
});
