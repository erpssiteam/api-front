import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CookiesTokenService } from '../cookies/cookies-token.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private _cookiesTokenService:CookiesTokenService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    if(this._cookiesTokenService.getToken() != null){
      this.isValidate();
      // logged in so return true
      return true;
    }

    // not logged in so redirect to login page with the return url and return false
    // this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
    this.router.navigate(['/login']);
    return false;
  }
  isValidate(){
    let cookieToken = this._cookiesTokenService.getToken();
    let dateValidToken = new Date (cookieToken.expered);
    let today = new Date();
    if(dateValidToken < today ){
      this.router.navigate(['/login']);
      return false;
    }else {
      return true;
    }
  }
}
