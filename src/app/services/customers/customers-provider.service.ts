import { Injectable } from '@angular/core';
import { Http }          from '@angular/http';
import { Router } from '@angular/router';

import 'rxjs/add/operator/toPromise';
import { Customer } from '../../model/customer.model';
import { CustumerSerialize } from '../../model/serializer/custumer.serialize.model';
import { Role } from '../../model/role.model';
import { User } from '../../model/user.model';

import {CommonProvider} from "../common/common-provider.service";
import {CookiesTokenService} from "../cookies/cookies-token.service";

const CUSTOMERS_DATA_URI = 'http://erpssi.sandbox.skilvioo.com/api/customers';
const CUSTOMERS_DATA_URI_PARAM = 'http://erpssi.sandbox.skilvioo.com/api/customers/';

@Injectable()
export class CustomersProvider extends CommonProvider{

    private customersMap = [];

    constructor(private _router: Router, http: Http, cookiesTokenService: CookiesTokenService) {
        super(http, cookiesTokenService, _router);
    };

    getAll(): Promise<Customer[]> {
        return new Promise((resolve, reject) => {
            if(this.customersMap.length == 0) {
                this.http.get(CUSTOMERS_DATA_URI, { headers: this.getHeader() }).toPromise().then(response => {
                    let json = JSON.parse(response['_body']).data;
                    let customers = [];
                    for(let k in json) {
                        if(json.hasOwnProperty(k)) {
                            customers.push(CustomersProvider.createCustomersModel(json[k]));
                        }
                    }
                    this.customersMap = customers;
                    resolve(this.customersMap);
                }).catch(error => {
                    this.error(error)
                });
            } else {
                resolve(this.customersMap);
            }
        });
    }

    get(id: Number): Promise<Customer[]> {
        return new Promise((resolve, reject) => {
            this.http.get(CUSTOMERS_DATA_URI_PARAM + id, { headers: this.getHeader() }).toPromise().then(response => {
                let json = JSON.parse(response['_body']).data;
                let customerModel = CustomersProvider.createCustomersModel(json);
                resolve(customerModel);
            }).catch(error => {
                this.error(error)
            });
        });
    }

    remove(id: Number): Promise<Customer[]> {
        return new Promise((resolve, reject) => {
            this.http.delete(CUSTOMERS_DATA_URI_PARAM + id, { headers: this.getHeader() }).toPromise().then(response => {
                // toast with response message
                this.customersMap.forEach((h, index) => {
                    if(h.id === id) {
                        this.customersMap.splice(index, 1);
                    }
                });
            }).catch(error => {
                this.error(error)
            });
            resolve(this.customersMap);
        });
    }

    post(customer: Customer): Promise<Customer> {
        let custumerSerialize = new CustumerSerialize();
        custumerSerialize.hydrateModelFromJSON(customer);
        return new Promise((resolve, reject) => {
            this.http.post(CUSTOMERS_DATA_URI ,{ data: custumerSerialize.hydrateModelFromJSON(customer) }, { headers: this.getHeader() }).toPromise().then(response => {
                let json = JSON.parse(response['_body']).data;
                let customerModel = CustomersProvider.createCustomersModel(json);
                this.customersMap.push(customerModel);
                resolve(this.customersMap);
            }).catch(error => {
                this.error(error)
            });
        });
    }

    put(customer: Customer): Promise<Customer> {
        let custumerSerialize = new CustumerSerialize();
        custumerSerialize.hydrateModelFromJSON(customer);
        return new Promise((resolve, reject) => {
            this.http.put(CUSTOMERS_DATA_URI_PARAM + customer.getId(), { data: custumerSerialize.hydrateModelFromJSON(customer) }, { headers: this.getHeader() }).toPromise().then(response => {
                let json = JSON.parse(response['_body']).data;
                customer.hydrateModelFromJSON(customer, json);
                resolve(customer);
            }).catch(error => {
                this.error(error)
            });
        });
    }

    private static createCustomersModel(json: any): Customer {
        let role = json.user.role;
        let user = new User(json.user.username, json.user.password, [role]);
        let customerModel = new Customer(json.first_name, json.last_name, json.birthday_date, json.email, json.address, json.zip_code, json.city, user);
        customerModel.hydrateModelFromJSON(customerModel, json);
        return customerModel;
    }
}
