/**
 * Created by rislou on 25/03/17.
 */
import { Injectable } from '@angular/core';
import { Http, Headers }          from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import {CookiesTokenService} from "../cookies/cookies-token.service";
import {Employee} from "../../model/employee.model";
import {User} from "../../model/user.model";
import {Role} from "../../model/role.model";

const URI_API_BACK = 'http://erpssi.sandbox.skilvioo.com/api';
// const URI_API_BACK = 'http://127.0.0.1:8000/api';
const FEATURE_NEWS = 'feature_news';
const FEATURE_EXPENSE_ACCOUNT = 'feature_expense_account';
const FEATURE_APPLICATION = 'feature_application';
const FEATURE_CRA = 'feature_cra';
const FEATURE_HOLIDAYS = 'feature_holiday';
const FEATURE_JOBPROFILE = 'feature_jobprofile';
const FEATURE_GROUPWARE = 'feature_groupware';
const FEATURE_SKILLS = 'feature_skills';

@Injectable()
export class CommonProvider {

  protected headers: Headers = new Headers;

  constructor(protected http: Http, protected cookiesTokenService:CookiesTokenService, protected router: Router) {
    this.headers.set('Access-Control-Allow-Origin', '*');
    this.headers.set('Content-Type', 'application/json');
  };

  getHeader(): Headers {
    if(this.cookiesTokenService.getToken() != null) {
      this.headers.set('authorization', 'Bearer ' + JSON.parse(localStorage.getItem('token')).token);
      return this.headers;
    } else {
      console.log('No token provided');
    }
    return this.headers;
  }

  getToken(): string {
    return 'Bearer ' +  JSON.parse(localStorage.getItem('token')).token;
  }

  getCurrentUserId() {
    if(localStorage.getItem('employee')) {
      return JSON.parse(localStorage.getItem('employee')).id;
    } else {
      console.log('No employee has been set in the local storage');
    }
  }

  getLocalStorageByKey(key: string): any {
    if(localStorage.getItem(key)) {
      return JSON.parse(localStorage.getItem(key));
    } else {
      console.log('No local storage found with this key');
    }
  }

  getCurrentEmployee() {
    let employeeJSON = this.getLocalStorageByKey('employee');
    let employee = new Employee(employeeJSON.first_name, employeeJSON.last_name, employeeJSON.birthday_date, employeeJSON.email, employeeJSON.address, employeeJSON.zip_code, employeeJSON.city, this.getCurrentUser());
    employee.hydrateModelFromJSON(employee, employeeJSON);
    return employee;
  }

  getCurrentUserRole(): Role {
    return new Role(this.getLocalStorageByKey('employee').user.role[0]);
  }

  getCurrentUser(): User {
    return new User(this.getLocalStorageByKey('employee').user.username, "", [this.getCurrentUserRole()]);
  }

  voteFactory(strategy): boolean {
    let role = this.getCurrentUserRole();
    switch (strategy) {
      case FEATURE_NEWS:
        return role.getLabel() === role.rolesMap.news || role.getLabel() === role.rolesMap.admin;
      case FEATURE_EXPENSE_ACCOUNT:
        return role.getLabel() === role.rolesMap.rh || role.getLabel() === role.rolesMap.manager || role.getLabel() === role.rolesMap.admin;
      case FEATURE_APPLICATION:
        return role.getLabel() === role.rolesMap.rh || role.getLabel() === role.rolesMap.manager || role.getLabel() === role.rolesMap.admin;
      case FEATURE_CRA:
        return role.getLabel() === role.rolesMap.rh || role.getLabel() === role.rolesMap.manager || role.getLabel() === role.rolesMap.admin;
      case FEATURE_SKILLS:
        return role.getLabel() === role.rolesMap.rh || role.getLabel() === role.rolesMap.admin || role.getLabel() === role.rolesMap.manager;
      case FEATURE_JOBPROFILE:
        return role.getLabel() === role.rolesMap.rh || role.getLabel() === role.rolesMap.manager || role.getLabel() === role.rolesMap.admin;
      case FEATURE_HOLIDAYS:
        return role.getLabel() === role.rolesMap.rh || role.getLabel() === role.rolesMap.admin || role.getLabel() === role.rolesMap.collaborator;
      case FEATURE_GROUPWARE:
        return role.getLabel() === role.rolesMap.admin;
      default:
        return false;
    }
  }

  error(data) {
      let code = data.status || JSON.parse(data._body).code
      let message = data.statusText || JSON.parse(data._body).message

      if (code.toString()[0] == "4" || code.toString()[0] == "5"){
          this.router.navigate(['/error'], {
              queryParams: {
                  code: parseInt(code),
                  message: message
              }
          });
      }
  }
}
