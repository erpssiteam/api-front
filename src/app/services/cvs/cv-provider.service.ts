
import { Injectable } from '@angular/core';
import { Http }          from '@angular/http';
import { Router } from '@angular/router';

import 'rxjs/add/operator/toPromise';
import {Holiday} from '../../model/holiday.model';
import { Cv } from '../../model/cv.model';
import {CommonProvider} from "../common/common-provider.service";
import {CookiesTokenService} from "../cookies/cookies-token.service";

import {Employee} from '../../model/employee.model';
import {Customer} from '../../model/customer.model';
import {User} from '../../model/user.model';
import {Role} from '../../model/role.model';

const DATA_URI = 'http://erpssi.sandbox.skilvioo.com/api/cvs';
const DATA_URI_PARAM = 'http://erpssi.sandbox.skilvioo.com/api/cvs/';

@Injectable()
export class CvProvider extends CommonProvider{

    private cvsMap = [];

    constructor(private _router: Router, http: Http, cookiesTokenService: CookiesTokenService) {
        super(http, cookiesTokenService, _router);
    };

    getAll(): Promise<Cv[]> {
        return new Promise((resolve, reject) => {
            if(this.cvsMap.length == 0) {
                this.http.get(DATA_URI, { headers: this.getHeader() }).toPromise().then(response => {
                    let json = JSON.parse(response['_body']).data;
                    let cvs = [];
                    for(let k in json) {
                        if(json.hasOwnProperty(k)) {
                            cvs.push(CvProvider.createCvsModel(json[k]));
                        }
                    }
                    this.cvsMap = cvs;
                    resolve(this.cvsMap);
                }).catch(error => {
                    this.error(error)
                });
            } else {
                resolve(this.cvsMap);
            }
        });
    }

    get(id: Number): Promise<Cv[]> {
        return new Promise((resolve, reject) => {
            this.http.get(DATA_URI_PARAM + id, { headers: this.getHeader() }).toPromise().then(response => {
                let json = JSON.parse(response['_body']).data;
                let cvModel = CvProvider.createCvsModel(json);
                resolve(cvModel);
            }).catch(error => {
                this.error(error)
            });
        });
    }

    remove(id: Number): Promise<Cv[]> {
        return new Promise((resolve, reject) => {
            this.http.delete(DATA_URI_PARAM + id, { headers: this.getHeader() }).toPromise().then(response => {
                // toast with response message
                this.cvsMap.forEach((h, index) => {
                    if(h.id === id) {
                        this.cvsMap.splice(index, 1);
                    }
                });
            }).catch(error => {
                this.error(error)
            });
            resolve(this.cvsMap);
        });
    }

    private static createCvsModel(json: any): Cv {
        let cvModel = new Cv();
        cvModel.hydrateModelFromJSON(cvModel, json);
        return cvModel;
    }
    private parseJson(response: any): any {
        let json = JSON.parse(response['_body']).data;
        let cvs = [];
        for (let k in json) {
            if (json.hasOwnProperty(k)) {
                cvs.push(CvProvider.createCvsModel(json[k]));
            }
        }
        this.cvsMap = cvs;
        return this.cvsMap;
    };
}

