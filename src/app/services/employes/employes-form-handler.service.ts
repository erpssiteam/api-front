import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { CommonProvider } from '../common/common-provider.service';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { CookiesTokenService } from '../cookies/cookies-token.service';
import { DateModel, DatePickerOptions } from "ng2-datepicker";

@Injectable()
export class EmployeeFormHandler extends CommonProvider {

    formErrors = {
        'lastname': '',
        'firstname': '',
        'address': '',
        'zipcode': '',
        'city': '',
        'password': '',
        'phone': '',
        'email': ''
    };
    validationMessages = {
        'lastname': {
            'required':     'Vous devez entrer un nom !',
            'minlength':    'Vous devez saisir un nom qui possède au moins 3 caractères',
            'maxlength':    'Vous pouvez saisir 20 caractères au maximum.'
        },
        'firstname': {
            'required':     "Vous devez entrer un prénom !",
            'minlength':    'Vous devez saisir un prénom qui possède au moins 3 caractères',
            'maxlength':    'Vous pouvez saisir 20 caractères au maximum.'
        },
        'address': {
            'required':     "Vous devez entrer une adresse !"
        },
        'zipcode': {
            'required':     'Vous devez entrer un code postal !',
            'minlength':    'Vous devez saisir un libellé qui possède au moins 4 caractère',
            'maxlength':    'Vous pouvez saisir 5 caractères au maximum.'
        },
        'city': {
            'required':     'Vous devez entrer une ville !'
        },
        'password': {
            'required':     'Vous devez entrer un mot de passe !'
        },
        'phone': {
            'required':     'Vous devez entrer un numéro de téléphone !'
        },
        'email': {
            'required':     'Vous devez entrer un email !'
        }
    };
    filePlaceholder = 'Choisir un justificatif...';

    configBirthday = {
        format: 'DD-MM-YYYY',
        initialDate: new Date(),
        locale: 'fr'
    };
    configDatePicker = {
        format: 'DD-MM-YYYY',
        initialDate: new Date(),
        locale: 'fr'
    };

    constructor(http: Http, cookiesTokenService: CookiesTokenService, private _router:Router) {
        super(http, cookiesTokenService,_router);
    };
}
