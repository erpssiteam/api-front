import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CommonProvider } from "../common/common-provider.service";
import { CookiesTokenService } from "../cookies/cookies-token.service";
import { Employee } from '../../model/employee.model';
import { EmployeSerialize } from '../../model/serializer/employe.serialize.model';
import { User } from '../../model/user.model';

const EMPLOYEE_DATA_URI = 'http://erpssi.sandbox.skilvioo.com/api/employees';
const EMPLOYEE_DATA_URI_PARAM = EMPLOYEE_DATA_URI+'/';


@Injectable()
export class EmployesProvider extends CommonProvider{

    private employesMap = [];


    constructor( http: Http, cookiesTokenService: CookiesTokenService, router : Router){
        super(http, cookiesTokenService, router);

    }
    getAll(): Promise<Employee[]> {
        return new Promise((resolve, reject) => {
            if(this.employesMap.length == 0) {
                this.http.get(EMPLOYEE_DATA_URI, { headers: this.getHeader() }).toPromise().then(response => {
                    let json = JSON.parse(response['_body']).data;
                    let employes = [];
                    for(let k in json) {
                        if(json.hasOwnProperty(k)) {
                            employes.push(EmployesProvider.createEmployeModel(json[k]));
                        }
                    }
                    this.employesMap = employes;
                    resolve(this.employesMap);
                });
            } else {
                resolve(this.employesMap);
            }
        });
    }
    get(id: Number): Promise<Employee[]> {
        return new Promise((resolve, reject) => {
            this.http.get(EMPLOYEE_DATA_URI_PARAM + id, { headers: this.getHeader() }).toPromise().then(response => {
                let json = JSON.parse(response['_body']).data;
                let employeModel = EmployesProvider.createEmployeModel(json);
                resolve(employeModel);
            });
        });
    }
    getCustomerByEmploye(id: Number): Promise<Employee[]> {
        return new Promise((resolve, reject) => {
            this.http.get(EMPLOYEE_DATA_URI_PARAM + id + '/customers', { headers: this.getHeader() }).toPromise().then(response => {
                resolve(this.parseJson(response));
            });
        });
    }

    remove(id: Number): Promise<Employee[]> {
        return new Promise((resolve, reject) => {
            this.http.delete(EMPLOYEE_DATA_URI_PARAM + id, { headers: this.getHeader() }).toPromise().then(response => {
                // toast with response message
                this.employesMap.forEach((ea, index) => {
                    if(ea.getId() === id) {
                        this.employesMap.splice(index, 1);
                    }
                });
            });
            resolve(this.employesMap);
        });
    }

    post(employee: Employee): Promise<Employee> {
        let employeSerialize = new EmployeSerialize();
        employeSerialize.hydrateModelFromJSON(employee);
        return new Promise((resolve, reject) => {
            this.http.post(EMPLOYEE_DATA_URI, { data: employeSerialize.hydrateModelFromJSON(employee) }, { headers: this.getHeader() }).toPromise().then(response => {
                let json = JSON.parse(response['_body']).data;
                let employeModel = EmployesProvider.createEmployeModel(json);
                this.employesMap.push(employeModel);
                resolve(this.employesMap);
            });
        });
    }

    put(employee: Employee): Promise<Employee> {
        let employeSerialize = new EmployeSerialize();
        employeSerialize.hydrateModelFromJSON(employee);
        return new Promise((resolve, reject) => {
            this.http.put(EMPLOYEE_DATA_URI_PARAM + employee.getId(), { data: employeSerialize.hydrateModelFromJSON(employee) }, { headers: this.getHeader() }).toPromise().then(response => {
                let json = JSON.parse(response['_body']).data;
                employee.hydrateModelFromJSON(employee, json);
                resolve(employee);
            });
        });
    }

    private static createEmployeModel(json: any): Employee {
        let role = json.user.role;
        let user = new User(json.user.username, json.user.password, [role]);
        let employeeModel = new Employee(json.first_name, json.last_name, json.birthday_date, json.email, json.address,
            json.zip_code, json.city, user);
        employeeModel.hydrateModelFromJSON(employeeModel, json);
        return employeeModel;
    }
    private parseJson(response: any): any {
        let json = JSON.parse(response['_body']).data;
        let employees = [];
        for (let k in json) {
            if (json.hasOwnProperty(k)) {
                employees.push(EmployesProvider.createEmployeModel(json[k]));
            }
        }
        this.employesMap = employees;
        return this.employesMap;
    };
}
