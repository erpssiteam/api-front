import { Injectable } from '@angular/core';
import { Http }          from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import {ExpenseAccount} from '../../model/expense-account.model';
import {Employee} from '../../model/employee.model';
import {User} from '../../model/user.model';
import {Role} from '../../model/role.model';
import { CommonProvider } from '../common/common-provider.service';
import { CookiesTokenService } from '../cookies/cookies-token.service';

const EXPENSE_ACCOUNT_DATA_URI = 'http://erpssi.sandbox.skilvioo.com/api/expense-accounts';
const EXPENSE_ACCOUNT_DATA_URI_PARAM = 'http://erpssi.sandbox.skilvioo.com/api/expense-accounts/';

@Injectable()
export class ExpenseAccountProvider extends CommonProvider {

  constructor(http: Http, cookiesTokenService: CookiesTokenService, router : Router) {
    super(http, cookiesTokenService, router);
  };

  getAll(): Promise<ExpenseAccount[]> {
    return new Promise((resolve, reject) => {
      this.http.get(EXPENSE_ACCOUNT_DATA_URI, { headers: this.getHeader() }).toPromise().then(response => {
        let expenseAccounts = this.parseJson(response);
        expenseAccounts.forEach((ea, index) => {
          if(ea.getEmployee().getId() === this.getCurrentUserId()) {
            expenseAccounts.splice(index, 1);
          }
        });
        resolve(expenseAccounts);
      });
    });
  };

  getAllByEmployee(): Promise<ExpenseAccount[]> {
    return new Promise((resolve, reject) => {
      this.http.get(EXPENSE_ACCOUNT_DATA_URI_PARAM + 'employee/' + this.getCurrentEmployee().getId(), {headers: this.getHeader()}).toPromise().then(response => {
        let expenseAccounts = this.parseJson(response);
        resolve(expenseAccounts);
      });
    });
  };

  get(id: Number): Promise<ExpenseAccount[]> {
    return new Promise((resolve, reject) => {
      this.http.get(EXPENSE_ACCOUNT_DATA_URI_PARAM + id, { headers: this.getHeader() }).toPromise().then(response => {
        let json = JSON.parse(response['_body']).data;
        let expenseAccountModel = ExpenseAccountProvider.createExpenseAccountModel(json);
        resolve(expenseAccountModel);
      });
    });
  };

  remove(id: Number, expenseAccounts: ExpenseAccount[]): Promise<ExpenseAccount[]> {
    return new Promise((resolve, reject) => {
      this.http.delete(EXPENSE_ACCOUNT_DATA_URI_PARAM + id, { headers: this.getHeader() }).toPromise().then(response => {
        // toast with response message
        expenseAccounts.forEach((ea, index) => {
          if (ea.getId() === id) {
            expenseAccounts.splice(index, 1);
          }
        });
      });
      resolve(expenseAccounts);
    });
  };

  post(expenseAccount: ExpenseAccount): Promise<ExpenseAccount> {
    return new Promise((resolve, reject) => {
      this.http.post(EXPENSE_ACCOUNT_DATA_URI_PARAM  + this.getCurrentUserId(), { data: expenseAccount }, { headers: this.getHeader() }).toPromise().then(response => {
        let json = JSON.parse(response['_body']).data;
        expenseAccount.hydrateModelFromJSON(expenseAccount, json);
        resolve(expenseAccount);
      });
    });
  };

  validate(state:number, expenseAccount: ExpenseAccount, expenseAccounts: ExpenseAccount[]): Promise<ExpenseAccount[]> {
    return new Promise((resolve, reject) => {
      this.http.put(EXPENSE_ACCOUNT_DATA_URI_PARAM + expenseAccount.getId() + '/handle/' + state, {}, { headers: this.getHeader() }).toPromise().then(response => {
        let json = JSON.parse(response['_body']).data;
        expenseAccount.hydrateModelFromJSON(expenseAccount, json);
        // toast with response message
        expenseAccounts.forEach((n, index) => {
          if (n.getId() === expenseAccount.getId()) {
            expenseAccounts[index] = expenseAccount;
          }
        });
        resolve(expenseAccounts);
      });
    });
  };

  put(expenseAccount: ExpenseAccount): Promise<ExpenseAccount> {
    return new Promise((resolve, reject) => {
      this.http.put(EXPENSE_ACCOUNT_DATA_URI_PARAM + expenseAccount.getId(), { data: expenseAccount }, { headers: this.getHeader() }).toPromise().then(response => {
        let json = JSON.parse(response['_body']).data;
        expenseAccount.hydrateModelFromJSON(expenseAccount, json);
        resolve(expenseAccount);
      });
    });
  };

  private static createExpenseAccountModel(json: any): ExpenseAccount {
    let role = new Role(json.employee.user.role.label);
    let user = new User(json.employee.user.username, json.employee.user.password, [role]);
    let employee = new Employee(json.employee.first_name, json.employee.last_name, json.employee.birthday_date, json.employee.email, json.employee.address, json.employee.zip_code, json.employee.city, user);
    let expenseAccountModel = new ExpenseAccount(employee, json.label, json.total, json.tva);
    expenseAccountModel.hydrateModelFromJSON(expenseAccountModel, json);
    return expenseAccountModel;
  };

  private parseJson(response: any): any {
    let json = JSON.parse(response['_body']).data;
    let expenseAccounts = [];
    for (let k in json) {
      if (json.hasOwnProperty(k)) {
        expenseAccounts.push(ExpenseAccountProvider.createExpenseAccountModel(json[k]));
      }
    }
    return expenseAccounts;
  };

}
