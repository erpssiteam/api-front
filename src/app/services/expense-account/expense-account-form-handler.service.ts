import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import {CommonProvider} from '../common/common-provider.service';
import {Http} from '@angular/http';
import {CookiesTokenService} from '../cookies/cookies-token.service';
import {DateModel, DatePickerOptions} from "ng2-datepicker";
import {FileUploader} from "ng2-file-upload";
import {Router} from "@angular/router";

const EXPENSE_ACCOUNT_DATA_URI_PARAM = 'http://erpssi.sandbox.skilvioo.com/api/expense-accounts/';

@Injectable()
export class ExpenseAccountFormHandler extends CommonProvider {

  formErrors = {
    'label': '',
    'description': '',
    'total': '',
    'tva': ''
  };
  validationMessages = {
    'label': {
      'required':      'Le libéllé est obligatoire.',
      'minlength':     'Vous devez saisir un libellé qui possède au moins 3 caractères',
      'maxlength':     'Vous pouvez saisir 5 caractères au maximum.'
    },
    'description': {
      'maxlenght': 'Vous pouvez saisir 255 caractères au maximum'
    },
    'tva': {
      'required': 'La TVA est obligatoire'
    },
    'total': {
      'required': 'Le total est obligatoire'
    }
  };
  filePlaceholder = 'Choisir un justificatif...';
  configDatePicker = {
    format: 'DD-MM-YYYY',
    initialDate: new Date(),
    locale: 'fr'
  };

  constructor(http: Http, cookiesTokenService: CookiesTokenService, router: Router) {
    super(http, cookiesTokenService, router);
  };

  getConfigForUpload(id): Object {
    return {
      url: EXPENSE_ACCOUNT_DATA_URI_PARAM + id + '/image',
      authToken: this.getToken()
    };
  };

}
