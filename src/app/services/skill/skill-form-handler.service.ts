import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import {CommonProvider} from '../common/common-provider.service';
import {Http} from '@angular/http';
import {CookiesTokenService} from '../cookies/cookies-token.service';
import {Router} from "@angular/router";

@Injectable()
export class SkillFormHandler extends CommonProvider {

  formErrors = {
    'label': '',
    "description": '',
  };
  validationMessages = {
    'label': {
      'required':      'Le label est obligatoire.',
      'minlength':     'Vous devez saisir un label qui possède au moins 3 caractères',
      'maxlength':     'Vous pouvez saisir 5 caractères au maximum.'
    },
    "description": {
      'maxlenght': 'Vous pouvez saisir 255 caractères au maximum'
    },
  };

  constructor(http: Http, cookiesTokenService: CookiesTokenService, router: Router) {
    super(http, cookiesTokenService, router);
  };

}
