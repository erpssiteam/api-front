import { Injectable } from '@angular/core';
import { Http }          from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import { CommonProvider } from '../common/common-provider.service';
import { CookiesTokenService } from '../cookies/cookies-token.service';
import {Skill} from "../../model/skill.model";
import {EmployeeSkill} from "../../model/employeeskill.model";
import {Categorie} from "../../model/categorie.model";

const SKILL_DATA_URI = 'http://erpssi.sandbox.skilvioo.com/api/skills';
const SKILL_DATA_URI_PARAM = 'http://erpssi.sandbox.skilvioo.com/api/skills/';
const SKILL_EMPLOYEES_DATA_URI = 'http://erpssi.sandbox.skilvioo.com/api/employee-skills';
const SKILL_EMPLOYEES_DATA_URI_PARAM = 'http://erpssi.sandbox.skilvioo.com/api/employee-skills/';
const SKILL_CATEGORIES_DATA_URI = 'http://erpssi.sandbox.skilvioo.com/api/category-skills';
const SKILL_CATEGORIES_DATA_URI_PARAM = 'http://erpssi.sandbox.skilvioo.com/api/category-skills/';

@Injectable()
export class SkillProvider extends CommonProvider {

  private skillsMap = [];

  constructor(http: Http, cookiesTokenService: CookiesTokenService, router : Router) {
    super(http, cookiesTokenService, router);
  };

  getAll(): Promise<Skill[]> {
    return new Promise((resolve, reject) => {
      if (this.skillsMap.length === 0) {
        this.http.get(SKILL_DATA_URI, { headers: this.getHeader() }).toPromise().then(response => {
          resolve(this.parseJson(response));
        });
      } else {
        resolve(this.skillsMap);
      }
    });
  };

  getAllCategories(): Promise<any[]> {
    return new Promise((resolve, reject) => {
        this.http.get(SKILL_CATEGORIES_DATA_URI, { headers: this.getHeader() }).toPromise().then(response => {
          resolve(this.parseJsonCategorie(response));
        });
    });
  };

  get(id: Number): Promise<Skill> {
    return new Promise((resolve, reject) => {
      this.http.get(SKILL_DATA_URI_PARAM + id, { headers: this.getHeader() }).toPromise().then(response => {
        let json = JSON.parse(response['_body']).data;
        let newsModel = SkillProvider.createSkillModel(json);
        resolve(newsModel);
      });
    });
  };

  getByEmployees(id: Number): Promise<EmployeeSkill[]> {
    return new Promise((resolve, reject) => {
        this.http.get(SKILL_EMPLOYEES_DATA_URI_PARAM + id, { headers: this.getHeader() }).toPromise().then(response => {
            resolve(this.parseJsonEmployeeSkill(response));
        });
    });
  };

  remove(id: Number): Promise<Skill[]> {
    return new Promise((resolve, reject) => {
      this.http.delete(SKILL_DATA_URI_PARAM + id, { headers: this.getHeader() }).toPromise().then(response => {
        // toast with response message
        this.skillsMap.forEach((n, index) => {
          if (n.getId() === id) {
            this.skillsMap.splice(index, 1);
          }
        });
      });
      resolve(this.skillsMap);
    });
  };

  post(news: Skill): Promise<Skill> {
    return new Promise((resolve, reject) => {
      this.http.post(SKILL_DATA_URI, { data: news }, { headers: this.getHeader() }).toPromise().then(response => {
          let json = JSON.parse(response['_body']).data;
          let newsModel = SkillProvider.createSkillModel(json);
          this.skillsMap.push(newsModel);
          resolve(this.skillsMap);
      });
    });
  };

  postByEmployees(skillEmployee: EmployeeSkill): Promise<EmployeeSkill> {
    return new Promise((resolve, reject) => {
      this.http.post(SKILL_EMPLOYEES_DATA_URI_PARAM + skillEmployee._employee, { "data" : { "data" : [skillEmployee] } }, { headers: this.getHeader() }).toPromise().then(response => {
          resolve(response);
      });
    });
  };

  put(news: Skill): Promise<Skill> {
    return new Promise((resolve, reject) => {
      this.http.put(SKILL_DATA_URI_PARAM + news.getId(), { data: news }, { headers: this.getHeader() }).toPromise().then(response => {
        let json = JSON.parse(response['_body']).data;
        news.hydrateModelFromJSON(news, json);
        resolve(news);
      });
    });
  };


    /**
    *
    * EMPLOYEE/SKILL
    */
  private static createEmployeeSkillModel(json: any): EmployeeSkill {
    let employeeskill = new EmployeeSkill();
    employeeskill.hydrateModelFromJSON(employeeskill, json);
    return employeeskill;
  };

  private parseJsonEmployeeSkill(response: any): any {
    let json = JSON.parse(response['_body']).data;
    let skills = [];
    for (let k in json) {
      if (json.hasOwnProperty(k)) {
        skills.push(SkillProvider.createEmployeeSkillModel(json[k]));
      }
    }
    return skills;
  };

    /**
    *
    * SKILL
    */
  private static createSkillModel(json: any): Skill {
    let skill = new Skill();
    skill.hydrateModelFromJSON(skill, json);
    return skill;
  };

  private parseJson(response: any): any {
    let json = JSON.parse(response['_body']).data;
    let skills = [];
    for (let k in json) {
      if (json.hasOwnProperty(k)) {
        skills.push(SkillProvider.createSkillModel(json[k]));
      }
    }
    this.skillsMap = skills;
    return this.skillsMap;
  };

  /**
  *
  * CATEGORIE
  */
  private static createCategorieModel(json: any): Categorie {
    let categorie = new Categorie();
    categorie.hydrateModelFromJSON(categorie, json);
    return categorie;
  };

  private parseJsonCategorie(response: any): any {
    let json = JSON.parse(response['_body']).data;
    let categories = [];
    for (let k in json) {
      if (json.hasOwnProperty(k)) {
        categories.push(SkillProvider.createCategorieModel(json[k]));
      }
    }
    return categories;
  };

}
