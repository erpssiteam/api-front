import { Component ,OnInit, ElementRef, ViewChild,Inject, Injectable, EventEmitter } from '@angular/core';
import { Router } from '@angular/router'
import { HttpApiErpssiService  } from '../httpApiErpssi/http-api-erpssi.service';
import { CookiesTokenService } from '../cookies/cookies-token.service';
@Injectable()

export class LoginsService  {

    // private url = "oauth/token"; // API LARAVEL
    private url = "login_check";

    // private url = "http://localhost:5555/oauth/tokens";
    private client_id = null;
    private client_secret = null;
    private grant_type = null;


    constructor(private _em: HttpApiErpssiService, private _router: Router, private _cookiesTokenService:CookiesTokenService){
        this.client_id = _em.client_id;
        this.client_secret = _em.clientSecret;
        this.grant_type = _em.grant_type;
    }

    login(obj){

        let data = {
            // client_id : this.client_id,
            // client_secret : this.client_secret,
            // grant_type : this.grant_type,
            _username : obj.login.user,
            _password : obj.login.password
        };

        this.authenticate(data);
    }

    private authenticate(data) {
        return this._em.post(data , this.url).then((data) => {
            this.success(data)
        });
    }

    private getUser(){
        return this._em.get('api/user/me')
    }

    private register(data) {
        return this._em.post(data,this.url)
    }

    success(data){
        let today = new Date();
        let tomorrow = new Date();
        tomorrow.setDate(today.getDate()+1);
      this._cookiesTokenService.setSession("employee", JSON.stringify(data.data));
      this._cookiesTokenService.setSession("token",JSON.stringify({'token': data.token , 'expered': tomorrow }));
      this._router.navigate(['/']);
    }
}
