import { Injectable } from '@angular/core';
import { Http }          from '@angular/http';
import { Router } from '@angular/router';

import 'rxjs/add/operator/toPromise';
import {JobProfile} from '../../model/jobprofile.model';
import {CommonProvider} from "../common/common-provider.service";
import {CookiesTokenService} from "../cookies/cookies-token.service";

const JOBPROFILE_DATA_URI = 'http://erpssi.sandbox.skilvioo.com/api/job-profiles';
const JOBPROFILE_DATA_URI_PARAM = 'http://erpssi.sandbox.skilvioo.com/api/job-profiles/';

@Injectable()
export class JobProfilesProvider extends CommonProvider{

    private jobProfileMap = [];

    constructor(private _router: Router, http: Http, cookiesTokenService: CookiesTokenService) {
        super(http, cookiesTokenService, _router);
    };

    getAll(): Promise<JobProfile[]> {
        return new Promise((resolve, reject) => {
            if(this.jobProfileMap.length == 0) {
                this.http.get(JOBPROFILE_DATA_URI, { headers: this.getHeader() }).toPromise().then(response => {
                    let json = JSON.parse(response['_body']).data;
                    let jobProfile = [];
                    for(let k in json) {
                        if(json.hasOwnProperty(k)) {
                            jobProfile.push(JobProfilesProvider.createJobProfilesModel(json[k]));
                        }
                    }
                    this.jobProfileMap = jobProfile;
                    resolve(this.jobProfileMap);
                }).catch(error => {
                    this.error(error)
                });
            } else {
                resolve(this.jobProfileMap);
            }
        });
    }

    get(id: Number): Promise<JobProfile[]> {
        return new Promise((resolve, reject) => {
            this.http.get(JOBPROFILE_DATA_URI_PARAM + id, { headers: this.getHeader() }).toPromise().then(response => {
                let json = JSON.parse(response['_body']).data;
                let jobProfileModel = JobProfilesProvider.createJobProfilesModel(json);
                resolve(jobProfileModel);
            }).catch(error => {
                this.error(error)
            });
        });
    }

    remove(id: Number): Promise<JobProfile[]> {
        return new Promise((resolve, reject) => {
            this.http.delete(JOBPROFILE_DATA_URI_PARAM + id, { headers: this.getHeader() }).toPromise().then(response => {
                // toast with response message
                this.jobProfileMap.forEach((jb, index) => {
                    if(jb.id === id) {
                        this.jobProfileMap.splice(index, 1);
                    }
                });
            }).catch(error => {
                this.error(error)
            });
            resolve(this.jobProfileMap);
        });
    }

    post(jobProfile: JobProfile): Promise<JobProfile> {
        return new Promise((resolve, reject) => {
            this.http.post(JOBPROFILE_DATA_URI_PARAM + jobProfile._customer, { data: jobProfile }, { headers: this.getHeader() }).toPromise().then(response => {
                let json = JSON.parse(response['_body']).data;
                let jobProfileModel = JobProfilesProvider.createJobProfilesModel(json);
                this.jobProfileMap.push(jobProfileModel);
                resolve(this.jobProfileMap);
            }).catch(error => {
                this.error(error)
            });
        });
    }

    put(jobProfile: JobProfile): Promise<JobProfile> {
        return new Promise((resolve, reject) => {
            this.http.put(JOBPROFILE_DATA_URI_PARAM + jobProfile._id +"/"+ jobProfile._customer, { data: jobProfile }, { headers: this.getHeader() }).toPromise().then(response => {
                let json = JSON.parse(response['_body']).data;
                jobProfile.hydrateModelFromJSON(jobProfile, json);
                resolve(jobProfile);
            }).catch(error => {
                this.error(error)
            });
        });
    }

    private static createJobProfilesModel(json: any): JobProfile {
        let jobProfileModel = new JobProfile();
        jobProfileModel.hydrateModelFromJSON(jobProfileModel, json);
        return jobProfileModel;
    }
}
