import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { CookiesTokenService } from '../cookies/cookies-token.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

declare var jQuery:any;

@Injectable()
export class HttpApiErpssiService {

    private _url:string = "http://erpssi.sandbox.skilvioo.com/api";
    // private _url:string = "http://127.0.0.1:8000/api";
    private _clientSecret:string = '';
    private _client_id = 2;
    private _client_secret = "";
    private _grant_type = "";
    private _data:any;
    private _headers:Headers = new Headers();

    constructor(private _http: Http, private  _router: Router, private _cookiesTokenService:CookiesTokenService){
        // not needed for post
        this.headers.set('Access-Control-Allow-Origin', '*');
        this.headers.set('Content-Type', 'application/json');
    }

    get url(): string {
        return this._url;
    }

    set url(value: string) {
        this._url = value;
    }

    get clientSecret(): string {
        return this._clientSecret;
    }

    set clientSecret(value: string) {
        this._clientSecret = value;
    }

    get client_id(): number {
        return this._client_id;
    }

    set client_id(value: number) {
        this._client_id = value;
    }

    get client_secret(): string {
        return this._client_secret;
    }

    set client_secret(value: string) {
        this._client_secret = value;
    }

    get grant_type(): string {
        return this._grant_type;
    }

    set grant_type(value: string) {
        this._grant_type = value;
    }

    get headers(): Headers {
        return this._headers;
    }

    set headers(value: Headers) {
        this._headers = value;
    }

    tokenHeader(){
        if(this._cookiesTokenService.getToken() != null){
            this.headers.set('authorization', 'Bearer '+ JSON.parse(localStorage.getItem('token')).token);
        }
    }

    // CRUD API
    get(chaineUrl:string){
        this.tokenHeader();
        this.headers.set('Access-Control-Allow-Methods', 'GET');

        return this._http.get(this._url+"/"+chaineUrl, {headers:this.headers})
        .map(response => response.json(),error => this.error(error));
    }

    put(creds:Object, chaineUrl:string){
        this.tokenHeader();
        this.headers.set('Access-Control-Allow-Methods', 'PUT');
        let method = 'PUT';
        return this._http.put(this._url+"/"+chaineUrl, JSON.stringify(creds), {headers:this.headers, method: method})
        .map(response => response.json(),error => this.error(error));
  }

  post(creds:Object, chaineUrl:string){
    // if('login_check' !=chaineUrl ){
    this.tokenHeader();
    let method = 'POST';
      return new Promise (resolve => {
        this._http.post(this._url+"/"+chaineUrl, JSON.stringify(creds), {headers: this.headers, method: method})

            .map(response => response.json())
            .subscribe(data => {
                this._data = data;
                resolve(data);
            }, (error) => {
                this.error(error);
            });
        });
        // }else{
        //   return new Promise (resolve => {
        //     this._http.post(this._url+"/"+chaineUrl, JSON.stringify(creds))
        //         .map(response => response.json())
        //         .subscribe(data => {
        //           this._data = data;
        //           resolve(data);
        //         });
        //   });

        // }
    }

    error(data){
        let code = data.status || JSON.parse(data._body).code
        let message = data.statusText || JSON.parse(data._body).message
        if (code.toString()[0] == "4" || code.toString()[0] == "5"){
            this._router.navigate(['/error'], {
                queryParams: {
                    code: parseInt(code),
                    message: message
                }
            });
        } else {
            // this._flashMessagesService.show(JSON.parse(data._body).message, { cssClass: 'alert-danger', timeout: 10000 });
        }
    }

    delete(chaineUrl:string){
        this.tokenHeader();
        this.headers.set('Access-Control-Allow-Methods', 'DELETE');

        return this._http.delete(this._url+"/"+chaineUrl, {headers:this.headers})
        .map(response => response.json());
    }

}
