/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { HttpApiErpssiService } from './http-api-erpssi.service';

describe('HttpApiErpssiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HttpApiErpssiService]
    });
  });

  it('should ...', inject([HttpApiErpssiService], (service: HttpApiErpssiService) => {
    expect(service).toBeTruthy();
  }));
});
