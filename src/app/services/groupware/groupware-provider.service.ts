import { Injectable } from '@angular/core';
import { Http }          from '@angular/http';
import { Router } from '@angular/router';

import 'rxjs/add/operator/toPromise';
import {Groupware} from '../../model/groupware.model';
import {CommonProvider} from "../common/common-provider.service";
import {CookiesTokenService} from "../cookies/cookies-token.service";

const GROUPWARE_DATA_URI = 'http://erpssi.sandbox.skilvioo.com/api/roles';
const GROUPWARE_EMPLOYEE_DATA_URI = 'http://erpssi.sandbox.skilvioo.com/api/employee/role';
const GROUPWARE_DATA_URI_PARAM = 'http://erpssi.sandbox.skilvioo.com/api/roles/';
const GROUPWARE_SEND_DATA_URI = 'http://erpssi.sandbox.skilvioo.com/api/employees/role';
const GROUPWARE_EMPLOYEE_DATA_URI_PARAM = 'http://erpssi.sandbox.skilvioo.com/api/employee/role';

@Injectable()
export class GroupwareProvider extends CommonProvider{
    private groupware: Groupware;

    constructor(http: Http, cookiesTokenService: CookiesTokenService, router: Router) {
        super(http, cookiesTokenService, router);
    };

    getAll(employees): Promise<Groupware> {
        return new Promise((resolve, reject) => {
            this.http.get(GROUPWARE_DATA_URI, { headers: this.getHeader() }).toPromise().then(response => {
                let json = JSON.parse(response['_body']).data;
                let groupware:Groupware;
                // for(let k in json) {
                //     if(json.hasOwnProperty(k)) {
                //         groupware.push(GroupwareProvider.createGroupwareModel(json[k]));
                //     }
                // }
                groupware = GroupwareProvider.createGroupwareModel(json,employees);
                this.groupware = groupware;
                resolve(this.groupware);
            }).catch(error => {
                this.error(error)
            });
        });
    }

    post(groupware): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.post(GROUPWARE_SEND_DATA_URI, { data: groupware }, { headers: this.getHeader() }).toPromise().then(response => {
                let json = JSON.parse(response['_body']).data;
            }).catch(error => {
                this.error(error)
            });
        });
    }

    private static createGroupwareModel(json: any, employees): Groupware {
        let groupwareModel = new Groupware();
        groupwareModel._employees = employees;
        groupwareModel.hydrateModelFromJSON(groupwareModel, json);
        return groupwareModel;
    }
}
