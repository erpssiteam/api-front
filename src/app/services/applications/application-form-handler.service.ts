import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import {CommonProvider} from '../common/common-provider.service';
import {Http} from '@angular/http';
import {CookiesTokenService} from '../cookies/cookies-token.service';
import {Router} from "@angular/router";
const APPLICATION_DATA_URI_PARAM = 'http://erpssi.sandbox.skilvioo.com/api/applies/upload-cv/';

@Injectable()
export class ApplicationFormHandlerProvider extends CommonProvider {

  formErrors = {
    'name': '',
    'firstname': '',
    'lastname': '',
    'email': '',
    'address': '',
    'zip_code': '',
    'city': '',
  };
  validationMessages = {
    'name': {
      'required':      'Le titre est obligatoire.'
    },
    'firstname': {
      'required':      'Le prénom est obligatoire.'
    },
    'lastname': {
      'required':      'Le nom de famille est obligatoire.'
    },
    'email': {
      'required':      "L'email est obligatoire."
    },
    'address': {
      'required':     "L'adresse est obligatoire."
    },
    'zip_code': {
      'required':      'Le code postal est obligatoire.',
      'maxlength':     'Code postal invalide'
    },
    'city': {
      'required':      'La ville est obligatoire.'
    }
  };
  filePlaceholder = 'Importer votre CV...';
  configDatePicker = {
    format: 'DD-MM-YYYY',
    initialDate: new Date(),
    locale: 'fr'
  };

  constructor(http: Http, cookiesTokenService: CookiesTokenService, router: Router) {
    super(http, cookiesTokenService, router);
  };

  getConfigForUpload(id): Object {
    return {
      url: APPLICATION_DATA_URI_PARAM + id,
      authToken: this.getToken()
    };
  };

}
