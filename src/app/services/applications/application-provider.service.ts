import { Injectable } from '@angular/core';
import { Http }          from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import { CommonProvider } from '../common/common-provider.service';
import { CookiesTokenService } from '../cookies/cookies-token.service';
import {Application} from "../../model/application.model";
import {Offer} from "../../model/offer.model";

const APPLICATION_DATA_URI = 'http://erpssi.sandbox.skilvioo.com/api/applies';
const APPLICATION_DATA_URI_PARAM = 'http://erpssi.sandbox.skilvioo.com/api/applies/';

@Injectable()
export class ApplicationProvider extends CommonProvider {

  private applicationsMap = [];

  constructor(http: Http, cookiesTokenService: CookiesTokenService, router : Router) {
    super(http, cookiesTokenService, router);
  };

  getAll(): Promise<Application[]> {
    return new Promise((resolve, reject) => {
      if (this.applicationsMap.length === 0) {
        this.http.get(APPLICATION_DATA_URI, { headers: this.getHeader() }).toPromise().then(response => {
          resolve(this.parseJson(response));
        });
      } else {
        resolve(this.applicationsMap);
      }
    });
  };

  get(id: Number): Promise<Application> {
    return new Promise((resolve, reject) => {
      this.http.get(APPLICATION_DATA_URI_PARAM + id, { headers: this.getHeader() }).toPromise().then(response => {
        let json = JSON.parse(response['_body']).data;
        let applicationModel = ApplicationProvider.createApplicationModel(json);
        resolve(applicationModel);
      });
    });
  };

  remove(id: Number): Promise<Application[]> {
    return new Promise((resolve, reject) => {
      this.http.delete(APPLICATION_DATA_URI_PARAM + id, { headers: this.getHeader() }).toPromise().then(response => {
        // toast with response message
        this.applicationsMap.forEach((n, index) => {
          if (n.getId() === id) {
            this.applicationsMap.splice(index, 1);
          }
        });
      });
      resolve(this.applicationsMap);
    });
  };

  post(application: Application, offer: Offer): Promise<Application> {
    return new Promise((resolve, reject) => {
      this.http.post(APPLICATION_DATA_URI_PARAM + 'create/' + offer, { data: application }, { headers: this.getHeader() }).toPromise().then(response => {
          let json = JSON.parse(response['_body']).data;
          let newsModel = ApplicationProvider.createApplicationModel(json);
          this.applicationsMap.push(newsModel);
          resolve(this.applicationsMap);
      });
    });
  };

  put(application: Application): Promise<Application> {
    return new Promise((resolve, reject) => {
      this.http.put(APPLICATION_DATA_URI_PARAM + application.getId(), { data: application }, { headers: this.getHeader() }).toPromise().then(response => {
        let json = JSON.parse(response['_body']).data;
        application.hydrateModelFromJSON(application, json);
        resolve(application);
      });
    });
  };

  validate(state:number, application: Application): Promise<Application[]> {
    return new Promise((resolve, reject) => {
      this.http.put(APPLICATION_DATA_URI_PARAM + application.getId() + '/handle/' + state, {}, { headers: this.getHeader() }).toPromise().then(response => {
        let json = JSON.parse(response['_body']).data;
        application.hydrateModelFromJSON(application, json);
        // toast with response message
        this.applicationsMap.forEach((n, index) => {
          if (n.getId() === application.getId()) {
            this.applicationsMap[index] = application;
          }
        });
        resolve(this.applicationsMap);
      });
    });
  };

  private static createApplicationModel(json: any): Application {
    let application = new Application();
    application.hydrateModelFromJSON(application, json);
    return application;
  };

  private parseJson(response: any): any {
    let json = JSON.parse(response['_body']).data;
    let applications = [];
    for (let k in json) {
      if (json.hasOwnProperty(k)) {
        applications.push(ApplicationProvider.createApplicationModel(json[k]));
      }
    }
    this.applicationsMap = applications;
    return this.applicationsMap;
  };

}
