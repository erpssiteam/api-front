import { Component, OnInit } from '@angular/core';
import {SkillProvider} from "../../../services/skill/skill-provider.service";
import {ActivatedRoute} from "@angular/router";
const FEATURE_SKILL = 'feature_skills';

@Component({
  selector: 'app-skill-update',
  templateUrl: './skill-update.component.html',
  styleUrls: ['./skill-update.component.css'],
  providers: [SkillProvider]
})

export class SkillUpdateComponent implements OnInit {

  private skill: any;
  private id: Number;
  private voter: Boolean = false;

  constructor(private skillProvider: SkillProvider, private router: ActivatedRoute) {
    this.voter = this.skillProvider.voteFactory(FEATURE_SKILL);
    this.router.params.subscribe(params => {
      this.id = +params['id'];
    });
  }

  ngOnInit() {
    if(this.voter)
      this.skillProvider.get(this.id).then(s => {
        this.skill = s;
      });
  }
}
