import { Component, OnInit } from '@angular/core';
import {SkillProvider} from "../../../services/skill/skill-provider.service";
import {Skill} from "../../../model/skill.model";
const FEATURE_SKILL = 'feature_skills';

@Component({
  selector: 'app-skill-create',
  templateUrl: './skill-create.component.html',
  styleUrls: ['./skill-create.component.css'],
  providers: [SkillProvider]
})
export class SkillCreateComponent implements OnInit {

  private skill: Skill;
  private voter: Boolean = false;

  constructor(private skillProvider: SkillProvider) {
    this.voter = this.skillProvider.voteFactory(FEATURE_SKILL);
    this.skill = new Skill();
  }

  ngOnInit() {
  }

}
