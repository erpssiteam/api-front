import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkillEmployeeComponent } from './skill-employee.component';

describe('SkillEmployeeComponent', () => {
  let component: SkillEmployeeComponent;
  let fixture: ComponentFixture<SkillEmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkillEmployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkillEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
