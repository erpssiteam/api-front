import { Component, OnInit } from '@angular/core';
import {SkillProvider} from "../../../services/skill/skill-provider.service";
import {Employee} from "../../../model/employee.model";
import {EmployesProvider} from "../../../services/employes/employes-provider.service";
import {Skill} from "../../../model/skill.model";
import {Categorie} from "../../../model/categorie.model";
import {EmployeeSkill} from "../../../model/employeeskill.model";
import { NgForm } from '@angular/forms';
import * as moment from 'moment';
const FEATURE_SKILL = 'feature_skills';

@Component({
    selector: 'app-skill-employee',
    templateUrl: './skill-employee.component.html',
    styleUrls: ['./skill-employee.component.css'],
    providers: [SkillProvider, EmployesProvider]
})
export class SkillEmployeeComponent implements OnInit {
    private skillsForm: NgForm;
    private employees: Employee[];
    private employee: Employee;
    private skills: Array<Skill> = [];
    private skillsEmployees: Array<EmployeeSkill> = [];
    private voter: Boolean = false;
    private idEmployee: Number = 0;
    private returnEmplopyeeSkill: Array<EmployeeSkill> = [];

    constructor(private skillProvider: SkillProvider, private employeeProvider: EmployesProvider) {
        this.voter = this.skillProvider.voteFactory(FEATURE_SKILL);
    }

    ngOnInit() {

        this.employeeProvider.getAll().then(e => { 
            this.employees = e;
        });
        this.skillProvider.getAll().then(s => {
            this.skills = s
        });
    }

    /**
    * GET
    */
    onEmployeeChange(event: any) {
        this.idEmployee = parseInt(event.target.value);

        if(this.idEmployee > 0) {
            this.skillProvider.getByEmployees(this.idEmployee).then(s => {

                for(let skill of s){

                    if(skill._validate) {
                        this.returnEmplopyeeSkill[this.idEmployee+"-"+skill._id] = skill
                    }

                    this.skillsEmployees[this.idEmployee+"-"+skill._id] = skill
                }
            });
        }
    }

    /**
    * UPDATE
    */
    onChanged(code, event){
        let split = code.split("-")
        let employeeSkill = this.skillsEmployees[code]

        if(employeeSkill == undefined){
            employeeSkill = new EmployeeSkill()
            employeeSkill._id = parseInt(split[1])
        }

        employeeSkill._validate = event

        this.returnEmplopyeeSkill[code] = employeeSkill
    }

    handleSkills(){
        for(let employeeSkill in this.returnEmplopyeeSkill){
            this.returnEmplopyeeSkill[employeeSkill]._employee = this.idEmployee
            this.returnEmplopyeeSkill[employeeSkill]._date_skill = undefined//new Date()
            // this.returnEmplopyeeSkill[employeeSkill]._date_skill = moment(this.returnEmplopyeeSkill[employeeSkill]._date_skill, "DD-MM-YYYY").format("YYYY-MM-DD")

            this.skillProvider.postByEmployees(this.returnEmplopyeeSkill[employeeSkill]).then(s => {

            });
        }
    }

}
