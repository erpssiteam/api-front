import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkillRootComponent } from './skill-root.component';

describe('SkillRootComponent', () => {
  let component: SkillRootComponent;
  let fixture: ComponentFixture<SkillRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkillRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkillRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
