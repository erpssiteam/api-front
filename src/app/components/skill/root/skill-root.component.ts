import { Component, OnInit } from '@angular/core';
import {SkillProvider} from "../../../services/skill/skill-provider.service";
const FEATURE_SKILL = 'feature_skills';

@Component({
  selector: 'app-skill-root',
  templateUrl: './skill-root.component.html',
  styleUrls: ['./skill-root.component.css'],
  providers: [SkillProvider]
})
export class SkillRootComponent implements OnInit {

  private voter: Boolean = false;

  constructor(private skillProvider: SkillProvider) {
    this.voter = this.skillProvider.voteFactory(FEATURE_SKILL);
  }

  ngOnInit() {
  }
}
