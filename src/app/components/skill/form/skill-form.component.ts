import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Skill} from "../../../model/skill.model";
import {DateModel} from "ng2-datepicker";
import {NgForm} from "@angular/forms";
import {SkillProvider} from "../../../services/skill/skill-provider.service";
import {Router} from "@angular/router";
import {SkillFormHandler} from '../../../services/skill/skill-form-handler.service';

@Component({
  selector: 'app-skill-form',
  templateUrl: './skill-form.component.html',
  styleUrls: ['./skill-form.component.css'],
  providers: [SkillProvider, SkillFormHandler]
})
export class SkillFormComponent implements OnInit {

  @Input() private skill: Skill;
  @Input() private isCreationStrategy: Boolean;
  private submitted: Boolean;
  skillForm: NgForm;
  @ViewChild('skillForm') currentForm: NgForm;
  formErrors: Object;
  validationMessages: Object;

  constructor(private skillProvider: SkillProvider, private router: Router, private skillFormHandler: SkillFormHandler) {
    this.formErrors = skillFormHandler.formErrors;
    this.validationMessages = skillFormHandler.validationMessages;
  }


  ngAfterViewChecked(): void {
    this.formChanged();
  }

  formChanged() {
    if (this.currentForm === this.skillForm) { return; }
    this.skillForm = this.currentForm;
    if (this.skillForm) {
      this.skillForm.valueChanges
        .subscribe(data => this.onValueChanged(data));
    }
  }
  onValueChanged(data?: any) {
    if (!this.skillForm) { return; }
    const form = this.skillForm.form;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  };

  ngOnInit() {
    this.submitted = false;

  }

  onSubmit() {
    this.submitted = true;
  }

  handleSkill() {
    if (this.isCreationStrategy === true) {
      this.skillProvider.post(this.skill).then(s => {
        this.router.navigate(['skills']);
      });
    } else {
      this.skillProvider.put(this.skill).then(s => {
        this.router.navigate(['skills']);
      });
    }
  }

}
