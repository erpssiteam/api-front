import {Component, Input, OnInit} from '@angular/core';
import {SkillProvider} from "../../../services/skill/skill-provider.service";
import {Skill} from "../../../model/skill.model";
const FEATURE_SKILLS = 'feature_skills';
@Component({
  selector: 'app-skill-list',
  templateUrl: './skill-list.component.html',
  styleUrls: ['./skill-list.component.css'],
  providers: [SkillProvider]
})
export class SkillListComponent implements OnInit {

  private voter: Boolean = false;
  private skills: Skill[];

  constructor(private skillProvider: SkillProvider) {
    this.voter = this.skillProvider.voteFactory(FEATURE_SKILLS);
  }

  ngOnInit() {
    this.skillProvider.getAll().then(n => {
      this.skills = n;
    });
  }

  deleteSkill(skillId: Number): void {
    if(this.voter)
      this.skillProvider.remove(skillId).then(n => {
        this.skills = n;
      });
  }

}
