import { Component, OnInit } from '@angular/core';
import {ApplicationProvider} from '../../../services/applications/application-provider.service';
import {Application} from '../../../model/application.model';
const FEATURE_APPLICATION = 'feature_application';
const APPLY_PENDING_STATE = 1;
const APPLY_VALIDATED_STATE = 2;
const APPLY_DENIED_STATE = 3;

@Component({
  selector: 'app-application-list',
  templateUrl: './application-list.component.html',
  styleUrls: ['./application-list.component.css'],
  providers: [ApplicationProvider]
})
export class ApplicationListComponent implements OnInit {

  private voter: Boolean = false;
  private applications: Application[];

  constructor(private applicationProvider: ApplicationProvider) {
    this.voter = this.applicationProvider.voteFactory(FEATURE_APPLICATION);
  }

  ngOnInit() {
    if (this.voter)
    this.applicationProvider.getAll().then(a => {
        this.applications = a;
    });
  }

  deleteApplication(applicationId: Number): void {
    if(this.voter)
      this.applicationProvider.remove(applicationId).then(a => {
        this.applications = a;
      });
  }

  validateApplication(application: Application): void {
    if(this.voter) {
      let state: number = 0;
      switch (application.getState()) {
        case 1: state = APPLY_VALIDATED_STATE; break;
        case 2: state = APPLY_DENIED_STATE; break;
        case 3: state = APPLY_VALIDATED_STATE; break;
      }
      this.applicationProvider.validate(state, application).then(a => {
        this.applications = a;
      });

    }
  }

}
