import { Component, OnInit } from '@angular/core';
import {Application} from "../../../model/application.model";
import {ApplicationProvider} from "../../../services/applications/application-provider.service";
import {ActivatedRoute} from "@angular/router";
const FEATURE_APPLICATION = 'feature_application';

@Component({
  selector: 'app-application-view',
  templateUrl: './application-view.component.html',
  styleUrls: ['./application-view.component.css'],
  providers: [ApplicationProvider]
})
export class ApplicationViewComponent implements OnInit {

  private voter: Boolean = false;
  private application: Application;
  private id: Number;

  constructor(private applicationProvider: ApplicationProvider, private router: ActivatedRoute) {
    this.voter = this.applicationProvider.voteFactory(FEATURE_APPLICATION);
    this.router.params.subscribe(params => {
      this.id = +params['id'];
    });
  }

  ngOnInit() {
    if(this.voter)
    this.applicationProvider.get(this.id).then(a => {
      this.application = a;
    });
  }
}
