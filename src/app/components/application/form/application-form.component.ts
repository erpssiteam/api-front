import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Application} from "../../../model/application.model";
import {DateModel, DatePickerOptions} from "ng2-datepicker";
import {FileUploader} from "ng2-file-upload";
import {NgForm} from "@angular/forms";
import {ApplicationProvider} from "../../../services/applications/application-provider.service";
import {Router} from "@angular/router";
import * as moment from 'moment';
import {ApplicationFormHandlerProvider} from "../../../services/applications/application-form-handler.service";
import {Offer} from "../../../model/offer.model";
const FEATURE_APPLICATION = 'feature_application';

@Component({
  selector: 'app-application-form',
  templateUrl: './application-form.component.html',
  styleUrls: ['./application-form.component.css'],
  providers: [ApplicationProvider, ApplicationFormHandlerProvider]
})
export class ApplicationFormComponent implements OnInit {

  private voter: Boolean = false;
  @Input() private application: Application;
  @Input() private isCreationStrategy: Boolean;
  @Input() private offers: Offer[];
  private offer: Offer;
  private submitted: Boolean;
  date: DateModel;
  options: DatePickerOptions;
  uploader: FileUploader;
  applicationForm: NgForm;
  filePlaceholder: string;
  @ViewChild('applicationForm') currentForm: NgForm;
  formErrors: Object;
  validationMessages: Object;

  constructor(private applicationProvider: ApplicationProvider, private router: Router, private applicationFormHandler: ApplicationFormHandlerProvider) {
    this.voter = this.applicationProvider.voteFactory(FEATURE_APPLICATION);
    this.uploader = new FileUploader({});
      this.formErrors = this.applicationFormHandler.formErrors;
      this.validationMessages = this.applicationFormHandler.validationMessages;
      this.filePlaceholder = this.applicationFormHandler.filePlaceholder;
  }

  ngAfterViewChecked(): void {
    this.formChanged();
  }

  formChanged() {
    if (this.currentForm === this.applicationForm) { return; }
    this.applicationForm = this.currentForm;
    if (this.applicationForm) {
      this.applicationForm.valueChanges
        .subscribe(data => this.onValueChanged(data));
    }
  }

  onValueChanged(data?: any) {
    if (!this.applicationForm) { return; }
    const form = this.applicationForm.form;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  };

  ngOnInit() {
    this.submitted = false;
    if (this.application.getCv() != null) {
      this.filePlaceholder = 'Changer le CV...';
    }
    if (this.application.getBirthdayDate()) {
      this.applicationFormHandler.configDatePicker.initialDate = this.application.getFormatedDateToDatePicker(moment(this.application.getBirthdayDate(), 'DD-MM-YYYY').toString());
    }
    this.options = new DatePickerOptions(this.applicationFormHandler.configDatePicker);
  }

  onSubmit() {
    this.submitted = true;
  }

  handleApplication() {
    if(this.voter) {
      this.application.setBirthdayDate(this.date.formatted);
      if (this.isCreationStrategy === true) {
        this.applicationProvider.post(this.application, this.offer).then(a => {
          this.uploader.setOptions(this.applicationFormHandler.getConfigForUpload(a[0].id));
          this.uploader.uploadAll();
          this.router.navigate(['applications']);
        });
      } else {
        this.applicationProvider.put(this.application).then(application => {
          this.uploader.setOptions(this.applicationFormHandler.getConfigForUpload(application.getId()));
          this.uploader.uploadAll();
          this.router.navigate(['applications']);
        });
      }
    }
  }


}
