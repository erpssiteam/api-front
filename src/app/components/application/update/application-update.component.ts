import { Component, OnInit } from '@angular/core';
import {ApplicationProvider} from "../../../services/applications/application-provider.service";
import {ActivatedRoute} from "@angular/router";
const FEATURE_APPLICATION = 'feature_application';

@Component({
  selector: 'app-application-update',
  templateUrl: './application-update.component.html',
  styleUrls: ['./application-update.component.css'],
  providers: [ApplicationProvider]
})
export class ApplicationUpdateComponent implements OnInit {

  private application: any;
  private id: Number;
  private voter: Boolean = false;

  constructor(private applicationProvider: ApplicationProvider, private router: ActivatedRoute) {
    this.voter = this.applicationProvider.voteFactory(FEATURE_APPLICATION);
    this.router.params.subscribe(params => {
      this.id = +params['id'];
    });
  }

  ngOnInit() {
    if(this.voter)
      this.applicationProvider.get(this.id).then(a => {
      this.application = a;
    });
  }

}
