import { Component, OnInit } from '@angular/core';
import {ApplicationProvider} from "../../../services/applications/application-provider.service";
import {Application} from "../../../model/application.model";
import {OfferProvider} from "../../../services/offer/offer-provider.service";
import {Offer} from "../../../model/offer.model";
const FEATURE_APPLICATION = 'feature_application';

@Component({
  selector: 'app-application-create',
  templateUrl: './application-create.component.html',
  styleUrls: ['./application-create.component.css'],
  providers: [ApplicationProvider, OfferProvider]
})
export class ApplicationCreateComponent implements OnInit {

  private voter: Boolean = false;
  private application: Application;
  private offers: Offer[];

  constructor(private offerProvider: OfferProvider, private applicationProvider: ApplicationProvider) {
    this.voter = this.applicationProvider.voteFactory(FEATURE_APPLICATION);
    if(this.voter)
    this.offerProvider.getAll().then(o => {
      this.offers = o;
      this.application = new Application();
    });
  }

  ngOnInit() {
  }

}
