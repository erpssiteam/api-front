import {Component, OnInit} from '@angular/core';
import {OfferProvider} from "../../../services/offer/offer-provider.service";
import {Offer} from "../../../model/offer.model";
import {ActivatedRoute} from "@angular/router";

const FEATURE_JOBPROFILE = 'feature_jobprofile';

@Component({
    selector: 'app-offer-update',
    templateUrl: './offer-update.component.html',
    styleUrls: ['./offer-update.component.css'],
    providers: [OfferProvider],
})
export class OfferUpdateComponent implements OnInit {
    private offer: any;
    private id: Number;
    private idJobProfile:Number;
    private voter: Boolean = false;

    constructor(private offerProvider: OfferProvider, private router: ActivatedRoute) {
        this.router.params.subscribe(params => {
            this.id = +params['id'];
        });
        this.router.queryParams.subscribe(params => {
            this.idJobProfile = +params['jobProfile'];
        });
        this.voter = this.offerProvider.voteFactory(FEATURE_JOBPROFILE);
    }

    ngOnInit() {
        this.offerProvider.get(this.id).then(h => {
            this.offer = h;
        });
    }
}
