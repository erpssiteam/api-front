import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {OfferProvider} from "../../../services/offer/offer-provider.service";
import {Offer} from "../../../model/offer.model";
import {ActivatedRoute} from "@angular/router";

const FEATURE_JOBPROFILE = 'feature_jobprofile';

@Component({
    selector: 'app-offer-create',
    templateUrl: './offer-create.component.html',
    providers: [OfferProvider],
    styleUrls: ['./offer-create.component.scss'],
})
export class OfferCreateComponent implements OnInit {
    private idJobProfile:Number;
    private offer: Offer;
    private voter: Boolean = false;

    constructor(private offerProvider: OfferProvider, private router: ActivatedRoute) {
        this.offer = new Offer();
        this.router.queryParams.subscribe(params => {
            this.idJobProfile = +params['jobProfile'];
        });
        this.voter = this.offerProvider.voteFactory(FEATURE_JOBPROFILE);
    }

    ngOnInit() {
    }


}
