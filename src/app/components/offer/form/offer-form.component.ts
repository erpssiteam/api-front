import { Component, Input, OnInit, AfterViewChecked, ViewChild } from '@angular/core';
import { Offer } from '../../../model/offer.model';
import { Customer } from '../../../model/customer.model';
import { OfferProvider } from '../../../services/offer/offer-provider.service';
import { CustomersProvider } from '../../../services/customers/customers-provider.service';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { NgForm } from '@angular/forms';
import { OfferFormHandler } from '../../../services/offer/offer-form-handler.service';
import { DatePickerOptions, DateModel } from 'ng2-datepicker';

@Component({
    selector: 'app-offer-form',
    templateUrl: './offer-form.component.html',
    styleUrls: ['./offer-form.component.css'],
    providers: [OfferProvider,OfferFormHandler,CustomersProvider]

})
export class OfferFormComponent implements OnInit, AfterViewChecked {

    @Input() private offer: Offer;
    @Input() private isCreationStrategy: Boolean;
    @Input() private idJobProfile: Number;
    @ViewChild('offerForm') currentForm: NgForm;

    private customersArray: Array<Customer> = [];
    private offerForm: NgForm;
    private formErrors: Object;
    private validationMessages: Object;
    private start_at: DateModel;
    private end_at: DateModel;
    private optionsStartAt: DatePickerOptions;
    private optionsEndAt: DatePickerOptions;

    constructor(private customersProvider:CustomersProvider, private offerProvider: OfferProvider, private router: Router, private offerFormHanlder: OfferFormHandler, private _route: ActivatedRoute) {
        this.formErrors = this.offerFormHanlder.formErrors;
        this.validationMessages = this.offerFormHanlder.validationMessages;
    }

    ngAfterViewChecked(): void {
        this.formChanged();
    }

    formChanged() {

        if (this.currentForm === this.offerForm) { return; }

        this.offerForm = this.currentForm;

        if (this.offerForm) {
            this.offerForm.valueChanges.subscribe(data => this.onValueChanged(data));
        }
    }

    onValueChanged(data?: any) {

        if (!this.offerForm) { return; }

        const form = this.offerForm.form;

        for (const field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = '';

            const control = form.get(field);

            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];

                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    };

    ngOnInit() {
        this.customersProvider.getAll().then(c => {
            this.customersArray = c
        });

        if (this.offer._start_at) {
            this.offerFormHanlder.configStartAt.initialDate = this.offer.getFormatedDateToDatePicker(moment(this.offer._start_at, "DD-MM-YYYY").toString());
        }
        if (this.offer._end_at) {
            this.offerFormHanlder.configEndAt.initialDate = this.offer.getFormatedDateToDatePicker(moment(this.offer._end_at, "DD-MM-YYYY").toString());
        }
        this.optionsStartAt = new DatePickerOptions(this.offerFormHanlder.configStartAt);
        this.optionsEndAt = new DatePickerOptions(this.offerFormHanlder.configEndAt);
    }

    handleOffer() {
        if (this.isCreationStrategy === true) {
            this.offer._job_profile = this.idJobProfile;
            this.offer._end_at = this.end_at.formatted;
            this.offer._start_at = this.start_at.formatted;
            this.offer._created_at = moment(new Date(), "DD-MM-YYYY").format("DD-MM-YYYY")
            this.offer._modified_at = moment(new Date(), "DD-MM-YYYY").format("DD-MM-YYYY")

            this.offerProvider.post(this.offer).then(h => {
                this.router.navigate(['jobs/offers'], {
                    queryParams: {
                        jobProfile: this.idJobProfile
                    }
                });
            });
        } else {
            this.offer._end_at = this.end_at.formatted;
            this.offer._start_at = this.start_at.formatted;
            this.offer._modified_at = moment(new Date(), "DD-MM-YYYY").format("DD-MM-YYYY")
            this.offer._created_at = moment(this.offer._created_at.date).format("DD-MM-YYYY")
            
            this.offerProvider.put(this.offer).then(h => {
                this.router.navigate(['jobs/offers'], {
                    queryParams: {
                        jobProfile: this.idJobProfile
                    }
                });
            });
        }
    }
}
