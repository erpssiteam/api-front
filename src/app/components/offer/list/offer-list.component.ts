import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { OfferProvider } from '../../../services/offer/offer-provider.service';
import { Offer } from '../../../model/offer.model';
import { ActivatedRoute, Params, Router } from '@angular/router';

const FEATURE_JOBPROFILE = 'feature_jobprofile';

@Component({
    selector: 'app-offer-list-account',
    templateUrl: './offer-list.component.html',
    providers: [OfferProvider],
    styleUrls: ['./offer-list.component.scss'],
})
export class OfferListComponent implements OnInit {
    @Input() private offerCommon: Array<Offer> = [];
    private idJobProfile:Number;
    private voter: Boolean = false;

    constructor(private offerProvider: OfferProvider, private _route: ActivatedRoute, private router: Router) {
        this.voter = this.offerProvider.voteFactory(FEATURE_JOBPROFILE);
    }

    navigateOffer(offerId:Number): void{

        if(offerId == 0){
            this.router.navigate(['jobs/offers/create'], {
                queryParams: {
                    jobProfile: this.idJobProfile
                }
            });
        } else {
            this.router.navigate(['jobs/offers/update/' + offerId], {
                queryParams: {
                    jobProfile: this.idJobProfile
                }
            });
        }
    }

    validOffer(offer){

        if(this.voter){
            offer._is_valid = 1;
            
            this.offerProvider.put(offer).then(h => {
                this.router.navigate(['jobs/offers'], {
                    queryParams: {
                        jobProfile: this.idJobProfile
                    }
                });
            });
        }
    }

    ngOnInit() {
        this._route.queryParams.subscribe(params => {
            this.idJobProfile = +params['jobProfile'];
        });

        this.offerProvider.getAll().then(o => {

            for(let offer of o){

                if(this.idJobProfile === offer._job_profile){
                    this.offerCommon.push(offer)
                }
            }
        });
    }

    /**
    * @param offerId
    */
    deleteOffer(offerId: Number): void {
        this.offerProvider.remove(offerId).then(h => {
            this.offerCommon = h;
        });
    }

    getStyle(value): String {
        if ( value ) {
            return 'green';
        } else {
            return 'red';
        }
    }
}
