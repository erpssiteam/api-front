import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {HolidaysProvider} from "../../../services/holidays/holidays-provider.service";
import {Holiday} from "../../../model/holiday.model";

const FEATURE_HOLIDAYS = 'feature_holiday';

@Component({
    selector: 'app-holidays-create',
    templateUrl: './holidays-create.component.html',
    providers: [HolidaysProvider],
    styleUrls: ['./holidays-create.component.scss'],
})
export class HolidaysCreateComponent implements OnInit {
    private voter: Boolean = false;
    private holiday: Holiday;

    constructor(private holidaysProvider: HolidaysProvider) {
        this.holiday = new Holiday();
        this.voter = this.holidaysProvider.voteFactory(FEATURE_HOLIDAYS);
    }

    ngOnInit() {
    }


}
