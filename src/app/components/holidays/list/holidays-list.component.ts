import {Component, OnInit, Input, Output, EventEmitter, AfterViewChecked} from '@angular/core';
import { HolidaysProvider } from '../../../services/holidays/holidays-provider.service';
import { Holiday } from '../../../model/holiday.model';
import { Router } from '@angular/router';

const FEATURE_HOLIDAYS = 'feature_holiday';

declare var jQuery:any;
declare var Materialize:any;

@Component({
    selector: 'app-holidays-list-account',
    templateUrl: './holidays-list.component.html',
    providers: [HolidaysProvider],
    styleUrls: ['./holidays-list.component.scss'],
})
export class HolidaysListComponent implements OnInit {
    @Input() private holidaysSelf: Array<Holiday> = [];
    @Input() private holidaysCommon: Array<Holiday> = [];
    private voter: Boolean = false;

    constructor(private holidaysProvider: HolidaysProvider, private router: Router) {
        this.voter = this.holidaysProvider.voteFactory(FEATURE_HOLIDAYS);
    }

    ngOnInit() {

        this.holidaysProvider.getAll().then(h => {
            this.holidaysSelf = []
            this.holidaysCommon = []

            for(let item of h){

                if(item._employee.id === this.holidaysProvider.getCurrentUserId()){
                    this.holidaysSelf.push(item)
                } else {
                    this.holidaysCommon.push(item)
                }
            }
        });
    }

    validHoliday(holiday){

        if(this.voter){
            holiday._state = 2;

            this.holidaysProvider.put(holiday).then(h => {
                this.router.navigate(["holidays"]);
            });
        }
    }

    /**
    * @param holidayId
    */
    deleteHolidays(holidayId: Number): void {
        this.holidaysProvider.remove(holidayId).then(h => {
            this.holidaysSelf = []
            this.holidaysCommon = []

            for(let item of h){

                if(item._employee.id === this.holidaysProvider.getCurrentUserId()){
                    this.holidaysSelf.push(item)
                } else {
                    this.holidaysCommon.push(item)
                }
            }
        });
    }
}
