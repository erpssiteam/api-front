import { Component, Input, OnInit, AfterViewChecked, ViewChild } from '@angular/core';
import { Holiday } from '../../../model/holiday.model';
import { HolidaysProvider } from '../../../services/holidays/holidays-provider.service';
import { DatePickerOptions, DateModel } from 'ng2-datepicker';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { NgForm } from '@angular/forms';
import { HolidaysFormHandler } from '../../../services/holidays/holidays-form-handler.service';

@Component({
    selector: 'app-holidays-form',
    templateUrl: './holidays-form.component.html',
    styleUrls: ['./holidays-form.component.css'],
    providers: [HolidaysProvider,HolidaysFormHandler]

})
export class HolidaysFormComponent implements OnInit, AfterViewChecked {

    @Input() private holiday: Holiday;
    @Input() private isCreationStrategy: Boolean;
    @ViewChild('holidayForm') currentForm: NgForm;

    private startedat: DateModel;
    private endedat: DateModel;
    private holidayForm: NgForm;
    private optionsStartedAt: DatePickerOptions;
    private optionsEndedAt: DatePickerOptions;
    private formErrors: Object;
    private validationMessages: Object;

    constructor(private holidaysProvider: HolidaysProvider, private router: Router, private holidaysFormHanlder: HolidaysFormHandler) {
        this.formErrors = this.holidaysFormHanlder.formErrors;
        this.validationMessages = this.holidaysFormHanlder.validationMessages;
    }

    ngAfterViewChecked(): void {
        this.formChanged();
    }

    formChanged() {

        if (this.currentForm === this.holidayForm) { return; }

        this.holidayForm = this.currentForm;

        if (this.holidayForm) {
            this.holidayForm.valueChanges.subscribe(data => this.onValueChanged(data));
        }
    }

    onValueChanged(data?: any) {

        if (!this.holidayForm) { return; }

        const form = this.holidayForm.form;

        for (const field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = '';

            const control = form.get(field);

            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];

                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    };

    ngOnInit() {
        if (this.holiday._started_at) {
            this.holidaysFormHanlder.configStartedAt.initialDate = this.holiday.getFormatedDateToDatePicker(moment(this.holiday._started_at, "DD-MM-YYYY").toString());
        }
        if (this.holiday._ended_at) {
            this.holidaysFormHanlder.configEndedAt.initialDate = this.holiday.getFormatedDateToDatePicker(moment(this.holiday._ended_at, "DD-MM-YYYY").toString());
        }
        this.optionsStartedAt = new DatePickerOptions(this.holidaysFormHanlder.configStartedAt);
        this.optionsEndedAt = new DatePickerOptions(this.holidaysFormHanlder.configEndedAt);
    }

    handleHoliday() {
        if (this.isCreationStrategy === true) {
            this.holiday._ended_at = this.endedat.formatted;
            this.holiday._started_at = this.startedat.formatted;

            this.holidaysProvider.post(this.holiday).then(h => {
                this.router.navigate(["holidays"]);
            });
        } else {
            this.holiday._ended_at = this.endedat.formatted;
            this.holiday._started_at = this.startedat.formatted;

            this.holidaysProvider.put(this.holiday).then(h => {
                this.router.navigate(["holidays"]);
            });
        }
    }
}
