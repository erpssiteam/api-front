import {Component, OnInit} from '@angular/core';
import {HolidaysProvider} from "../../../services/holidays/holidays-provider.service";
import {Holiday} from "../../../model/holiday.model";
import {ActivatedRoute} from "@angular/router";

const FEATURE_HOLIDAYS = 'feature_holiday';

@Component({
    selector: 'app-holidays-update',
    templateUrl: './holidays-update.component.html',
    styleUrls: ['./holidays-update.component.css'],
    providers: [HolidaysProvider],
})
export class HolidaysUpdateComponent implements OnInit {
    private voter: Boolean = false;
    private holiday: any;
    private id: Number;

    constructor(private holidaysProvider: HolidaysProvider, private router: ActivatedRoute) {
        this.router.params.subscribe(params => {
            this.id = +params['id'];
        });
        this.voter = this.holidaysProvider.voteFactory(FEATURE_HOLIDAYS);
    }

    ngOnInit() {
        this.holidaysProvider.get(this.id).then(h => {
            this.holiday = h;
        });
    }
}
