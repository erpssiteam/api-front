import { Component, OnInit } from '@angular/core';
import {News} from "../../../model/news.model";
import {NewsProvider} from "../../../services/news/news-provider.service";
const FEATURE_NEWS = 'feature_news';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.css'],
  providers: [NewsProvider]
})
export class NewsListComponent implements OnInit {

  private voter: Boolean = false;
  private news: News[];

  constructor(private newsProvider: NewsProvider) {
    this.voter = this.newsProvider.voteFactory(FEATURE_NEWS);
  }

  ngOnInit() {
    this.newsProvider.getAll().then(n => {
      this.news = n;
    });
  }

  deleteNews(newsId: Number): void {
    if(this.voter)
      this.newsProvider.remove(newsId).then(n => {
      this.news = n;
    });
  }

}
