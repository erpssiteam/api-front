import { Component, OnInit } from '@angular/core';
import {NewsProvider} from "../../../services/news/news-provider.service";
import {ActivatedRoute} from "@angular/router";
const FEATURE_NEWS = 'feature_news';

@Component({
  selector: 'app-news-update',
  templateUrl: './news-update.component.html',
  styleUrls: ['./news-update.component.css'],
  providers: [NewsProvider]
})
export class NewsUpdateComponent implements OnInit {

  private news: any;
  private id: Number;
  private voter: Boolean = false;

  constructor(private newsProvider: NewsProvider, private router: ActivatedRoute) {
    this.voter = this.newsProvider.voteFactory(FEATURE_NEWS);
    this.router.params.subscribe(params => {
      this.id = +params['id'];
    });
  }

  ngOnInit() {
    if(this.voter)
    this.newsProvider.get(this.id).then(n => {
      this.news = n;
    });
  }

}
