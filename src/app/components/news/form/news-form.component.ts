import {AfterViewChecked, Component, Input, OnInit, ViewChild} from '@angular/core';
import {NewsProvider} from "../../../services/news/news-provider.service";
import {NewsFormHandler} from "../../../services/news/news-form-handler.service";
import {Router} from "@angular/router";
import {News} from "../../../model/news.model";
import {DateModel, DatePickerOptions} from "ng2-datepicker";
import {NgForm} from "@angular/forms";
import * as moment from 'moment';

@Component({
  selector: 'app-news-form',
  templateUrl: './news-form.component.html',
  styleUrls: ['./news-form.component.css'],
  providers: [NewsProvider, NewsFormHandler]
})
export class NewsFormComponent implements OnInit, AfterViewChecked {

  @Input() private news: News;
  @Input() private isCreationStrategy: Boolean;
  private submitted: Boolean;
  date: DateModel;
  options: DatePickerOptions;
  newsForm: NgForm;
  @ViewChild('newsForm') currentForm: NgForm;
  formErrors: Object;
  validationMessages: Object;

  constructor(private newsProvider: NewsProvider, private router: Router, private newsFormHandler: NewsFormHandler) {
    this.formErrors = newsFormHandler.formErrors;
    this.validationMessages = newsFormHandler.validationMessages;
  }

  ngAfterViewChecked(): void {
    this.formChanged();
  }
  formChanged() {
    if (this.currentForm === this.newsForm) { return; }
    this.newsForm = this.currentForm;
    if (this.newsForm) {
      this.newsForm.valueChanges
        .subscribe(data => this.onValueChanged(data));
    }
  }
  onValueChanged(data?: any) {
    if (!this.newsForm) { return; }
    const form = this.newsForm.form;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  };

  ngOnInit() {
    this.submitted = false;
    if (this.news.getDateNews()) {
      this.newsFormHandler.configDatePicker.initialDate = this.news.getFormatedDateToDatePicker(moment(this.news.getDateNews(), 'DD-MM-YYYY').toString());
    }
    this.options = new DatePickerOptions(this.newsFormHandler.configDatePicker);
  }

  onSubmit() {
    this.submitted = true;
  }

  handleNews() {
    if (this.isCreationStrategy === true) {
      this.news.setDateNews(this.date.formatted);
      this.newsProvider.post(this.news).then(n => {
        this.router.navigate(['news']);
      });
    } else {
      this.news.setDateNews(this.date.formatted);
      this.newsProvider.put(this.news).then(n => {
        this.router.navigate(['news']);
      });
    }
  }

}
