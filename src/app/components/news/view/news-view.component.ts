import { Component, OnInit } from '@angular/core';
import {News} from "../../../model/news.model";
import {NewsProvider} from "../../../services/news/news-provider.service";
import {ActivatedRoute} from "@angular/router";
const FEATURE_NEWS = 'feature_news';

@Component({
  selector: 'app-news-view',
  templateUrl: './news-view.component.html',
  styleUrls: ['./news-view.component.css'],
  providers: [NewsProvider]
})
export class NewsViewComponent implements OnInit {
  private news: News;
  private id: Number;

  constructor(private newsProvider: NewsProvider, private router: ActivatedRoute) {
    this.router.params.subscribe(params => {
      this.id = +params['id'];
    });
  }

  ngOnInit() {
    this.newsProvider.get(this.id).then(n => {
      this.news = n;
    });
  }


}
