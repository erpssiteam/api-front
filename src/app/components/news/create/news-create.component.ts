import { Component, OnInit } from '@angular/core';
import {News} from "../../../model/news.model";
import {NewsProvider} from "../../../services/news/news-provider.service";
const FEATURE_NEWS = 'feature_news';

@Component({
  selector: 'app-news-create',
  templateUrl: './news-create.component.html',
  styleUrls: ['./news-create.component.css'],
  providers: [NewsProvider]
})
export class NewsCreateComponent implements OnInit {

  private news: News;
  private voter: Boolean = false;

  constructor(private newsProvider: NewsProvider) {
    this.voter = this.newsProvider.voteFactory(FEATURE_NEWS);
    this.news = new News("", "", new Date);
  }

  ngOnInit() {
  }

}
