import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {ExpenseAccountProvider} from "../../../services/expense-account/expense-account-provider.service";
import {ExpenseAccount} from "../../../model/expense-account.model";

@Component({
  selector: 'app-expense-account-create',
  templateUrl: './expense-account-create.component.html',
  providers: [ExpenseAccountProvider],
  styleUrls: ['./expense-account-create.component.scss'],
})
export class ExpenseAccountCreateComponent implements OnInit {

  private expenseAccount: ExpenseAccount;

  constructor(private expenseAccountProvider: ExpenseAccountProvider) {
    this.expenseAccount = new ExpenseAccount(this.expenseAccountProvider.getCurrentEmployee(), "", null, null);
  }

  ngOnInit() {
  }


}
