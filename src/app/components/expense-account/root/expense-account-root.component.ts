import { Component, OnInit } from '@angular/core';
import {ExpenseAccountProvider} from '../../../services/expense-account/expense-account-provider.service';
const FEATURE_EXPENSE_ACCOUNT = 'feature_expense_account';

@Component({
  selector: 'app-expense-account-root',
  templateUrl: './expense-account-root.component.html',
  styleUrls: ['./expense-account-root.component.css'],
  providers: [ExpenseAccountProvider],
})
export class ExpenseAccountRootComponent implements OnInit {

  private voter: Boolean = false;

  constructor(private expenseAccountProvider: ExpenseAccountProvider) {
    this.voter = this.expenseAccountProvider.voteFactory(FEATURE_EXPENSE_ACCOUNT);
  }

  ngOnInit() {
  }

}
