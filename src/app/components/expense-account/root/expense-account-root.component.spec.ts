import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpenseAccountRootComponent } from './expense-account-root.component';

describe('ExpenseAccountRootComponent', () => {
  let component: ExpenseAccountRootComponent;
  let fixture: ComponentFixture<ExpenseAccountRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpenseAccountRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpenseAccountRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
