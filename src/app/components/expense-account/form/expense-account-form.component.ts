import {Component, Input, OnInit, AfterViewChecked, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import {ExpenseAccount} from '../../../model/expense-account.model';
import {ExpenseAccountProvider} from '../../../services/expense-account/expense-account-provider.service';
import { DatePickerOptions, DateModel } from 'ng2-datepicker';
import {Router} from '@angular/router';
import * as moment from 'moment';
import {FileUploader} from 'ng2-file-upload';
import {ExpenseAccountFormHandler} from '../../../services/expense-account/expense-account-form-handler.service';

@Component({
  selector: 'app-expense-account-form',
  templateUrl: './expense-account-form.component.html',
  styleUrls: ['./expense-account-form.component.css'],
  providers: [ExpenseAccountProvider, ExpenseAccountFormHandler]

})
export class ExpenseAccountFormComponent implements OnInit, AfterViewChecked {

  @Input() private expenseAccount: ExpenseAccount;
  @Input() private isCreationStrategy: Boolean;
  private submitted: Boolean;
  date: DateModel;
  options: DatePickerOptions;
  uploader: FileUploader;
  expenseAccountForm: NgForm;
  filePlaceholder: string;
  @ViewChild('expenseAccountForm') currentForm: NgForm;
  formErrors: Object;
  validationMessages: Object;

  constructor(private expenseAccountProvider: ExpenseAccountProvider, private router: Router, private expenseAccountFormHanlder: ExpenseAccountFormHandler) {
    this.uploader = new FileUploader({});
    this.formErrors = expenseAccountFormHanlder.formErrors;
    this.validationMessages = expenseAccountFormHanlder.validationMessages;
    this.filePlaceholder = this.expenseAccountFormHanlder.filePlaceholder;
  }

  ngAfterViewChecked(): void {
    this.formChanged();
  }

  formChanged() {
    if (this.currentForm === this.expenseAccountForm) { return; }
    this.expenseAccountForm = this.currentForm;
    if (this.expenseAccountForm) {
      this.expenseAccountForm.valueChanges
        .subscribe(data => this.onValueChanged(data));
    }
  }
  onValueChanged(data?: any) {
    if (!this.expenseAccountForm) { return; }
    const form = this.expenseAccountForm.form;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  };

  ngOnInit() {
    this.submitted = false;
    if (this.expenseAccount.getImage() != null) {
      this.filePlaceholder = 'Changer le justificatif...';
    }
    if (this.expenseAccount.getDateExpenseAccount()) {
      this.expenseAccountFormHanlder.configDatePicker.initialDate = this.expenseAccount.getFormatedDateToDatePicker(moment(this.expenseAccount.getDateExpenseAccount(), 'DD-MM-YYYY').toString());
    }
    this.options = new DatePickerOptions(this.expenseAccountFormHanlder.configDatePicker);
  }

  onSubmit() {
    this.submitted = true;
  }

  handleExpenseAccount() {
    if (this.isCreationStrategy === true) {
      this.expenseAccount.setDateExpenseAccount(this.date.formatted);
      this.expenseAccountProvider.post(this.expenseAccount).then(ea => {
        this.uploader.setOptions(this.expenseAccountFormHanlder.getConfigForUpload(ea.getId()));
        this.uploader.uploadAll();
        this.router.navigate(['expense-accounts']);
      });
    } else {
      this.expenseAccountProvider.put(this.expenseAccount).then(ea => {
        this.uploader.setOptions(this.expenseAccountFormHanlder.getConfigForUpload(ea.getId()));
        this.uploader.uploadAll();
        this.router.navigate(["expense-accounts"]);
      });
    }
  }

}
