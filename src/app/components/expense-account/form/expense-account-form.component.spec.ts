import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpenseAccountFormComponent } from './expense-account-form.component';

describe('ExpenseAccountFormComponent', () => {
  let component: ExpenseAccountFormComponent;
  let fixture: ComponentFixture<ExpenseAccountFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpenseAccountFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpenseAccountFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
