import {Component, OnInit} from '@angular/core';
import {ExpenseAccountProvider} from "../../../services/expense-account/expense-account-provider.service";
import {ExpenseAccount} from "../../../model/expense-account.model";
import {ActivatedRoute} from "@angular/router";
const FEATURE_EXPENSE_ACCOUNT = 'feature_expense_account';


@Component({
  selector: 'app-expense-account-update',
  templateUrl: './expense-account-update.component.html',
  styleUrls: ['./expense-account-update.component.css'],
  providers: [ExpenseAccountProvider],
})
export class ExpenseAccountUpdateComponent implements OnInit {

  private expenseAccount: any;
  private id: Number;
  private voter: Boolean = false;

  constructor(private expenseAccountProvider: ExpenseAccountProvider, private router: ActivatedRoute) {
    this.voter = this.expenseAccountProvider.voteFactory(FEATURE_EXPENSE_ACCOUNT);
    this.router.params.subscribe(params => {
      this.id = +params['id'];
    });
  }

  ngOnInit() {
    this.expenseAccountProvider.get(this.id).then(ea => {
      this.expenseAccount = ea;
    });
  }



}
