import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {ExpenseAccountProvider} from '../../../../services/expense-account/expense-account-provider.service';
import {ExpenseAccount} from '../../../../model/expense-account.model';

@Component({
  selector: 'app-expense-self-list-account',
  templateUrl: './expense-account-self-list.component.html',
  providers: [ExpenseAccountProvider],
  styleUrls: ['./expense-account-self-list.component.css'],
})
export class ExpenseAccountSelfListComponent implements OnInit {

  private expenseAccounts: ExpenseAccount[];
  @Input() private voter: boolean;

  constructor(private expenseAccountProvider: ExpenseAccountProvider) {}

  ngOnInit() {
      this.expenseAccountProvider.getAllByEmployee().then(ea => {
        this.expenseAccounts = ea;
      });
  }


}
