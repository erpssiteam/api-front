import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {ExpenseAccountProvider} from '../../../../services/expense-account/expense-account-provider.service';
import {ExpenseAccount} from '../../../../model/expense-account.model';
const EXPENSE_ACCOUNT_PENDING_STATE = 1;
const EXPENSE_ACCOUNT_VALIDATED_STATE = 2;
const EXPENSE_ACCOUNT_DENIED_STATE = 3;
@Component({
  selector: 'app-expense-common-list-account',
  templateUrl: './expense-account-common-list.component.html',
  providers: [ExpenseAccountProvider],
  styleUrls: ['./expense-account-common-list.component.css'],
})
export class ExpenseAccountCommonListComponent implements OnInit {

  @Input() private expenseAccounts: ExpenseAccount[];
  @Input() private voter: boolean;
  @Input() private isManagedStrategy: boolean;
  private role: String;
  private rolesMap: Object;

  constructor(private expenseAccountProvider: ExpenseAccountProvider) {
    this.rolesMap = this.expenseAccountProvider.getCurrentUserRole().rolesMap;
    this.role = this.expenseAccountProvider.getCurrentUserRole().getLabel();
  }

  ngOnInit() {}

  /**
   * @param expenseAccountId
   */
  deleteExpenseAccount(expenseAccountId: Number): void {
    this.expenseAccountProvider.remove(expenseAccountId, this.expenseAccounts).then(ea => {
      this.expenseAccounts = ea;
    });
  }

  validateExpenseAccount(expenseAccount: ExpenseAccount): void {
    if(this.voter) {
      let state: number = 0;
      switch (expenseAccount.getState()) {
        case 1: state = EXPENSE_ACCOUNT_VALIDATED_STATE; break;
        case 2: state = EXPENSE_ACCOUNT_DENIED_STATE; break;
        case 3: state = EXPENSE_ACCOUNT_VALIDATED_STATE; break;
      }
      this.expenseAccountProvider.validate(state, expenseAccount, this.expenseAccounts).then(ea => {
        this.expenseAccounts = ea;
      });

    }
  }

}
