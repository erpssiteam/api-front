import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {ExpenseAccountProvider} from '../../../../services/expense-account/expense-account-provider.service';
import {ExpenseAccount} from '../../../../model/expense-account.model';

@Component({
  selector: 'app-expense-managed-list-account',
  templateUrl: './expense-account-managed-list.component.html',
  providers: [ExpenseAccountProvider],
  styleUrls: ['./expense-account-managed-list.component.css'],
})
export class ExpenseAccountManagedListComponent implements OnInit {

  private expenseAccounts: ExpenseAccount[];
  @Input() private voter: boolean;

  constructor(private expenseAccountProvider: ExpenseAccountProvider) {}

  ngOnInit() {
    this.expenseAccountProvider.getAll().then(ea => {
      this.expenseAccounts = ea;
    });
  }

}
