import { Component, OnInit, ElementRef, EventEmitter, Input, Output } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Cv } from "../../../model/cv.model";
import { CvProvider } from '../../../services/cvs/cv-provider.service';
import { DomSanitizer} from '@angular/platform-browser';

import { MaterializeAction, MaterializeDirective } from 'angular2-materialize';

declare var jQuery:any;
declare var Materialize:any;
@Component({
  selector: 'app-cv-list',
  templateUrl: './cv-list.component.html',
  providers: [CvProvider],
  styleUrls: ['./cv-list.component.css']
})
export class CvListComponent implements OnInit {


  @Input() private cvs: Cv[] = [];
  @Input() private charge: boolean = false;
  public today = new Date();

  constructor(public _cp: CvProvider , private _elRef : ElementRef, private router : Router,private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this._cp.getAll().then(ea => {
      this.cvs = ea;
      this.charge = true;
    })
  }
  changeUrl(chargeUrl){
    return this.sanitizer.bypassSecurityTrustResourceUrl(chargeUrl);
  }

}
