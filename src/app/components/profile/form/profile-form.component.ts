import { Component, Input, OnInit, AfterViewChecked, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CookiesTokenService } from '../../../services/cookies/cookies-token.service';
import { Employee } from '../../../model/employee.model';

@Component({
    selector: 'app-profile-form',
    templateUrl: './profile-form.component.html',
    styleUrls: ['./profile-form.component.css']

})
export class ProfileFormComponent implements OnInit, AfterViewChecked {
    private employee:Employee;

    constructor(private router: Router, private cookiesTokenService: CookiesTokenService) {

    }

    ngAfterViewChecked(): void {

    }

    ngOnInit() {
        let returnSession = this.cookiesTokenService.getSessionJSON('employee');

        if(returnSession){
            let employee = new Employee(returnSession.first_name, returnSession.last_name, returnSession.birthday_date, returnSession.email, returnSession.address,
                                        returnSession.zip_code, returnSession.city, returnSession.user);
            this.employee = employee.hydrateModelFromJSON(employee, returnSession)
        }
    }
}
