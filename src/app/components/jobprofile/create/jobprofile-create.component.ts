import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {JobProfilesProvider} from "../../../services/jobprofile/jobprofile-provider.service";
import {JobProfile} from "../../../model/jobprofile.model";

const FEATURE_JOBPROFILE = 'feature_jobprofile';

@Component({
    selector: 'app-jobprofile-create',
    templateUrl: './jobprofile-create.component.html',
    providers: [JobProfilesProvider],
    styleUrls: ['./jobprofile-create.component.scss'],
})
export class JobProfileCreateComponent implements OnInit {
    private voter: Boolean = false;
    private jobprofile: JobProfile;

    constructor(private jobprofileProvider: JobProfilesProvider) {
        this.jobprofile = new JobProfile();
        this.voter = this.jobprofileProvider.voteFactory(FEATURE_JOBPROFILE);
    }

    ngOnInit() {
    }


}
