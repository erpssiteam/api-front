import { Component, Input, OnInit, AfterViewChecked, ViewChild } from '@angular/core';
import { JobProfile } from '../../../model/jobprofile.model';
import { Customer } from '../../../model/customer.model';
import { JobProfilesProvider } from '../../../services/jobprofile/jobprofile-provider.service';
import { CustomersProvider } from '../../../services/customers/customers-provider.service';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { NgForm } from '@angular/forms';
import { JobProfileFormHandler } from '../../../services/jobprofile/jobprofile-form-handler.service';

@Component({
    selector: 'app-jobprofile-form',
    templateUrl: './jobprofile-form.component.html',
    styleUrls: ['./jobprofile-form.component.css'],
    providers: [JobProfilesProvider,JobProfileFormHandler,CustomersProvider]

})
export class JobProfileFormComponent implements OnInit, AfterViewChecked {

    @Input() private jobprofile: JobProfile;
    @Input() private isCreationStrategy: Boolean;
    @ViewChild('jobprofileForm') currentForm: NgForm;

    private customersArray: Array<Customer> = [];
    private jobprofileForm: NgForm;
    private formErrors: Object;
    private validationMessages: Object;

    constructor(private customersProvider:CustomersProvider, private jobprofileProvider: JobProfilesProvider, private router: Router, private jobprofileFormHanlder: JobProfileFormHandler) {
        this.formErrors = this.jobprofileFormHanlder.formErrors;
        this.validationMessages = this.jobprofileFormHanlder.validationMessages;
    }

    ngAfterViewChecked(): void {
        this.formChanged();
    }

    formChanged() {

        if (this.currentForm === this.jobprofileForm) { return; }

        this.jobprofileForm = this.currentForm;

        if (this.jobprofileForm) {
            this.jobprofileForm.valueChanges.subscribe(data => this.onValueChanged(data));
        }
    }

    onValueChanged(data?: any) {

        if (!this.jobprofileForm) { return; }

        const form = this.jobprofileForm.form;

        for (const field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = '';

            const control = form.get(field);

            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];

                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    };

    ngOnInit() {
        this.customersProvider.getAll().then(c => {
            this.customersArray = c
        });
    }

    handleJobProfile() {
        if (this.isCreationStrategy === true) {
            this.jobprofileProvider.post(this.jobprofile).then(h => {
                this.router.navigate(["jobs"]);
            });
        } else {
            this.jobprofileProvider.put(this.jobprofile).then(h => {
                this.router.navigate(["jobs"]);
            });
        }
    }
}
