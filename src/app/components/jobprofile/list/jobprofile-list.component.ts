import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { JobProfilesProvider } from '../../../services/jobprofile/jobprofile-provider.service';
import { JobProfile } from '../../../model/jobprofile.model';
import { Router } from '@angular/router';

const FEATURE_JOBPROFILE = 'feature_jobprofile';

@Component({
    selector: 'app-jobprofile-list-account',
    templateUrl: './jobprofile-list.component.html',
    providers: [JobProfilesProvider],
    styleUrls: ['./jobprofile-list.component.scss'],
})
export class JobProfileListComponent implements OnInit {
    @Input() private jobprofileCommon: Array<JobProfile> = [];
    private voter: Boolean = false;

    constructor(private jobprofileProvider: JobProfilesProvider, private router:Router) {
        this.voter = this.jobprofileProvider.voteFactory(FEATURE_JOBPROFILE);
    }

    ngOnInit() {
        this.jobprofileProvider.getAll().then(h => {
            this.jobprofileCommon = h;
        });
    }

    validJob(job){

        if(this.voter){
            job._published = 1;

            this.jobprofileProvider.put(job).then(h => {
                this.router.navigate(["jobs"]);
            });
        }
    }

    /**
    * @param jobprofileId
    */
    deleteJobProfile(jobprofileId: Number): void {
        this.jobprofileProvider.remove(jobprofileId).then(h => {
            this.jobprofileCommon = h
        });
    }

    /**
    * @param jobprofileId
    */
    offersJobProfile(jobprofileId: Number): void {
        this.router.navigate(['jobs/offers'], {
            queryParams: {
                jobProfile: jobprofileId
            }
        });
    }
}
