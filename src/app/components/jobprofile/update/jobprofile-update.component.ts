import {Component, OnInit} from '@angular/core';
import {JobProfilesProvider} from "../../../services/jobprofile/jobprofile-provider.service";
import {JobProfile} from "../../../model/jobprofile.model";
import {ActivatedRoute} from "@angular/router";

const FEATURE_JOBPROFILE = 'feature_jobprofile';

@Component({
    selector: 'app-jobprofile-update',
    templateUrl: './jobprofile-update.component.html',
    styleUrls: ['./jobprofile-update.component.css'],
    providers: [JobProfilesProvider],
})
export class JobProfileUpdateComponent implements OnInit {
    private voter: Boolean = false;
    private jobprofile: any;
    private id: Number;

    constructor(private jobprofileProvider: JobProfilesProvider, private router: ActivatedRoute) {
        this.router.params.subscribe(params => {
            this.id = +params['id'];
        });
        this.voter = this.jobprofileProvider.voteFactory(FEATURE_JOBPROFILE);
    }

    ngOnInit() {
        this.jobprofileProvider.get(this.id).then(h => {
            this.jobprofile = h;
        });
    }
}
