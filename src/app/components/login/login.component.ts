import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginsService } from '../../services/login/logins.service';
import { CookiesTokenService } from '../../services/cookies/cookies-token.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    providers: [ LoginsService, CookiesTokenService ],
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    _login = {user:"",password:""} ;

    constructor(public _lm: LoginsService, private _router:Router, private _cookiesTokenService:CookiesTokenService) {

        if(this._cookiesTokenService.getToken() != null){
            this._router.navigate(['/']);
        }
    }

    ngOnInit() {
    }

    onSubmitLogin(): void {
        // this._um.addUser(this.user);

        if(this._cookiesTokenService.getCookie('cookie') != null){

            if((this._login.user != undefined && this._login.user != "") || (this._login.password != undefined && this._login.password != "")){
                this._lm.login({login :this._login});
            }
        } else {
            alert("Thank you to validate the cookies !");
        }
    }
}
