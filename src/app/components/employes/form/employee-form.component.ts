import {Component, Input, OnInit, AfterViewChecked, ViewChild} from '@angular/core';
import { Employee } from '../../../model/employee.model';
import { DatePickerOptions, DateModel } from 'ng2-datepicker';
import {Router} from "@angular/router";
import * as moment from 'moment';
import { EmployesProvider } from '../../../services/employes/employes-provider.service';
import { EmployeeFormHandler } from '../../../services/employes/employes-form-handler.service';
import {NgForm} from "@angular/forms";



declare var Materialize:any;

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  providers: [ EmployesProvider, EmployeeFormHandler ],
  styleUrls: ['./employee-form.component.css']
})
export class EmployeeFormComponent implements OnInit, AfterViewChecked {

  roles = [
    {id: 1, value: "ROLE_ADMIN", libelle : "admin"},
    {id: 2, value: "ROLE_RH", libelle : "rh"},
    {id: 3, value: "ROLE_MANAGER", libelle : "manager"},
    {id: 4, value: "ROLE_COLLABORATOR", libelle : "collaborator"},
    {id: 5, value: "ROLE_NEWS", libelle : "news"}
  ];
  situationsFamilliales= [
    {id: 1, value: "célibataire", libelle : "employes.add.situation_familliale.celibataire"},
    {id: 2, value: "divorcé", libelle : "employes.add.situation_familliale.divorce"},
    {id: 3, value: "marié", libelle : "employes.add.situation_familliale.marie"},
    {id: 4, value: "paxé", libelle : "employes.add.situation_familliale.paxe"},
    {id: 5, value: "couple", libelle : "employes.add.situation_familliale.couple"},
    {id: 6, value: "veuf", libelle : "employes.add.situation_familliale.veuf"}
  ];

  @Input() private disabled: boolean;
  @Input() private employee: Employee;
  @Input() private isCreationStrategy: Boolean;
  @ViewChild('employeeForm') currentForm: NgForm;

  employeetForm: NgForm;
  private submitted: Boolean;
  private fichier = null;
  private sub: any;
  date: DateModel;
  options: DatePickerOptions;
  id: number;
  private formErrors: Object;
  private validationMessages: Object;

  private action:String;

  constructor(private EmployesManager: EmployesProvider, private router: Router, private employeeFormHandler: EmployeeFormHandler) {
    this.formErrors = this.employeeFormHandler.formErrors;
    this.validationMessages = this.employeeFormHandler.validationMessages;
  }

  ngOnInit() {
    this.submitted = false;
      if (this.employee.getBirthdayDate()) {
        this.employeeFormHandler.configBirthday.initialDate = this.employee.getFormatedDateToDatePicker(moment(this.employee.getBirthdayDate(), 'DD-MM-YYYY').toString());
      }
    this.options = new DatePickerOptions(this.employeeFormHandler.configDatePicker);
  }

  onSubmit() {
    this.submitted = true;
  }

  handleEmploye() {
    if(this.isCreationStrategy === true) {
      this.EmployesManager.post(this.employee).then(ea => {
        this.router.navigate(["employees"]);
      });
    } else {
      this.EmployesManager.put(this.employee).then(ea => {
        this.router.navigate(["employees"]);
      });
    }

  }
  ngOnDestroy() {
  }

  ngAfterViewChecked() {
    this.formChanged();
  }
  formChanged() {
    if (this.currentForm === this.employeetForm) { return; }
    this.employeetForm = this.currentForm;
    if (this.employeetForm) {
      this.employeetForm.valueChanges
          .subscribe(data => this.onValueChanged(data));
        Materialize.updateTextFields();
    }
  }
  onValueChanged(data?: any) {
    if (!this.employeetForm) { return; }
    const form = this.employeetForm.form;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  };

  verifForm(){
    var patternTelephone = new RegExp("/(0|(\\+33)|(0033))[1-9][0-9]{8}$");
    var patternZipCode = new RegExp("/^(([0-8][0-9])|(9[0-5])|(2[ab]))[0-9]{3}$/");

    if(this.employee.getLastname() && this.employee.getLastname() !='' && this.employee.getFirstname() &&
        this.employee.getFirstname()!=''  && this.employee.getAddress() && this.employee.getAddress() !='' && this.employee.getZipcode()
        && this.employee.getZipcode()!= 0   && this.employee.getCity() && this.employee.getCity()!= ''
        && this.employee.getEmail() && this.employee.getEmail()!= '' && this.employee.getPhone() && this.employee.getPhone()!= '')
    {
      if (patternTelephone.test(this.employee.getPhone().toString()) && patternZipCode.test(this.employee.getZipcode().toString())){
        this.disabled = !this.disabled;
      }
    } else {
      // this.disabled = !this.disabled;
    }
  }
  verifCin(){
    var patternPasseport =new RegExp("/^[A-PR-WY][1-9]\d\s?\d{4}[1-9]$","ig");
    let patternCIN = new RegExp("^0[1-2]([0-9]{2}){5}$");
    if(this.employee.getCin() && ""!=this.employee.getCin()){
      if (patternPasseport.test(this.employee.getCin())){
        this.disabled = !this.disabled;
      } else if(patternCIN.test(this.employee.getCin())){
        this.disabled = !this.disabled;
      } else {
        this.disabled = !this.disabled;
      }
    }else {
      this.disabled = false;
    }
  }


}
