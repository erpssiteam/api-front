import { Component, OnInit, ElementRef, EventEmitter, Input, Output } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Employee } from "../../../model/employee.model";
import { EmployesProvider } from '../../../services/employes/employes-provider.service';


import { MaterializeAction, MaterializeDirective } from 'angular2-materialize';

declare var jQuery:any;
declare var Materialize:any;

@Component({
  selector: 'app-employe-view',
  templateUrl: './employe-view.component.html',
  providers: [EmployesProvider],
  styleUrls: ['./employe-view.component.css']
})
export class EmployeViewComponent implements OnInit {

  private employee: any;
  private customers: any;
  private id: Number;

  constructor(private employeeManager: EmployesProvider, private router: ActivatedRoute) {
    this.router.params.subscribe(params => {
      this.id = +params['id'];
    });
  }
  ngOnInit() {
    this.employeeManager.get(this.id).then(ea => {
      this.employee = ea;
      this.employeeManager.getCustomerByEmploye(this.id).then(ce=> {
        this.customers =  ce;
      })
    });
  }
  isActive(instruction: boolean){
    if ( false != instruction){
      return true;
    }
  }


}
