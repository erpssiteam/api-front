import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import { EmployesProvider } from '../../../services/employes/employes-provider.service';

import {Employee} from "../../../model/employee.model";

@Component({
  selector: 'app-employee-update',
  templateUrl: './employee-update.component.html',
  styleUrls: ['./employee-update.component.css'],
  providers: [EmployesProvider],
})
export class EmployeeUpdateComponent implements OnInit {

  private employee: any;
  private id: Number;

  constructor(private employeeManager: EmployesProvider, private router: ActivatedRoute) {
    this.router.params.subscribe(params => {
      this.id = +params['id'];
    });
  }

  ngOnInit() {
    this.employeeManager.get(this.id).then(ea => {
        this.employee = ea;
    });
  }



}
