import { Component, OnInit } from '@angular/core';
import { EmployesProvider } from '../../../services/employes/employes-provider.service';
import { Employee } from "../../../model/employee.model";
import { User } from "../../../model/user.model";
import {Role} from "../../../model/role.model";


@Component({
  selector: 'app-employee-create',
  templateUrl: './employee-create.component.html',
  providers: [EmployesProvider],
  styleUrls: ['./employee-create.component.css']
})
export class EmployeeCreateComponent implements OnInit {

  private employee: Employee;

  constructor(private employesProvider: EmployesProvider) {
    this.employee = new Employee("","", new Date(),"","",0,"", new User("","",[new Role('collaborator')]));
  }

  ngOnInit() {

  }

}
