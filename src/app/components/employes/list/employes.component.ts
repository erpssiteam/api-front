import { Component, OnInit, ElementRef, EventEmitter, Input, Output } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Employee } from "../../../model/employee.model";
import { EmployesProvider } from '../../../services/employes/employes-provider.service';


declare var jQuery:any;
declare var Materialize:any;

@Component({
    selector: 'app-employes',
    templateUrl: './employes.component.html',
    providers: [EmployesProvider],
    styleUrls: ['./employes.component.css']
})
export class EmployesComponent implements OnInit {
    @Input() private employes: Employee[] = [];
    @Input() private charge: boolean = false;

    public today = new Date();

    constructor( public _em: EmployesProvider , private _elRef : ElementRef, private router : Router ){
    }

    ngOnInit() {
        this._em.getAll().then(ea => {
            this.employes = ea;
            this.charge = true;
        })
    }

    isActive(instruction: boolean){
        if ( false != instruction){
            return true;
        }
    }

    delete(employeId: Number): void {
        this._em.remove(employeId).then(ea => {
        this.employes = ea;
         });
    }
}
