import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GroupwareProvider } from '../../../services/groupware/groupware-provider.service';
import { EmployesProvider } from '../../../services/employes/employes-provider.service';
// import { EmployeeProvider } from '../../../services/employee/employee-provider.service';
import { Groupware } from '../../../model/groupware.model';
import { NgForm } from '@angular/forms';

const FEATURE_GROUPWARE = 'feature_groupware';

@Component({
    selector: 'app-groupware-list-account',
    templateUrl: './groupware-list.component.html',
    providers: [GroupwareProvider, EmployesProvider],
    styleUrls: ['./groupware-list.component.scss'],
})
export class GroupwareListComponent implements OnInit {
    private voter: Boolean = false;
    private groupware:Groupware;
    private groupwareForm: NgForm;
    private returnGroupware: Array<{label:String,id:Number}> = [];
    private employees;

    constructor(private groupwareProvider: GroupwareProvider, private employesProvider:EmployesProvider) {
        this.voter = this.groupwareProvider.voteFactory(FEATURE_GROUPWARE);

        this.employesProvider.getAll().then(g => {
            this.employees = g

            this.groupwareProvider.getAll(this.employees).then(g => {
                this.groupware = g;

                for(let item in g._employeesRoles){

                    if(g._employeesRoles[item] == "checked"){
                        let split = item.split("-")
                        this.returnGroupware.push({label:split[0],id:Number(split[1])})
                    }
                }
            });
        })
    }

    ngOnInit() {
    }

    onChanged(employee,role){
        let currentIndex = this.returnGroupware.indexOf(this.returnGroupware.find(o => o.id == Number(employee)))
        if(currentIndex >= 0) {
            this.returnGroupware[currentIndex].label = role
        }
    }

    handleGroupware(){
        let array: Array<{id_employee:Number,name_role:String}> = [];
        for(let item of this.returnGroupware){
            if(item.label){
                array.push({id_employee:item.id,name_role:item.label})
            }
        }
        this.groupwareProvider.post(array);
    }
}
