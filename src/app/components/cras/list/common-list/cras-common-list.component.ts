import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { CraProvider } from '../../../../services/cras/cra-provider.service';
import { Cra } from '../../../../model/cra.model';
import { Router } from '@angular/router';

const STATE_PENDING = 1;
const STATE_VALIDATED = 2;
const STATE_DENIED = 3;

@Component({
  selector: 'app-cras-common-list-account',
  templateUrl: './cras-common-list.component.html',
  providers: [ CraProvider ],
  styleUrls: ['./cras-common-list.component.css'],
})
export class CrasCommonListComponent implements OnInit {

  @Input() private cras: Cra[];
  @Input() private voter: boolean;
  @Input() private isManagedStrategy: boolean;
  private role: String;
  private rolesMap: Object;

  constructor(private craProvider: CraProvider,private router:Router) {
    this.rolesMap = this.craProvider.getCurrentUserRole().rolesMap;
    this.role = this.craProvider.getCurrentUserRole().getLabel();
  }

  ngOnInit() {}

  /**
   * @param craId
   */
  delete(craId: Number): void {
    this.craProvider.remove(craId).then(ea => {
      this.cras = ea;
    });
  }
  validate(cra: Cra): void {
    if(this.voter) {
      let state: number = 0;
      switch (cra.getState()) {
        case 3: state = STATE_PENDING; break;
        case 1: state = STATE_VALIDATED; break;
        case 2: state = STATE_DENIED; break;
      }
      this.craProvider.validate(state, cra).then(a => {
        this.cras = a;
        this.router.navigate(['cras'])
      });

    }
  }

}
