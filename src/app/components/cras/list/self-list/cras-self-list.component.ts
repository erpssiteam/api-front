import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {CraProvider} from '../../../../services/cras/cra-provider.service';
import {Cra} from '../../../../model/cra.model';

@Component({
  selector: 'app-cras-self-list-account',
  templateUrl: './cras-self-list.component.html',
  providers: [CraProvider],
  styleUrls: ['./cras-self-list.component.css'],
})
export class CrasSelfListComponent implements OnInit {

  private cras: Cra[];
  @Input() private voter: boolean;

  constructor(private craProvider: CraProvider) {}

  ngOnInit() {
      this.craProvider.getAllByEmployee().then(ea => {
        this.cras = ea;
      });
  }


}
