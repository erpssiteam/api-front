import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {CraProvider} from '../../../../services/cras/cra-provider.service';
import {Cra} from '../../../../model/cra.model';

@Component({
  selector: 'app-cras-managed-list-account',
  templateUrl: './cras-managed-list.component.html',
  providers: [CraProvider],
  styleUrls: ['./cras-managed-list.component.css'],
})
export class CrasManagedListComponent implements OnInit {

  private cras: Cra[];
  @Input() private voter: boolean;

  constructor(private craProvider: CraProvider) {}

  ngOnInit() {
    this.craProvider.getAll().then(ea => {
      this.cras = ea;
    });
  }

}
