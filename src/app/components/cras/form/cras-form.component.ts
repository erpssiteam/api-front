import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Cra} from "../../../model/cra.model";
import { DateModel, DatePickerOptions } from "ng2-datepicker";
import { NgForm } from "@angular/forms";
import {CraProvider} from "../../../services/cras/cra-provider.service";
import {Router} from "@angular/router";
import * as moment from 'moment';
import {CraFormHandler} from "../../../services/cras/cra-form-handler.service";
import {Customer} from "../../../model/customer.model";
const FEATURE_CRA = 'feature_cra';

@Component({
  selector: 'app-cras-form',
  templateUrl: './cras-form.component.html',
  styleUrls: ['./cras-form.component.css'],
  providers: [ CraProvider, CraFormHandler]
})
export class CrasFormComponent implements OnInit {

  private voter: Boolean = false;
  @Input() private cra: Cra;
  @Input() private isCreationStrategy: Boolean;
  @Input() private customers: Customer[];
  private submitted: Boolean;
  started_at: DateModel;
  ended_at: DateModel;
  options: DatePickerOptions;
  craForm: NgForm;
  @ViewChild('craForm') currentForm: NgForm;
  formErrors: Object;
  validationMessages: Object;

  constructor(private craProvider: CraProvider, private router: Router, private craFormHandler: CraFormHandler) {
    this.voter = this.craProvider.voteFactory(FEATURE_CRA);
    this.formErrors = this.craFormHandler.formErrors;
    this.validationMessages = this.craFormHandler.validationMessages;
  }

  ngAfterViewChecked(): void {
    this.formChanged();
  }

  formChanged() {
    if (this.currentForm === this.craForm) { return; }
    this.craForm = this.currentForm;
    if (this.craForm) {
      this.craForm.valueChanges
          .subscribe(data => this.onValueChanged(data));
    }
  }

  onValueChanged(data?: any) {
    if (!this.craForm) { return; }
    const form = this.craForm.form;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  };

  ngOnInit() {
    this.submitted = false;

    if (this.cra.getStarted_at()) {
      this.craFormHandler.configDatePicker.initialDate = this.cra.getFormatedDateToDatePicker(moment(this.cra.getStarted_at(), 'DD-MM-YYYY h:mm').toString());
    }
    if (this.cra.getEnded_at()) {
      this.craFormHandler.configDatePicker.initialDate = this.cra.getFormatedDateToDatePicker(moment(this.cra.getEnded_at(), 'DD-MM-YYYY h:mm').toString());
    }
    if (this.cra.getRecorded_at()) {
      this.craFormHandler.configDatePicker.initialDate = this.cra.getFormatedDateToDatePicker(moment(this.cra.getRecorded_at(), 'DD-MM-YYYY').toString());
    }
    if (this.cra.getValidated_at()) {
      this.craFormHandler.configDatePicker.initialDate = this.cra.getFormatedDateToDatePicker(moment(this.cra.getValidated_at(), 'DD-MM-YYYY').toString());
    }

    this.options = new DatePickerOptions(this.craFormHandler.configDatePicker);
  }

  onSubmit() {
    this.submitted = true;
  }
  selected(customerId, craCustomer){
    if(customerId === craCustomer.id){
      return true
    }else{
      return false
    }
  }

  handleCra() {
    if(this.voter) {
      this.cra.setStarted_at(this.started_at.formatted);
      this.cra.setEnded_at(this.ended_at.formatted);
      if (this.isCreationStrategy === true) {
        this.cra.setRecorded_at(moment().format('DD-MM-YYYY').toString());
        this.craProvider.post(this.cra).then(a => {
          this.router.navigate(['cras']);
        });
      } else {
        this.craProvider.put(this.cra).then(application => {
          this.router.navigate(['cras']);
        });
      }
    }
  }




}
