import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrasFormComponent } from './cras-form.component';

describe('CrasFormComponent', () => {
  let component: CrasFormComponent;
  let fixture: ComponentFixture<CrasFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrasFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrasFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
