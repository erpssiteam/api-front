import { Component, OnInit } from '@angular/core';
import { CraProvider } from '../../../services/cras/cra-provider.service';
import { Cra } from '../../../model/cra.model';
import { ActivatedRoute } from '@angular/router';
import {CustomersProvider} from "../../../services/customers/customers-provider.service";
import {Customer} from "../../../model/customer.model";
const FEATURE_CRA = 'feature_cra';


@Component({
  selector: 'app-cras-update',
  templateUrl: './cras-update.component.html',
  styleUrls: ['./cras-update.component.css'],
  providers: [ CraProvider,CustomersProvider ],
})
export class CrasUpdateComponent implements OnInit {

  private cra: any;
  private id: Number;
  private voter: Boolean = false;
  private customers: Customer [];


  constructor(private craProvider: CraProvider, private router: ActivatedRoute, private customersProvider: CustomersProvider) {
    this.voter = this.craProvider.voteFactory(FEATURE_CRA);
    this.router.params.subscribe(params => {
      this.id = +params['id'];
    });
    this.customersProvider.getAll().then(c => {
      this.customers = c;
      this.craProvider.get(this.id).then(ea => {
        this.cra = ea;
        this.cra.idEmploye = this.cra.employee.id;
        this.cra.idCustomer = this.cra.customer.id;
      });

    });
  }
  ngOnInit() {
  }

}
