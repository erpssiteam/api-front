import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrasUpdateComponent } from './cras-update.component';

describe('CrasUpdateComponent', () => {
  let component: CrasUpdateComponent;
  let fixture: ComponentFixture<CrasUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrasUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrasUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
