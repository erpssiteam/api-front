import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrasCreateComponent } from './cras-create.component';

describe('CrasCreateComponent', () => {
  let component: CrasCreateComponent;
  let fixture: ComponentFixture<CrasCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrasCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrasCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
