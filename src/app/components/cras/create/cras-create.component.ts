import { Component, OnInit } from '@angular/core';
import { CraProvider } from '../../../services/cras/cra-provider.service';
import { Cra } from '../../../model/cra.model';
import { EmployesProvider } from '../../../services/employes/employes-provider.service';
import { Employee } from '../../../model/employee.model';
import { CustomersProvider } from '../../../services/customers/customers-provider.service';
import { Customer } from '../../../model/customer.model';
const FEATURE_CRA = 'feature_cra';

@Component({
  selector: 'app-cras-create',
  templateUrl: './cras-create.component.html',
  styleUrls: ['./cras-create.component.css'],
  providers: [ CraProvider, EmployesProvider, CustomersProvider ]
})
export class CrasCreateComponent implements OnInit {
  private voter: Boolean = false;
  private cra: any;
  private customers: Customer[];


  constructor(private craProvider: CraProvider, private employesProvider: EmployesProvider, private customersProvider: CustomersProvider) {
    this.voter = this.craProvider.voteFactory(FEATURE_CRA);
    this.cra = new Cra();
    if(this.voter)
      this.customersProvider.getAll().then(c => {
        this.customers = c;
      });
  }
  ngOnInit() {

  }

}
