import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrasViewComponent } from './cras-view.component';

describe('CrasViewComponent', () => {
  let component: CrasViewComponent;
  let fixture: ComponentFixture<CrasViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrasViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrasViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
