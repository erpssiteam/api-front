import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Cra } from "../../../model/cra.model";
import { CraProvider } from '../../../services/cras/cra-provider.service';

import { MaterializeAction, MaterializeDirective } from 'angular2-materialize';

declare let jQuery:any;
declare let Materialize:any;

@Component({
  selector: 'app-cras-view',
  templateUrl: './cras-view.component.html',
  providers: [CraProvider],
  styleUrls: ['./cras-view.component.css']
})
export class CrasViewComponent implements OnInit {

  private cra: any;
  private customers: any;
  private id: Number;


  constructor(private craProvider: CraProvider, private router: ActivatedRoute) {
    this.router.params.subscribe(params => {
      this.id = +params['id'];
    })
  }

  ngOnInit() {
    this.craProvider.get(this.id).then(cr => {
      this.cra = cr;
    });
  }

  isActive(instruction: boolean) {
    if (false != instruction) {
      return true;
    }
  }
}
