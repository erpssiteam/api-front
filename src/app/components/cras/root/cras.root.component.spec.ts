import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrasRootComponent } from './cras.root.component';

describe('CrasRootComponent', () => {
  let component: CrasRootComponent;
  let fixture: ComponentFixture<CrasRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrasRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrasRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
