import { Component, OnInit } from '@angular/core';
import { CraProvider } from '../../../services/cras/cra-provider.service';
const FEATURE_CRA = 'feature_cra';

@Component({
  selector: 'app-cras-root',
  templateUrl: './cras.root.component.html',
  styleUrls: ['./cras.root.component.css'],
  providers: [ CraProvider ],
})
export class CrasRootComponent implements OnInit {

  private voter: Boolean = false;

  constructor(private craProvider: CraProvider) {
    this.voter = this.craProvider.voteFactory(FEATURE_CRA);
  }
  ngOnInit() {
  }

}


