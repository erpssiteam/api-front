import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { JobProfilesProvider } from '../../services/jobprofile/jobprofile-provider.service';
import { NewsProvider } from '../../services/news/news-provider.service';
import { HolidaysProvider } from '../../services/holidays/holidays-provider.service';
import * as moment from 'moment';

const FEATURE_NEWS = 'feature_news';
const FEATURE_HOLIDAYS = 'feature_holiday';
const FEATURE_JOBPROFILE = 'feature_jobprofile';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css'],
    providers: [JobProfilesProvider, NewsProvider, HolidaysProvider],
})
export class DashboardComponent implements OnInit {
    private jobprofile: Number = 0;
    private jobprofilePublished: Number = 0;
    private jobprofileNotPublished: Number = 0;
    private newsArray: Array<any> = [];
    private news: Number = 0;
    private newsPublished: Number = 0;
    private newsNotPublished: Number = 0;
    private holiday: Number = 0;
    private holidayPending: Number = 0;
    private holidayTodo: Number = 0;

    constructor(private jobprofileProvider: JobProfilesProvider, private newsProvider: NewsProvider, private holidaysProvider: HolidaysProvider) {

    }

    ngOnInit() {
        if(this.jobprofileProvider.voteFactory(FEATURE_JOBPROFILE)){
            this.jobprofileProvider.getAll().then(h => {

                for(let jp of h){
                    this.jobprofile = +this.jobprofile + 1

                    if(jp._published){
                        this.jobprofilePublished = +this.jobprofilePublished + 1
                    } else {
                        this.jobprofileNotPublished = +this.jobprofileNotPublished + 1
                    }
                }
            })
        }

        if(this.newsProvider.voteFactory(FEATURE_NEWS)){
            this.newsProvider.getAll().then(n => {
                this.newsArray = n

                for(let news of n){
                    this.news = +this.news + 1

                    if(news.isPublished){
                        this.newsPublished = +this.newsPublished + 1
                    } else {
                        this.newsNotPublished = +this.newsNotPublished + 1
                    }
                }
            });
        }

        if(this.holidaysProvider.voteFactory(FEATURE_HOLIDAYS)){
            this.holidaysProvider.getAll().then(h => {

                for(let holiday of h){
                    this.holiday = +this.holiday + 1

                    // holiday started
                    if(moment(holiday._started_at,"DD-MM-YYYY") < moment(new Date())){

                        // holiday not closed
                        if(moment(holiday._ended_at,"DD-MM-YYYY") > moment(new Date())){
                            this.holidayPending = +this.holidayPending + 1
                        } else {
                            // holiday closed
                        }
                    } else {
                        // holiday not started
                        this.holidayTodo = +this.holidayTodo + 1
                    }
                }
            });
        }
    }
}
