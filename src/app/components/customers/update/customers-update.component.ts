import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import { CustomersProvider } from '../../../services/customers/customers-provider.service'

import { Customer } from '../../../model/customer.model'

@Component({
  selector: 'app-customers-update',
  templateUrl: './customers-update.component.html',
  providers: [ CustomersProvider ],
  styleUrls: ['./customers-update.component.css']
})
export class CustomersUpdateComponent implements OnInit {

  private customer: any;
  private id: Number;

  constructor(private customerProvider: CustomersProvider, private router: ActivatedRoute) {
    this.router.params.subscribe(params => {
      this.id = +params['id'];
    });
  }

  ngOnInit() {
    this.customerProvider.get(this.id).then(ea => {
      this.customer = ea;
    });
  }

}