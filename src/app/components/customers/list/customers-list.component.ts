import { Component, OnInit, ElementRef, EventEmitter, Input, Output } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Customer } from "../../../model/customer.model";
import { CustomersProvider } from '../../../services/customers/customers-provider.service';


import { MaterializeAction, MaterializeDirective } from 'angular2-materialize';

declare var jQuery:any;
declare var Materialize:any;
@Component({
  selector: 'app-customers-list',
  templateUrl: './customers-list.component.html',
  providers: [CustomersProvider],
  styleUrls: ['./customers-list.component.css']
})
export class CustomersListComponent implements OnInit {

  @Input() private customers: Customer[] = [];
  @Input() private charge: boolean = false;

  public today = new Date();

  constructor( public _em: CustomersProvider , private _elRef : ElementRef, private router : Router ){
  }

  ngOnInit() {
    this._em.getAll().then(ea => {
      this.customers = ea;
      this.charge = true;
    })
  }

  isActive(instruction: boolean){
    if ( false != instruction){
      return true;
    }
  }

  delete(customerId: Number): void {
    this._em.remove(customerId).then(ea => {
      this.customers = ea;
    });
  }

}
