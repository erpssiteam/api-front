import {Component, Input, OnInit, AfterViewChecked, ViewChild} from '@angular/core';
import { Customer } from '../../../model/customer.model';
import { DatePickerOptions, DateModel } from 'ng2-datepicker';
import { Router } from "@angular/router";
import * as moment from 'moment';
import { CustomersProvider } from '../../../services/customers/customers-provider.service';
import { CustomerFormHandler } from '../../../services/customers/customers-form-handler.service';
import { NgForm } from "@angular/forms";

declare var Materialize:any;

@Component({
  selector: 'app-customers-form',
  templateUrl: './customers-form.component.html',
  providers: [ CustomersProvider, CustomerFormHandler ],
  styleUrls: ['./customers-form.component.css']
})
export class CustomersFormComponent implements OnInit, AfterViewChecked {

  @Input() private disabled: boolean;
  @Input() private customer: Customer;
  @Input() private isCreationStrategy: Boolean;
  @ViewChild('customerForm') currentForm: NgForm;

  customerForm: NgForm;
  private submitted: Boolean;
  private fichier = null;
  private sub: any;
  date: DateModel;
  options: DatePickerOptions;
  id: number;
  private formErrors: Object;
  private validationMessages: Object;
  private action:String;

  constructor(private CustomersProvider: CustomersProvider, private router: Router, private customerFormHandler: CustomerFormHandler) {
    this.formErrors = this.customerFormHandler.formErrors;
    this.validationMessages = this.customerFormHandler.validationMessages;
  }


  ngOnInit() {
    this.submitted = false;

  }
  onSubmit() {
    this.submitted = true;
  }
  handleCustomer() {
    if(this.isCreationStrategy === true) {
      this.CustomersProvider.post(this.customer).then(ea => {
        this.router.navigate(["customers"]);
      });
    } else {
      this.CustomersProvider.put(this.customer).then(ea => {
        this.router.navigate(["customers"]);
      });
    }

  }
  ngOnDestroy() {
  }
  ngAfterViewChecked() {
    this.formChanged();
  }
  formChanged() {
    if (this.currentForm === this.customerForm) { return; }
    this.customerForm = this.currentForm;
    if (this.customerForm) {
      this.customerForm.valueChanges
          .subscribe(data => this.onValueChanged(data));
      Materialize.updateTextFields();
    }
  }
  onValueChanged(data?: any) {
    if (!this.customerForm) { return; }
    const form = this.customerForm.form;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  };
  verifForm(){
    var patternTelephone = new RegExp("/(0|(\\+33)|(0033))[1-9][0-9]{8}$");
    var patternZipCode = new RegExp("/^(([0-8][0-9])|(9[0-5])|(2[ab]))[0-9]{3}$/");

    if(this.customer.getLastname() && this.customer.getLastname() !='' && this.customer.getFirstname() && this.customer.getFirstname()!=''  && this.customer.getAddress() && this.customer.getAddress() !='' && this.customer.getZipcode()
        && this.customer.getZipcode()!= 0   && this.customer.getCity() && this.customer.getCity()!= ''
        && this.customer.getEmail() && this.customer.getEmail()!= '' && this.customer.getPhone() && this.customer.getPhone()!= '')
    {
      if (patternTelephone.test(this.customer.getPhone().toString()) && patternZipCode.test(this.customer.getZipcode().toString())){
        this.disabled = !this.disabled;
      }
    } else {
      // this.disabled = !this.disabled;
    }
  }
  verifSiret(siret){
    let isValide;
    if ( (siret.length != 14) || (isNaN(siret)) )
      isValide = false;
    else {
      let somme = 0;
      let tmp;
      for (var cpt = 0; cpt<siret.length; cpt++) {
        if ((cpt % 2) == 0) { // Les positions impaires : 1er, 3è, 5è, etc...
          tmp = siret.charAt(cpt) * 2; // On le multiplie par 2
          if (tmp > 9)
            tmp -= 9;	// Si le résultat est supérieur à 9, on lui soustrait 9
        }
        else
          tmp = siret.charAt(cpt);
        somme += parseInt(tmp);
      }
      if ((somme % 10) == 0)
        isValide = true; // Si la somme est un multiple de 10 alors le SIRET est valide
      else
        isValide = false;
    }
    return isValide;

  }
  verifSiren(siren){
      let isValide;
      if ( (siren.length != 9) || (isNaN(siren)) )
        isValide = false;
      else {
        var somme = 0;
        var tmp;
        for (var cpt = 0; cpt<siren.length; cpt++) {
          if ((cpt % 2) == 1) { // Les positions paires : 2ème, 4ème, 6ème et 8ème chiffre
            tmp = siren.charAt(cpt) * 2; // On le multiplie par 2
            if (tmp > 9)
              tmp -= 9;	// Si le résultat est supérieur à 9, on lui soustrait 9
          }
          else
            tmp = siren.charAt(cpt);
          somme += parseInt(tmp);
        }
        if ((somme % 10) == 0)
          isValide = true;	// Si la somme est un multiple de 10 alors le SIREN est valide
        else
          isValide = false;
      }
      return isValide;
  }

}






