import { Component, OnInit } from '@angular/core';
import { CustomersProvider } from '../../../services/customers/customers-provider.service';
import { Customer } from '../../../model/customer.model';
import { User } from "../../../model/user.model";
import { Role } from "../../../model/role.model";

@Component({
  selector: 'app-customers-create',
  templateUrl: './customers-create.component.html',
  providers: [CustomersProvider],
  styleUrls: ['./customers-create.component.css']
})

export class CustomersCreateComponent implements OnInit {

  private customer: Customer;

  constructor(private customerProvider: CustomersProvider) {
    this.customer = new Customer("","", new Date(),"","",0,"", new User("","",[new Role('collaborator')]));
  }

  ngOnInit() {

  }

}