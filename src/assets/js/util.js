$(document).ready(function(){

    initChampDate = function(){
        $.mask.definitions['j'] = "[0-3]";
        $.mask.definitions['J'] = "[0-9]";
        $.mask.definitions['m'] = "[0-1]";
        $.mask.definitions['M'] = "[0-9]";

        $(".date").mask("jJ/mM/9999",{placeholder: "dd/mm/aaaa", completed:function(){
            var validateDate = this.validateDateOfDay(this.val());
            if("ko" == validateDate){
                this.removeClass("valid");
                this.addClass("invalid");
            }else {
                this.removeClass("invalid");
                this.addClass("valid");
            }
        }});
    };
    validateDateOfDay = function(date){
        var day = new Date();
        var datePublication = this.formatageDate(date);
        if(datePublication !="ko"){
            if(datePublication <  day){
                return 'ok';
            } else {
                return"ko";
            }
        }else {
            return"ko";
        }
    };
    formatageDate=  function(date){
        var parts = date.split("/");
        var jourMoi = {1 : 31,2:29,3:31,4:30,5:31,6:30,7:31,8:31,9:30,10:31,11:30,12:31};
        if ( parts[1] - 1 > 13 ||  jourMoi[parts[1]] >31 ){
            return "ko";
        }else{
            return new Date(parts[2], parts[1] - 1, parts[0]);
        }
    };
    initChampDate();

    escapeHtml = function (text) {
        var map = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#039;'
        };

        return text.replace(/[&<>"']/g, function(m) { return map[m]; });
    }
});
