$(document).ready(function(){
    $('#candidats').DataTable({
        "language": {
            "url": "./assets/datatable/lang/dataTables.french.lang"
        },
        "ordering": true,
        "iDisplayLength": 5,
        "aLengthMenu": [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "all"]
        ]
    });
    Materialize.updateTextFields();
});