$(document).ready(function(){
    $('#cras').DataTable({
        "language": {
            "url": "./assets/datatable/lang/dataTables.french.lang"
        },
        "ordering": true,
        "iDisplayLength": 5,
        "aLengthMenu": [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "all"]
        ]
    });
    Materialize.updateTextFields();
    jQuery(function($){
        $.mask.definitions['j'] = "[0-3]";
        $.mask.definitions['J'] = "[0-9]";
        $.mask.definitions['m'] = "[0-1]";
        $.mask.definitions['M'] = "[0-9]";
        $("#input-add-cra-dt").mask("jJ/mM/9999", { placeholder:"dd/mm/aaaa" });
    });

});