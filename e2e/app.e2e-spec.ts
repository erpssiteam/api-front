import { FontErpssiPage } from './app.po';

describe('font-erpssi App', function() {
  let page: FontErpssiPage;

  beforeEach(() => {
    page = new FontErpssiPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
